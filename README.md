# README #

This package is intended contains helper classes for the core package and some universal helpers. In particular, I have added the possibility to use certain gsl functions, because they are simply faster than those found in either the COLT or commons math packages. If you have the GSL installed, either use the proved shared libraries or make on of your own as described in src/main/c++/jmath.cpp, I have included instructions for Linux and MacOS, haven't tried it on a windows machine yet.

### License ###
This package is licensed under GPLv3. If you care to use it in a context not covered by this license, feel free to contact us.

### Contact ###

[Patrick](mailto:patrick.jaehnichen@hu-berlin.de)