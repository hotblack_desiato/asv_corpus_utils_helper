/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/*
 * generate the shared library:
 * Mac OS X
 * g++ -lgsl -dynamiclib -I/System/Library/Frameworks/JavaVM.framework/Versions/Current/Headers -I/Users/patrick/Code/workspace/asv_corpus_utils_helper/src/main/c++ -o src/main/resources/de/uni_leipzig/informatik/asv/corpusUtils/helper/math/libjmath_mac.so src/main/c++/jmath.cpp
 * Linux
 * g++ -fPIC -lgsl -shared -I/Users/patrick/Code/workspace/asv_corpus_utils_helper/src/main/c++ -I/usr/lib/jvm/jdk1.7.0_04/include -I/usr/lib/jvm/jdk1.7.0_04/include/linux -o src/main/resources/de/uni_leipzig/informatik/asv/corpusUtils/helper/math/libjmath_linux.so src/main/c++/jmath.cpp
 *
 */

#include <jni.h>
#include <stdio.h>
#include "de_uni_leipzig_informatik_asv_corpusUtils_helper_math_MathUtils.h"
#include <math.h>
#include <gsl/gsl_sf_psi.h>

JNIEXPORT jdouble JNICALL Java_de_uni_1leipzig_informatik_asv_corpusUtils_helper_math_MathUtils_gslLgamma
  (JNIEnv *env, jclass thisClass, jdouble val){
  return lgamma(val);
}

JNIEXPORT jdouble JNICALL Java_de_uni_1leipzig_informatik_asv_corpusUtils_helper_math_MathUtils_gslDigamma
  (JNIEnv *env, jclass thisClass, jdouble val){
	return gsl_sf_psi(val);
}


JNIEXPORT jdouble JNICALL Java_de_uni_1leipzig_informatik_asv_corpusUtils_helper_math_MathUtils_gslTrigamma
  (JNIEnv *env, jclass thisClass, jdouble val){
	return gsl_sf_psi_1(val);
}
