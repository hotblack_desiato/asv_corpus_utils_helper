/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;



public abstract class AbstractGaussianProcess {
	protected ICovarianceFunction covariance;
	protected IMeanFunction mean;
	protected double[][] dataCovariates;
	protected double[][] dataCovariateCovariance;
	protected double[][] testData;
	protected double[][] testDataCovariance;
	protected double[][] dataTestDataCrossCovariance;
	protected double[] targetData;
	protected double[] predictiveMean;
	protected double[] predictiveVariance;
	protected double m0;
	
	/**
	 * Create a Gaussian Process object with the given mean and covariance functions.
	 * @param meanFct - the mean function
	 * @param covFct - the covariance function
	 */
	protected AbstractGaussianProcess(IMeanFunction meanFct, ICovarianceFunction covFct) {
		this.mean = meanFct;
		this.covariance = covFct;
	}
	
	/**
	 * Getter for the covariance function. Expects the type of the covariance function as generic argument.
	 * @return the process' covariance function
	 */
	public <K extends ICovarianceFunction> K getCovFunction() {
		return (K)this.covariance;
	}
	
	/**
	 * Getter for the mean function. Expects the type of the mean function as generic argument.
	 * @return the process' mean function
	 */
	public <M extends IMeanFunction> M getMeanFunction() {
		return (M)this.mean;
	}
	
	public void setDataCovariates(double[][] _x) {
		this.dataCovariates = _x;
		this.dataCovariateCovariance = this.covariance.computeCovarianceMatrix(dataCovariates);
	}
	
	public void setTargetData(double[] target) {
		this.m0 = VectorUtils.mean(target);
		this.targetData = VectorUtils.subtractScalar(target, m0);
	}
	
	public void setTestData(double[][] testPoints) {
		this.testData =  testPoints;
		this.testDataCovariance = this.covariance.computeCovarianceMatrix(testData);
		this.dataTestDataCrossCovariance = this.covariance.computeCrossCovarianceMatrix(dataCovariates, testData);
	}
	
	/**
	 * Sets the training data used to train this GP. Data covariates are expected to
	 * be matrices consisting of row vectors, i.e. their dimensions are N x D. Columns represent the components
	 * of the D-dimensional points, rows correspond the n-th of the N points given.
	 * @param input_x - the data covariates
	 * @param target - the target values
	 */
	@Deprecated
	public void setTrainingData(double[][] input_x, double[] target) {
		this.dataCovariates = input_x;
		//zero mean the targets and store the mean
		this.m0 = VectorUtils.mean(target);
		this.targetData = VectorUtils.subtractScalar(target, this.m0);
	}
	
	/**
	 * Trains this Gaussian process with the given inputs and targets. 
	 */
	public abstract void train();
	
	
	/**
	 * Compute the marginal likelihood of the observed data points.
	 * @return log p(y|X), the log marginal likelihood
	 */
	public abstract double computeLikelihood();
	
	/**
	 * Get the training data.
	 * @return the training data used to train this GP
	 */
	public double[][] getDataCovariates() {
		return this.dataCovariates;
	}
	
	/**
	 * Get the target data.
	 * @return the target data used to train this GP
	 * @return
	 */
	public double[] getTargetData() {
		return this.targetData;
	}

	/**
	 * Returns the predictive 
	 * @return the predictiveMean
	 */
	public double[] getPredictiveMean() {
		// put mean back on
		return VectorUtils.addScalar(this.predictiveMean, this.m0);
	}

	/**
	 * @return the predictiveVariance
	 */
	public double[] getPredictiveVariance() {
		return this.predictiveVariance;
	}
	
//	public abstract void 
	
	/**
	 * Predict mean and variance for the provided input, given that
	 * the GP has been trained before.
	 * @param unseenPoints - the test data for which to predict targets
	 */
	public abstract void predict(double[][] unseenPoints);
	
	
}
