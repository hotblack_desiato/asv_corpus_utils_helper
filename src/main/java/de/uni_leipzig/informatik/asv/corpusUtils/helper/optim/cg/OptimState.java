/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg;

public class OptimState {
	
	private int iter;
	private double step;
	private double maxStep;
	private double tol;
	private double[] x1;
	private double[] dx1;
	private double[] x2;
	private double pNorm;
	private double[] p;
	private double g0Norm;
	private double[] g0;
	
	public int getIter() {
		return iter;
	}
	public void setIter(int iter) {
		this.iter = iter;
	}
	public double getStep() {
		return step;
	}
	public void setStep(double step) {
		this.step = step;
	}
	public double getMaxStep() {
		return maxStep;
	}
	public void setMaxStep(double maxStep) {
		this.maxStep = maxStep;
	}
	public double getTol() {
		return tol;
	}
	public void setTol(double tol) {
		this.tol = tol;
	}
	public double getpNorm() {
		return pNorm;
	}
	public void setpNorm(double pNorm) {
		this.pNorm = pNorm;
	}
	public double getG0Norm() {
		return g0Norm;
	}
	public void setG0Norm(double g0Norm) {
		this.g0Norm = g0Norm;
	}
	public double[] getX1() {
		return x1;
	}
	public double[] getDx1() {
		return dx1;
	}
	public double[] getX2() {
		return x2;
	}
	public double[] getP() {
		return p;
	}
	public double[] getG0() {
		return g0;
	}
	
	
	
	public void setX1(double[] x1) {
		this.x1 = x1;
	}
	public void setDx1(double[] dx1) {
		this.dx1 = dx1;
	}
	public void setX2(double[] x2) {
		this.x2 = x2;
	}
	public void setP(double[] p) {
		this.p = p;
	}
	public void setG0(double[] g0) {
		this.g0 = g0;
	}
	public void allocate(int n) {
		x1 = new double[n];
		dx1 = new double[n];
		x2 = new double[n];
		p = new double[n];
		g0 = new double[n];
	}
	
	
}
