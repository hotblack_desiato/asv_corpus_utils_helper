/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.sparsevb;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class LogLikelihoodOptimizeParameters {
	
	private double noiseLevel, detSigmaQ, traceKQ, jitter;
	private RealMatrix Knn, Knm, Kmm, KmmInv, Qnn, SigmaQInv, y;
	private RealMatrix X, Xm;
	private RealMatrix dKmn, dKnm, dKmm;
	private SparseGaussianProcessVB model;
	private RealMatrix Kmn;

	public LogLikelihoodOptimizeParameters(SparseGaussianProcessVB _model) {
		this.model = _model;
	}
	
	public double getNoiseLevel() {
		return this.noiseLevel;
	}

	public RealMatrix getKnn() {
		return this.Knn;
	}
	public void setKnn(RealMatrix knn) {
		this.Knn = knn;
	}
	public RealMatrix getKnm() {
		return this.Knm;
	}

	public RealMatrix getY() {
		return this.y;
	}

	public void setY(RealMatrix _y) {
		this.y = _y;
	}
	
	public RealMatrix getKmm() {
		return this.Kmm;
	}

	
	public double getDetSigmaQ() {
		return this.detSigmaQ;
	}


	public RealMatrix getKmmInv() {
		return this.KmmInv;
	}


	public RealMatrix getQnn() {
		return this.Qnn;
	}


	public RealMatrix getSigmaQInv() {
		return this.SigmaQInv;
	}

	public double getTraceKQ() {
		return this.traceKQ;
	}
	
	public RealMatrix getX() {
		return this.X;
	}

	public void setX(RealMatrix x) {
		this.X = x;
	}

	public RealMatrix getXm() {
		return this.Xm;
	}

	public void setXm(RealMatrix xm) {
		this.Xm = xm;
	}

	public RealMatrix getdKmn() {
		return this.dKmn;
	}

	public RealMatrix getdKnm() {
		return this.dKnm;
	}

	
	
	public double getJitter() {
		return this.jitter;
	}

	public void setJitter(double _jitter) {
		this.jitter = _jitter;
	}

	public SparseGaussianProcessVB getModel() {
		return this.model;
	}

	public void doComputations() {
		this.Knm = MatrixUtils.createRealMatrix(this.model.getCovFunction().computeCrossCovarianceMatrix(this.X.getData(), this.Xm.getData()));
		this.Kmn = this.Knm.copy().transpose();

		this.Kmm = MatrixUtils.createRealMatrix(this.model.getCovFunction().computeCovarianceMatrix(this.Xm.getData()));
		
		//add jitter
		RealMatrix jitterMatrix = MatrixUtils.createRealIdentityMatrix(this.Xm.getRowDimension()).scalarMultiply(this.jitter);
		this.KmmInv = new LUDecomposition(this.Kmm.add(jitterMatrix)).getSolver().getInverse();

		this.Qnn = this.Knm.multiply(this.KmmInv).multiply(this.Kmn);

		RealMatrix noiseInv = MatrixUtils.createRealIdentityMatrix(this.X.getRowDimension()).scalarMultiply(1./this.noiseLevel);
		RealMatrix tmp = this.Kmm.add(this.Kmn.multiply(noiseInv).multiply(this.Knm));
		RealMatrix tmpInv = new LUDecomposition(tmp, 1e-50).getSolver().getInverse();
		
		RealMatrix noiseQnnInv = noiseInv.subtract(noiseInv.multiply(this.Knm).multiply(tmpInv).multiply(this.Kmn).multiply(noiseInv));
		
		RealMatrix noise = MatrixUtils.createRealIdentityMatrix(this.X.getRowDimension()).scalarMultiply(this.noiseLevel);
		LUDecomposition chol = new LUDecomposition(this.Qnn.add(noise));

		this.detSigmaQ = chol.getDeterminant();
		this.SigmaQInv = noiseQnnInv;
		this.traceKQ = this.Knn.subtract(this.Qnn).getTrace();
		
		double[][][] crossCovDx = this.model.getCovFunction().computeCrossCovarianceMatrixDerivX(this.Xm.getData(), this.X.getData());
		double[][][] crossCovDxPrime = this.model.getCovFunction().computeCrossCovarianceMatrixDerivXPrime(this.getX().getData(), this.getXm().getData());
		
		this.dKmn = MatrixUtils.createRealMatrix(extract2DArray(crossCovDx));
		this.dKnm = MatrixUtils.createRealMatrix(extract2DArray(crossCovDxPrime));

	}
	
	private double[][] extract2DArray(double[][][] _in) {
		double[][] out = new double[_in.length][];
		for(int i=0;i<_in.length;i++) {
			out[i] = new double[_in[i].length];
			for(int j=0;j<_in[i].length;j++) {
				out[i][j] = _in[i][j][0];
			}
		}
		return out;
	}
	
}
