/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.add;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.diag;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.multiplyWithScalar;

import java.util.Vector;

import org.apache.commons.lang3.ArrayUtils;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class CovarianceFunctionSum extends CovarianceFunction {

	private CovarianceFunction[] summands;
	private double s2;
	private double[] weights;

	public CovarianceFunctionSum(double noise, double var, CovarianceFunction[] factors, double[] _weights) {
		super(noise);
		this.s2 = var;
		this.summands = factors;
		this.weights = _weights;
	}

	/**
	 * @param noise
	 */
	public CovarianceFunctionSum(double noise, double var, CovarianceFunction[] factors) {
		super(noise);
		this.s2 = var;
		this.summands = factors;
		this.weights = new double[this.summands.length];
		for(int i=0;i<this.weights.length;i++) 
			this.weights[i] = 1./this.weights.length;
	}
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovariance(double[], double[])
	 */
	@Override
	public double[][] computeCovarianceMatrix(double[][] a) {
		// TODO: make sure factor covariance is one
//		for(CovarianceFunction f : factors)
//			f;
		double[][] cov = null;
		for(int i =0;i<this.summands.length;i++) {
			if(i == 0) {
				cov = multiplyWithScalar(this.summands[i].computeCovarianceMatrix(a), this.weights[i]);
//				cov = divideByScalar(cov, scalar)
			} else {
				double[][] covFactor = multiplyWithScalar(this.summands[i].computeCovarianceMatrix(a), this.weights[i]);
				cov = add(cov, covFactor);
			}
		}
		cov = multiplyWithScalar(cov, this.s2);
		cov = add(diag(multiplyWithScalar(ArrayFactory.doubleArray(de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type.ONE, a.length), this.noiseLevel)), cov);
		this.trainingDataCovariance = cov;
		return cov;
	}

	
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#computeCrossCovarianceMatrix(double[][], double[][])
	 */
	@Override
	public double[][] computeCrossCovarianceMatrix(double[][] unseenPoints,
			double[][] trainingPoints) {
		double[][] cov = null;
		for(int i =0;i<this.summands.length;i++) {
			if(i == 0)
				cov = multiplyWithScalar(this.summands[i].computeCrossCovarianceMatrix(unseenPoints, trainingPoints), this.weights[i]);
			else {
				double[][] covFactor = multiplyWithScalar(this.summands[i].computeCrossCovarianceMatrix(unseenPoints, trainingPoints), this.weights[i]);
				cov = add(cov, covFactor);
			}
		}
		return cov;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivXPrime(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivXPrime(double[] a, double[] b) {
		throw new UnsupportedOperationException("not implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivX(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivX(double[] a, double[] b) {
		throw new UnsupportedOperationException("not implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#getHyperparameters()
	 */
	@Override
	public double[] getHyperparameters() {
		Vector<Double> hyperparams = new Vector<>();
		for(CovarianceFunction f : this.summands) {
			double[] fHypers = f.getHyperparameters();
			for(double fHyper : fHypers)
				hyperparams.add(Double.valueOf(fHyper));
		}
		return ArrayUtils.toPrimitive(hyperparams.toArray(new Double[]{}));
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#setHyperparameters(double[])
	 */
	@Override
	public void setHyperparameters(double[] newHyperparams) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#getHyperparameterObjectiveFunction(de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess)
	 */
	@Override
	protected ObjectiveFunction getHyperparameterObjectiveFunction(
			AbstractGaussianProcess gp) {
		throw new UnsupportedOperationException("not implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovariance(double[], double[])
	 */
	@Override
	public double computeCovariance(double[] a, double[] b) {
		// TODO Auto-generated method stub
		return 0;
	}

}
