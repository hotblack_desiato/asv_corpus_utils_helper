/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.optim;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;


/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public abstract class Optimizer {
	protected double f;
	protected double[] x;
	protected double[] gradient;
	protected double[] dx;
	protected ObjectiveFunction func;
	
	public static boolean debug = false;

	public enum Result {
		SUCCESS, ENOPROG, CONTINUE
	}
	
	public static <O extends Optimizer> O getOptimizer(Class<O> optimizerClass) {
		try {
			return optimizerClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void optimize(double[] guess, double stepSize, double tolerance, double convergenceThreshold, int maxIters, ObjectiveFunction objectiveFunction, Optimizer opt) {
		opt.allocate(guess.length);
		opt.set(objectiveFunction, guess, stepSize, tolerance);
		double oldValue = 1e200;
		double curValue, convergence = 1;
		int iter = 0;
		Result res = Result.CONTINUE;
		while(res != Result.SUCCESS && iter <= maxIters) {
			res = opt.iterate();
			res = opt.testGradient(convergenceThreshold);
			curValue = opt.getF();
			convergence = (oldValue-curValue)/oldValue;
			if(debug) {
				System.out.println("at iteration " + iter + ", convergence = " + convergence);
			}
			if(oldValue - curValue < 5 && Math.abs(convergence) < convergenceThreshold)
				break;
			oldValue = curValue;
			iter++;
		}
	}
	
	public void allocate(int n) {
		this.x = new double[n];
		this.gradient = new double[n];
		this.dx = new double[n];
	}
	
	public abstract void set(ObjectiveFunction o, double[] guess, double stepSize, double tolerance);
	
	public abstract Result iterate();
	
	public Result testGradient(double optConverged) {
		double norm;
		norm = VectorUtils.norm(this.gradient);
		if (norm < optConverged)
			return Result.SUCCESS;
		return Result.CONTINUE;

	}
	
	public double getF() {
		return this.f;
	}
	
	public double[] getX() {
		return this.x;
	}

	public double[] getGradient() {
		return this.gradient;
	}
}
