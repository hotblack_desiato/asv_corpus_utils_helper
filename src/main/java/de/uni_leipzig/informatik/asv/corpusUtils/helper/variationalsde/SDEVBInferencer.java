/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.omg.PortableInterceptor.SUCCESSFUL;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer.Result;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.rprop.RPropOptimizer;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick
 *         Jaehnichen</a>
 * 
 */
public class SDEVBInferencer {

	// the prior SDE, does not need to have linear drift
	private StochasticDifferentialEquation prior;
	// the number of available measurements, i.e. the number of timestamps
	private int T;
	// the time infinitesimal
	private double dt;
	// the number of infinitesimal time steps
	private int K;
	// the dimensionality of the process
	private int dim;
	// the data
	private double[] obsTimes;
	private RealMatrix[] inputData;
	private RealMatrix[] targetData;
	private HashMap<Integer, Integer> observationMapping;

	// lagrange multiplier for constraint optimization
	private RealMatrix[] lagrangeMultiplierLambda;
	private RealMatrix[] lagrangeMultiplierPsi;
	private RealMatrix[] lagrangeMultiplierLambdaDerivT;
	private RealMatrix[] lagrangeMultiplierPsiDerivT;

	// the variational function parameters defining the posterior drift function
	private RealMatrix[] variationalFunctionA;
	private RealMatrix[] variationalFunctionB;

	// the posterior process mean and variance
	private RealMatrix[] posteriorMean;
	private RealMatrix[] posteriorVariance;
	
	//the prior on x_0
	private RealMatrix m0;
	private RealMatrix var0;

	// measurement noise
	private RealMatrix measurementNoise;
	private boolean measurementNoiseChanged = true;
	private double measurementDeterminant;

	// optimizers
	private Optimizer sMinA;
	private SDESlopeObjective aObjective;
	private Optimizer sMinb;
	private SDEAbsolutePartObjective bObjective;
	private SDEOptimizeParameters optParams;
	private RealMatrix measurementNoiseInv;
	private double omega = .005;
	private RealMatrix invVar0;

	public SDEVBInferencer(StochasticDifferentialEquation _prior) {
		this.prior = _prior;
	}

	public void setTrainingData(double[] times, double[][] input,
			double[][] targets, double _dt, int _K, double r) {
		this.obsTimes = times;
		this.dim = input[0].length;
		this.T = this.obsTimes.length;
//		this.K = (int) ((this.obsTimes[this.T - 1] - this.obsTimes[0]) / this.dt);
		this.dt = _dt;
		this.K = _K;

		// create matrix arrays
		this.inputData = new RealMatrix[this.T];
		this.targetData = new RealMatrix[this.T];
		
		for (int t = 0; t < this.T; ++t) {
			this.inputData[t] = MatrixUtils.createColumnRealMatrix(input[t]);
			this.targetData[t] = MatrixUtils.createColumnRealMatrix(targets[t]);
		}

		
		this.lagrangeMultiplierLambda = new RealMatrix[this.K + 1];
		this.lagrangeMultiplierPsi = new RealMatrix[this.K + 1];
		this.lagrangeMultiplierLambdaDerivT = new RealMatrix[this.K];
		this.lagrangeMultiplierPsiDerivT = new RealMatrix[this.K];

		this.variationalFunctionA = new RealMatrix[this.K + 1];
		this.variationalFunctionB = new RealMatrix[this.K + 1];

		this.posteriorMean = new RealMatrix[this.K + 1];
		this.posteriorVariance = new RealMatrix[this.K + 1];
		
		this.posteriorMean[0] = MatrixUtils.createColumnRealMatrix(ArrayFactory.doubleArray(Type.NORMAL, this.dim)).scalarMultiply(.001).add(this.targetData[0]);
		System.out.println("posterior mean at t=0: " + this.posteriorMean[0]);
		for(int k=0;k<=this.K;k++) {
			this.lagrangeMultiplierLambda[k] = MatrixUtils.createColumnRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.dim)).scalarMultiply(.001);
			this.lagrangeMultiplierPsi[k] = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.dim, this.dim)).scalarMultiply(.001);
			
			if(k<this.K) {
				this.lagrangeMultiplierLambdaDerivT[k] = MatrixUtils.createColumnRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.dim)).scalarMultiply(.1);
				this.lagrangeMultiplierPsiDerivT[k] = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.dim, this.dim)).scalarMultiply(.1);
			}
			
			this.variationalFunctionA[k] = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.UNIFORM, this.dim, this.dim));//.scalarMultiply(.1);
			this.variationalFunctionB[k] = MatrixUtils.createColumnRealMatrix(ArrayFactory.doubleArray(Type.UNIFORM, this.dim));//.scalarMultiply(.1);
			if(k>0)
				this.posteriorMean[k] = MatrixUtils.createColumnRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.dim)).scalarMultiply(.01).add(this.posteriorMean[k-1]);
			this.posteriorVariance[k] = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.dim, this.dim)).scalarAdd(.5);
		}
		
		double scalarVar0 = 5;
		this.m0 = MatrixUtils.createColumnRealMatrix(new double[dim]);
		this.var0 = MatrixUtils.createRealIdentityMatrix(dim).scalarMultiply(scalarVar0);
		this.invVar0 = MatrixUtils.createRealIdentityMatrix(dim).scalarMultiply(1./scalarVar0);
		
		this.observationMapping = new HashMap<>();
		int curObsTimeIdx = 0;
		for(int k=0;k<=this.K;k++) {
			if(this.obsTimes[curObsTimeIdx] <= this.obsTimes[0]+k*this.dt) {
				this.observationMapping.put(k, curObsTimeIdx);
				this.posteriorMean[k].setColumn(0, input[curObsTimeIdx]);
				curObsTimeIdx++;
				if(curObsTimeIdx >= this.T)
					break;
			}
		}
		
		this.measurementNoise = MatrixUtils.createRealIdentityMatrix(this.dim);
		this.measurementNoise = this.measurementNoise.scalarMultiply(r);
	}

	public void train() throws IOException {
		train(false);
	}
	
	public void train(boolean doLogging) throws IOException {
		int optimizeMaxIter = 30;
		int iter = 0;
		// initialize optimizers for variational functions
		initOptimizers();

		double objective = 0, objectiveOld = 1e100;
		while (iter < optimizeMaxIter) {
			iter++;
			objective = doTrainingIteration(iter);
			if ((Math.abs((objective - objectiveOld) / objectiveOld) < 1e-3
						&& Math.abs(objective - objectiveOld) < 5.) 
					|| objective > objectiveOld || objective < 0)
				break;
			objectiveOld = objective;
			if(doLogging) {
				try {
					logResults(iter);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("objective(" + iter + ") : " + objective); 
		}

	}

	/**
	 * @param iter
	 * @throws IOException 
	 */
	private void logResults(int iter) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter("/Users/patrick/Data/sde/log_iter" + iter + ".txt"));
		double mean, var, a, b, lambda, psi;
		for(int k=0;k<=this.K;k++) {
			mean = this.posteriorMean[k].getEntry(0, 0);
			var = this.posteriorVariance[k].getEntry(0, 0);
			a = this.variationalFunctionA[k].getEntry(0, 0);
			b = this.variationalFunctionB[k].getEntry(0, 0);
			lambda = this.lagrangeMultiplierLambda[k].getEntry(0, 0);
			psi = this.lagrangeMultiplierPsi[k].getEntry(0, 0);
			out.write(k*this.dt + "\t" + mean + "\t" + (mean + 2*Math.sqrt(var)) + "\t" + (mean - 2*Math.sqrt(var)) + "\t" + a + "\t" + b + "\t" + lambda + "\t" + psi + "\n");
		}
		out.close();
	}

	/**
	 * @return
	 * @throws IOException 
	 */
	public double doTrainingIteration(int i) throws IOException {
		doForwardPropagation();
		doBackwardPropagation();
		doOptimizationStep();
//		updateInitialValues();
		
		writeToFile(i);
		
		return computeLagrangian();
	}


	/**
	 * @throws IOException 
	 * 
	 */
	private void writeToFile(int i) throws IOException {
		double[][] mean = getPredictiveMeanInDouble();
		double[][][] var = getPredictiveVarianceInDouble();
		double[][][] a = getVariationalFunctionAInDouble();
		double[][] b = getVariationalFunctionBInDouble();
		double[][][] psi = getLagrangeMultiplierPsiInDouble();
		double[][] lambda = getLagrangeMultiplierLambdaInDouble();
		BufferedWriter out = new BufferedWriter(new FileWriter("/Users/patrick/Data/sde/prediction" + i));
		for(int k=0;k<K;k++) {
			out.write(k*dt + "\t" + mean[k][0] + "\t" + (mean[k][0] + 2*Math.sqrt(var[k][0][0])) + "\t" + (mean[k][0] - 2*Math.sqrt(var[k][0][0])) + "\t" + a[k][0][0] + "\t" + b[k][0] + "\t" + lambda[k][0] + "\t" + psi[k][0][0] + "\n");
		}
		out.close();
	}

	/**
	 * 
	 */
	private void updateInitialValues() {
		RealMatrix derivM0 = lagrangeMultiplierLambda[0].add(invVar0.multiply(posteriorMean[0].subtract(m0)));
		RealMatrix derivS0 = lagrangeMultiplierPsi[0].add(invVar0.subtract(posteriorVariance[0]).scalarMultiply(.5));
		
		System.out.println("before : " + posteriorMean[0]);
		
		//																|| check whether to add or subtract the gradient
		posteriorMean[0] = posteriorMean[0].subtract( posteriorMean[0].add(derivM0).scalarMultiply(omega) );
		posteriorVariance[0] = posteriorVariance[0].subtract( posteriorVariance[0].add(derivS0).scalarMultiply(omega) );
		
		System.out.println("after : " + posteriorMean[0]);
		System.out.println("---");
	}

	public RealMatrix[] getPredictiveMean() {
//		doForwardPropagation();
		return this.posteriorMean;
	}

	
	public RealMatrix[] getPredictiveVariance() {
//		doForwardPropagation();
		return this.posteriorVariance;
	}

	public RealMatrix[] getVariationalFunctionA() {
		return this.variationalFunctionA;
	}

	public RealMatrix[] getVariationalFunctionB() {
		return this.variationalFunctionB;

	}
	/**
	 * @return the lagrangeMultiplierLambda
	 */
	public RealMatrix[] getLagrangeMultiplierLambda() {
		return this.lagrangeMultiplierLambda;
	}

	/**
	 * @return the lagrangeMultiplierPsi
	 */
	public RealMatrix[] getLagrangeMultiplierPsi() {
		return this.lagrangeMultiplierPsi;
	}

	public double[][][] getVariationalFunctionAInDouble() {
		double[][][] ret = new double[this.variationalFunctionA.length][][];
		for(int i=0;i<this.variationalFunctionA.length;i++)
			ret[i] = this.variationalFunctionA[i].getData();
		return ret;
	}

	public double[][] getVariationalFunctionBInDouble() {
		double[][] ret = new double[this.variationalFunctionB.length][];
		for(int i=0;i<this.variationalFunctionB.length;i++) {
			ret[i] = this.variationalFunctionB[i].getColumn(0);
		}
		return ret;
	}
	/**
	 * @return the lagrangeMultiplierLambda
	 */
	public double[][] getLagrangeMultiplierLambdaInDouble() {
		double[][] ret = new double[this.lagrangeMultiplierLambda.length][];
		for(int i=0;i<this.lagrangeMultiplierLambda.length;i++) {
			ret[i] = this.lagrangeMultiplierLambda[i].getColumn(0);
		}
		return ret;
	}

	/**
	 * @return the lagrangeMultiplierPsi
	 */
	public double[][][] getLagrangeMultiplierPsiInDouble() {
		double[][][] ret = new double[this.lagrangeMultiplierPsi.length][][];
		for(int i=0;i<this.lagrangeMultiplierPsi.length;i++)
			ret[i] = this.lagrangeMultiplierPsi[i].getData();
		return ret;
	}
	
	public double[][] getPredictiveMeanInDouble() {
		double[][] ret = new double[this.posteriorMean.length][];
		for(int i=0;i<this.posteriorMean.length;i++) {
			ret[i] = this.posteriorMean[i].getColumn(0);
		}
		return ret;
	}
	
	public double[][][] getPredictiveVarianceInDouble() {
		double[][][] ret = new double[this.posteriorVariance.length][][];
		for(int i=0;i<this.posteriorVariance.length;i++)
			ret[i] = this.posteriorVariance[i].getData();
		return ret;
	}
	
	/**
	 * @return the prior
	 */
	public StochasticDifferentialEquation getPrior() {
		return prior;
	}

	//// COMPUTATIONS ////
	
	void setVariationalFunctionA(RealMatrix _in, int k) {
		this.variationalFunctionA[k] = _in;
	}
	
	void setVariationalFunctionB(RealMatrix _in, int k) {
		this.variationalFunctionB[k] = _in;
	}

	/**
	 * @param variationalFunctionA the variationalFunctionA to set
	 */
	void setVariationalFunctionA(double[] variationalFunctionA, int k) {
		this.variationalFunctionA[k] = createRealMatrixFromList(variationalFunctionA, this.dim, this.dim);
	}

	/**
	 * @param variationalFunctionB the variationalFunctionB to set
	 */
	void setVariationalFunctionB(double[] variationalFunctionB, int k) {
		this.variationalFunctionB[k] = MatrixUtils.createColumnRealMatrix(variationalFunctionB);
	}

	/**
	 * 
	 */
	private void doBackwardPropagation() {
		for(int k=this.K;k>=1;k--) {
			RealMatrix energyDeriv = this.prior.energyDerivativeMean(this.posteriorMean[k], this.posteriorVariance[k], this.variationalFunctionB[k], this.variationalFunctionA[k], k);
			this.lagrangeMultiplierLambdaDerivT[k-1] = this.variationalFunctionA[k].transpose().multiply(this.lagrangeMultiplierLambda[k]).subtract(energyDeriv);
			this.lagrangeMultiplierLambda[k-1] = this.lagrangeMultiplierLambda[k].subtract(this.lagrangeMultiplierLambdaDerivT[k-1].scalarMultiply(this.dt));
			
			energyDeriv = this.prior.energyDerivativeVariance(this.posteriorMean[k], this.posteriorVariance[k], this.variationalFunctionB[k], this.variationalFunctionA[k], k);
			this.lagrangeMultiplierPsiDerivT[k-1] = this.lagrangeMultiplierPsi[k].multiply(this.variationalFunctionA[k].scalarMultiply(2)).subtract(energyDeriv);
			this.lagrangeMultiplierPsi[k-1] = this.lagrangeMultiplierPsi[k].subtract(this.lagrangeMultiplierPsiDerivT[k-1].scalarMultiply(this.dt));
			
			if(this.observationMapping.get(k-1) != null) {
				this.lagrangeMultiplierLambda[k-1] = this.lagrangeMultiplierLambda[k-1].add(computeObservationEnergyDerivativeMean(k-1));
				this.lagrangeMultiplierPsi[k-1] = this.lagrangeMultiplierPsi[k-1].add(computeObservationEnergyDerivativeVariance(k-1));
			}
		}
	}

	/**
	 * 
	 */
	private void doOptimizationStep() {
		double[] curValA, curValB;
		double[] grad;
		double f, oldF = 1e200, convergence = 1;
		int maxIter = 10, iter;
		RealMatrix gradMat;
		Result res = Result.CONTINUE;
		for (int k = 0; k <= this.K; k++) {
			this.optParams.setTime(k);
			curValA = create1DArray(this.variationalFunctionA[k]);
			curValB = create1DArray(this.variationalFunctionB[k]);
			sMinA.allocate(curValA.length);
			sMinb.allocate(curValB.length);
			while(res != Result.SUCCESS && convergence > 1e-3) {
			
				sMinA.set(aObjective, curValA, .35, 1e-6);
				sMinA.iterate();
				f = sMinA.getF();
				curValA = sMinA.getX();
				this.variationalFunctionA[k] = createRealMatrixFromList(this.sMinA.getX(), dim, dim);
	
				sMinb.set(bObjective, curValB, .35, 1e-6);
				sMinb.iterate();
				f += sMinb.getF();
				curValB = sMinb.getX();
				
				this.variationalFunctionB[k] = createRealMatrixFromList(this.sMinb.getX(), dim, 1);
				convergence = Math.abs((oldF - f)/oldF);
				System.out.println("f = " + f + ", oldF = " + oldF + ", convergence: " + convergence);
				oldF = f;
			}	
		}

	}

	/**
	 * @return
	 */
	public double computeLagrangian() {
		double objective = computeVariationalFreeEnergy();
		objective -= computeLagrangianConstraints();
		objective += this.lagrangeMultiplierLambda[0].transpose().multiply(this.posteriorMean[0]).getTrace();
		objective += this.lagrangeMultiplierPsi[0].multiply(this.posteriorVariance[0]).getTrace();
		return objective;
	}
	
	public double computeLagrangian(int k) {
		double variationalEnergy = computeSDEEnergy(k)+computeObservationEnergy(k);
		double lagrangian = computeLagrangianConstraints(k);
		return (variationalEnergy - lagrangian)*this.dt;
	}

	/**
	 * @return
	 */
	private double computeLagrangianConstraints() {
		double lObjective = 0;
		for(int k=1;k<=this.K;k++) {
			lObjective += computeLagrangianConstraints(k)*this.dt;
		}
		return lObjective;
	}

	private double computeLagrangianConstraints(int k) {
		//lambda part
		RealMatrix derivSdeM = this.prior.energyDerivativeMean(this.posteriorMean[k], this.posteriorVariance[k], this.variationalFunctionB[k], this.variationalFunctionA[k], k);
		RealMatrix dLambdaDt = this.variationalFunctionA[k].transpose().multiply(this.lagrangeMultiplierLambda[k]).subtract(derivSdeM);
		
		RealMatrix part1 = this.variationalFunctionA[k].multiply(this.posteriorMean[k]).subtract(this.variationalFunctionB[k]);
		part1 = this.lagrangeMultiplierLambda[k].transpose().multiply(part1);
		if(k != this.K) {
			part1 = part1.subtract(this.lagrangeMultiplierLambdaDerivT[k].transpose().multiply(this.posteriorMean[k]));
//			part1 = part1.subtract(dLambdaDt.transpose().multiply(this.posteriorMean[k]));
		}
		
		//psi part
		RealMatrix derivSdeS = this.prior.energyDerivativeVariance(this.posteriorMean[k], this.posteriorVariance[k], this.variationalFunctionB[k], this.variationalFunctionA[k], k);
		RealMatrix dPsiDt = this.lagrangeMultiplierPsi[k].multiply(this.variationalFunctionA[k]).scalarMultiply(2).subtract(derivSdeS);
		
		RealMatrix part2 = this.lagrangeMultiplierPsi[k].multiply(this.variationalFunctionA[k].multiply(this.posteriorVariance[k]).scalarMultiply(2).subtract(this.prior.getNoise()));
		if(k != this.K) {
			part2 = part2.subtract(this.lagrangeMultiplierPsiDerivT[k].multiply(this.posteriorVariance[k]));
//			part2 = part2.subtract(dPsiDt.multiply(this.posteriorVariance[k]));
		}
		return part1.getTrace() + part2.getTrace();
	}
	
	public RealMatrix computeLagrangianDerivativeB(int k) {
		return computeSDEEnergyDerivativeB(k).add(this.lagrangeMultiplierLambda[k]);
	}
	
	public RealMatrix computeLagrangianDerivativeA(int k, RealMatrix sdeEnergyDerivativeB) {
		RealMatrix eSdeDerivA = computeSDEEnergyDerivativeA(k, sdeEnergyDerivativeB);
		RealMatrix lambdaMeanT = this.lagrangeMultiplierLambda[k].multiply(this.posteriorMean[k].transpose());
		RealMatrix psiVarTwo = this.lagrangeMultiplierPsi[k].multiply(this.posteriorVariance[k]).scalarMultiply(2);
		
		return eSdeDerivA.subtract(lambdaMeanT).subtract(psiVarTwo);
	}

	/**
	 * 
	 */
	private void doForwardPropagation() {
		for (int k = 0; k < this.K; k++) {
			RealMatrix meanChange = (this.variationalFunctionA[k].multiply(this.posteriorMean[k])
										.subtract(this.variationalFunctionB[k]))
									.scalarMultiply(this.dt); 
			this.posteriorMean[k+1] = this.posteriorMean[k].subtract(meanChange);
			RealMatrix varChange = (this.variationalFunctionA[k].multiply(this.posteriorVariance[k])
										.add(this.posteriorVariance[k].multiply(this.variationalFunctionA[k].transpose()))
										.subtract(this.prior.getNoise()))
									.scalarMultiply(this.dt);
			this.posteriorVariance[k+1] = this.posteriorVariance[k].subtract(varChange);
		}
	}

	private void initOptimizers() {
		if (this.sMinA != null && this.sMinb != null)
			return;
//		this.sMinA = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
//		this.sMinb = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
		this.sMinA = Optimizer.getOptimizer(RPropOptimizer.class);
		this.sMinb = Optimizer.getOptimizer(RPropOptimizer.class);
		this.optParams = new SDEOptimizeParameters();
		this.optParams.setDimensions(this.dim);
		this.optParams.setInf(this);
		this.aObjective = new SDESlopeObjective(this.dim*this.dim, this.optParams);
		this.bObjective = new SDEAbsolutePartObjective(this.dim, this.optParams);
	}

	private double computeVariationalFreeEnergy() {
		double energy = 0;
		double localEnergy = 0;
		for (int k = 1; k <= this.K; k++) {
			localEnergy = computeSDEEnergy(k);
			localEnergy += computeObservationEnergy(k);
			energy += localEnergy*this.dt;
		}
		return energy;
	}

	private double computeVariationalFreeEnergy(
			RealMatrix measurementFirstMoment,
			RealMatrix measurementSecondMoment) {
		double energy = 0;
		for (int k = 1; k <= this.K; k++) {
			energy += computeSDEEnergy(k);
			energy += computeObservationEnergy(measurementFirstMoment,
					measurementSecondMoment, k);
		}
		return energy;
	}

	/**
	 * @return
	 */
	double computeSDEEnergy(int k) {
		RealMatrix term1 = this.prior.squaredDriftFunctionExpectation(this.posteriorMean[k], this.posteriorVariance[k], k);
		RealMatrix term2 = this.variationalFunctionA[k].multiply(this.prior.driftFunctionTimesXExpectation(this.posteriorMean[k], this.posteriorVariance[k], k)).scalarMultiply(2);
		RealMatrix term3 = this.prior.driftFunctionExpectation(this.posteriorMean[k], this.posteriorVariance[k], k).multiply(this.variationalFunctionB[k]).scalarMultiply(2);
		RealMatrix term4 = squaredLinearFunctionExpectation(k);
		
		RealMatrix finalMat = this.prior.getNoiseInv().multiply(term1.add(term2).subtract(term3).add(term4));
//		double term1 = this.prior
//				.squaredDriftFunctionExpectation(this.posteriorMean[k],
//						this.posteriorVariance[k], k)
//				.multiply(this.prior.getNoiseInv()).getTrace();
//		double term2 = this.prior
//				.driftFunctionExpectation(this.posteriorMean[k],
//						this.posteriorVariance[k], k)
//				.multiply(this.prior.getNoiseInv())
//				.multiply(linearDriftFunctionExpectation(k)).getTrace();
//		double term3 = squaredLinearFunctionExpectation(k).multiply(
//				this.prior.getNoiseInv()).getTrace();
		return .5 * (finalMat.getTrace());
	}

	private RealMatrix linearDriftFunctionExpectation(int k) {
		return this.variationalFunctionA[k].scalarMultiply(-1)
				.multiply(this.posteriorMean[k])
				.add(this.variationalFunctionB[k]);
	}

	private RealMatrix squaredLinearFunctionExpectation(int k) {
		RealMatrix eX2 = StochasticDifferentialEquation.normalSecondMoment(this.posteriorMean[k], this.posteriorVariance[k]);
		RealMatrix term1 = this.variationalFunctionA[k].multiply(eX2).multiply(this.variationalFunctionA[k].transpose());
		RealMatrix term2 = this.variationalFunctionA[k].multiply(this.posteriorMean[k].multiply(this.variationalFunctionB[k].transpose())).scalarMultiply(2);
		RealMatrix term3 = this.variationalFunctionB[k].multiply(this.variationalFunctionB[k].transpose());
		return term1.subtract(term2).add(term3);
	}

	double computeObservationEnergy(int k) {
		if(this.observationMapping.get(k) == null)
			return 0.;
		int i = this.observationMapping.get(k);
		double ret = 0;
		RealMatrix RInv = getInverseMeasurementNoise();
		RealMatrix yMinM = this.targetData[i].subtract(this.posteriorMean[k]);
		ret += .5 * (yMinM.transpose().multiply(RInv).multiply(yMinM)
				.getTrace() + this.posteriorVariance[k].multiply(RInv)
				.getTrace());
		ret += .5 * this.dim * Math.log(2 * Math.PI);
		ret += .5 * Math.log(this.measurementDeterminant);
		return ret;
	}

	double computeObservationEnergy(RealMatrix firstMoment,
			RealMatrix secondMoment, int k) {
		double ret = 0;
		RealMatrix RInv = getInverseMeasurementNoise();
		RealMatrix yMinM = firstMoment.subtract(this.posteriorMean[k]);
		ret += .5 * (yMinM.transpose().multiply(RInv).multiply(yMinM)
				.getTrace() + this.posteriorVariance[k].add(secondMoment)
				.multiply(RInv).getTrace());
		ret += .5 * this.dim * Math.log(2 * Math.PI);
		ret += .5 * Math.log(this.measurementDeterminant);
		return ret;
	}
	
	
	public void setMeasurementNoise(RealMatrix _measurementNoise) {
		this.measurementNoise = _measurementNoise;
		this.measurementNoiseChanged = true;
	}
	
	/**
	 * @return
	 */
	private RealMatrix getInverseMeasurementNoise() {
		if(this.measurementNoiseChanged) {
			CholeskyDecomposition cd = new CholeskyDecomposition(
					this.measurementNoise);
			this.measurementNoiseInv = cd.getSolver().getInverse();
			this.measurementDeterminant = cd.getDeterminant();
		}
		return this.measurementNoiseInv;
	}

	public RealMatrix computeSDEEnergyDerivativeA(int k, RealMatrix sdeEnergyDerivativeB) {
		
//		RealMatrix expFDerivX = this.prior.driftFunctionExpectedDerivativeX(posteriorMean[k], posteriorVariance[k], k);
//		RealMatrix term1 = this.prior.getNoiseInv().multiply(expFDerivX.add(variationalFunctionA[k])).multiply(posteriorVariance[k]);
//		return term1.subtract(sdeEnergyDerivativeB.multiply(posteriorMean[k].transpose()));
		
		RealMatrix eX2 = this.posteriorMean[k].multiply(this.posteriorMean[k].transpose()).add(this.posteriorVariance[k]);
		RealMatrix eFx = this.prior.driftFunctionTimesXExpectation(this.posteriorMean[k], this.posteriorVariance[k], k);
		
		RealMatrix matrixSum = eFx.add(this.variationalFunctionA[k].multiply(eX2)).subtract(this.variationalFunctionB[k].multiply(this.posteriorMean[k]));
		return this.prior.getNoiseInv().multiply(matrixSum);
		
		
		
//		RealMatrix term1 = this.prior.driftFunctionTimesXExpectation(this.posteriorMean[k], this.posteriorVariance[k], k);
//		RealMatrix m2 = this.posteriorMean[k].multiply(this.posteriorMean[k].transpose());
//		RealMatrix term2 = this.variationalFunctionA[k].multiply(m2.add(this.posteriorVariance[k]));
//		RealMatrix term3 = this.variationalFunctionB[k].multiply(this.posteriorMean[k].transpose());
//		RealMatrix finalTerm = term1.add(term2).subtract(term3);

//		RealMatrix sdeDerivb = computeSDEEnergyDerivativeB(k);
//		RealMatrix middleTerm = this.prior.driftFunctionExpectedDerivativeX(this.posteriorMean[k], this.posteriorVariance[k], k).add(this.variationalFunctionA[k]);
//		RealMatrix firstTerm = this.prior.getNoiseInv().multiply(middleTerm).multiply(this.posteriorVariance[k]);
//		RealMatrix secondTerm = sdeDerivb.multiply(this.posteriorMean[k].transpose());
//		
//		RealMatrix sol1 = firstTerm.subtract(secondTerm);
//		RealMatrix sol2 = this.prior.getNoiseInv().multiply(finalTerm); 
		
//		System.out.println(sol1.subtract(sol2));
		
//		return sol2;
		
	}

	public RealMatrix computeSDEEnergyDerivativeB(int k) {
		RealMatrix expectedF = this.prior.driftFunctionExpectation(this.posteriorMean[k], this.posteriorVariance[k], k);
		RealMatrix middleTerm = this.variationalFunctionA[k].multiply(this.posteriorMean[k]);
		return this.prior.getNoiseInv().multiply(expectedF.scalarMultiply(-1).subtract(middleTerm).add(this.variationalFunctionB[k]));
	}


	/**
	 * @param i
	 * @return
	 */
	public RealMatrix computeObservationEnergyDerivativeMean(int k) {
		int dataIndex = this.observationMapping.get(k);
		return getInverseMeasurementNoise().multiply(this.posteriorMean[k].subtract(this.targetData[dataIndex]));
	}

	
	/**
	 * @param i
	 * @return
	 */
	public RealMatrix computeObservationEnergyDerivativeVariance(int i) {
		return getInverseMeasurementNoise().scalarMultiply(.5);
	}
	
	public static double[] create1DArray(RealMatrix _in) {
		double[] ret = new double[_in.getRowDimension()*_in.getColumnDimension()];
		double[][] data = _in.getData();
		for(int row = 0;row<_in.getRowDimension();row++)
			System.arraycopy(data[row], 0, ret, row*data[row].length, data[row].length);
		return ret;
	}
	
	public static RealMatrix createRealMatrixFromList(double[] _in, int rows, int cols) {
		double[][] data = new double[rows][cols];
		for(int row=0;row<rows;row++)
			System.arraycopy(_in, row*cols, data[row], 0, cols);
		return MatrixUtils.createRealMatrix(data);
	}

}
