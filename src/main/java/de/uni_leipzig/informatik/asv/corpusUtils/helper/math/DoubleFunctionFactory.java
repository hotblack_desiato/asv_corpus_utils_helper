/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.math;

import cern.colt.function.DoubleFunction;
import cern.jet.random.Gamma;

public class DoubleFunctionFactory {
	
	public static final DoubleFunction digamma = new DoubleFunction() {
		@Override
		public double apply(double arg0) {
			return MathUtils.digamma(arg0);
		}
	};
	
	public static final DoubleFunction loggamma = new DoubleFunction() {
		@Override
		public double apply(double arg0) {
			return MathUtils.loggamma(arg0);
		}
	};
	
	public static final DoubleFunction gammaRand = new DoubleFunction() {
		@Override
		public double apply(double arg0) {
			return Gamma.staticNextDouble(100., 100.);
		}
	};
	
	public static final DoubleFunction increment = new DoubleFunction() {
		@Override
		public double apply(double arg0) {
			return arg0+1;
		}
	};

	public static final DoubleFunction decrement = new DoubleFunction() {
		@Override
		public double apply(double arg0) {
			return arg0-1;
		}
	};

	
}
