/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.periodic;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.*;
import java.util.Vector;

import org.apache.commons.math3.analysis.function.Subtract;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class PeriodicRBFKernel extends CovarianceFunction {

	private double sigma;
	private double lengthScale;

	/**
	 * @param noise
	 */
	public PeriodicRBFKernel(double noise, double sigma, double lengthScale) {
		super(noise);
		this.sigma = sigma;
		this.lengthScale = lengthScale;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovariance(double[], double[])
	 */
	@Override
	public double computeCovariance(double[] a, double[] b) {
		double d = norm(subtract(a, b));
		if(d==0)
			return sigma;
		return sigma*Math.exp(-2*Math.pow(Math.sin(.5*d), 2)/(lengthScale*lengthScale) );
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivXPrime(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivXPrime(double[] a, double[] b) {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivX(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivX(double[] a, double[] b) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#getHyperparameters()
	 */
	@Override
	public double[] getHyperparameters() {
		return new double[]{noiseLevel, sigma, lengthScale};
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#setHyperparameters(double[])
	 */
	@Override
	public void setHyperparameters(double[] newHyperparams) {
		noiseLevel = newHyperparams[0];
		sigma = newHyperparams[1];
		lengthScale = newHyperparams[2];
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#getHyperparameterObjectiveFunction(de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess)
	 */
	@Override
	protected ObjectiveFunction getHyperparameterObjectiveFunction(
			AbstractGaussianProcess gp) {

		return null;
	}

}
