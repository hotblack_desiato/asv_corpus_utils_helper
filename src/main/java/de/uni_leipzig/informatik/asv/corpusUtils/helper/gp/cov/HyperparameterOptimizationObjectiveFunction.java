/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public abstract class HyperparameterOptimizationObjectiveFunction extends
		ObjectiveFunction {

	/**
	 * @param dimension
	 * @param parameters
	 */
	protected HyperparameterOptimizationObjectiveFunction(int dimension,
			AbstractGaussianProcess parameters) {
		super(dimension, parameters);
	}


	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#f(double[])
	 */
	@Override
	public double f(double[] point) {
		trainGPWithHyperparams(point);
		return -this.getGP().computeLikelihood();
	}


	@Override
	public void df(double[] point, double[] df) {
		trainGPWithHyperparams(point);
		computeGradient(df);
	}


	@Override
	public double fdf(double[] point, double[] df) {
		trainGPWithHyperparams(point);
		computeGradient(df);
		return -getGP().computeLikelihood();
	}
	
	protected abstract void computeGradient(double[] df);

	protected AbstractGaussianProcess trainGPWithHyperparams(double[] point) {
		AbstractGaussianProcess gp = getGP();
		CovarianceFunction k = getKernel();
		k.setHyperparameters(point);
		gp.train();
		return gp;
	}

	protected AbstractGaussianProcess getGP() {
		AbstractGaussianProcess gp = this.getParameters();
		return gp;
	}

	protected CovarianceFunction getKernel() {
		CovarianceFunction k = this.<AbstractGaussianProcess>getParameters().getCovFunction();
		return k;
	}
}
