/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

/**
 * Basic I/O utility class.
 * @author Patrick Jaehnichen <jaehnichen@informatik.uni-leipzig.de>
 *
 */
public class FileUtils {

	/**
	 * Tries to find the correct charset for a file with unknown charset.
	 * Uses the icu4j library for this purpose and returns a {@link CharsetMatch}
	 * @param filename - the file to be checked
	 * @return the charset match representing the most probable charset of the file
	 * @throws IOException
	 */
	public static CharsetMatch getCharsetMatch(String filename) throws IOException {
		CharsetDetector cd = new CharsetDetector();
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(new File(filename)));
		cd.setText(is);
		CharsetMatch result = cd.detect();
		is.close();
		return result;
	}
	
	/**
	 * Reads a text file and returns a string array
	 * @param filename - the file to be read
	 * @return a string array containing the lines in the file as elements
	 * @throws IOException
	 */
	public static String[] readLinesFromFile(String filename) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line = "";
		ArrayList<String> lines = new ArrayList<>();
		while((line = br.readLine()) != null) {
			lines.add(line.trim());
		}
		br.close();
		return lines.toArray(new String[]{});
	}
	
	/**
	 * Reads a text file and returns a string. Works a {@link readLinesFromFile} but concatenates
	 * all lines by a line break and returns this as a string.
	 * @param filename - the file to be read
	 * @return a string containg the whole content of the file
	 * @throws IOException
	 */
	public static String readLineFromFile(String filename) throws IOException {
		String[] lines = readLinesFromFile(filename);
		StringBuilder sb = new StringBuilder();
		for(String line : lines)
			sb.append(line).append("\n");
		return sb.toString().trim();
	}

}
