/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.sde;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class OrnsteinUhlenbeckSDE extends StochasticDifferentialEquation {

	/**
	 * @param processNoise
	 * @param hyperparameters
	 */
	public OrnsteinUhlenbeckSDE(double[][] processNoise, double theta) {
		super(processNoise, new double[]{theta});
	}
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunction(org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunction(RealMatrix X, int k) {
		double theta = getHyperparameters()[0]; 
		return X.scalarMultiply(-theta);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionExpectedDerivativeX(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunctionExpectedDerivativeX(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		double[] ret = new double[expectationX.getRowDimension()]; 
		RealMatrix id = MatrixUtils.createColumnRealMatrix(ret).scalarAdd(-theta);
		return id;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionExpectation(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunctionExpectation(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		return expectationX.scalarMultiply(-theta);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionTimesXExpectation(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunctionTimesXExpectation(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		return normalSecondMoment(expectationX, varianceX).scalarMultiply(-theta);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#squaredDriftFunctionExpectation(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix squaredDriftFunctionExpectation(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		return normalSecondMoment(expectationX, varianceX).scalarMultiply(theta*theta);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionDerivativeHyperparameters()
	 */
	@Override
	public double[] driftFunctionDerivativeHyperparameters() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#energyDerivativeVariance(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix energyDerivativeVariance(RealMatrix posteriorMean,
			RealMatrix posteriorVariance, RealMatrix variationalFunctionB,
			RealMatrix variationalFunctionA, int k) {
		double theta = getHyperparameters()[0];
		RealMatrix aMinTheta = variationalFunctionA.scalarAdd(-theta);
		RealMatrix term1 = aMinTheta.multiply(aMinTheta.transpose()).scalarMultiply(.5).multiply(normalSecondMomentDerivMean(posteriorMean));
		RealMatrix term2 = aMinTheta.scalarMultiply(-1);
		return this.getNoiseInv().multiply(term1.add(term2));
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#energyDerivativeMean(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix energyDerivativeMean(RealMatrix posteriorMean,
			RealMatrix posteriorVariance, RealMatrix variationalFunctionB,
			RealMatrix variationalFunctionA, int k) {
		double theta = getHyperparameters()[0];
		RealMatrix aMinTheta = variationalFunctionA.scalarAdd(-theta);
		RealMatrix term1 = aMinTheta.multiply(aMinTheta.transpose()).scalarMultiply(.5).multiply(normalSecondMomentDerivVar(posteriorVariance));
		return this.getNoiseInv().multiply(term1);
	}

}
