/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.rprop;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class RPropOptimizer extends Optimizer{
	
	private double[] deltaX;
	private double[] delta;
	private double weightIncreaase = 1.2;
	private double weightDecrease = .5;
	
	private double maxWeightUpdate = 50d;
	private double minWeightUpdate = 1e-6;
	private double tolerance;
	
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer#allocate(int)
	 */
	@Override
	public void allocate(int n) {
		super.allocate(n);
		this.deltaX = ArrayFactory.doubleArray(Type.ZERO, n);
		this.delta = ArrayFactory.doubleArray(Type.ONE, n);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer#set(de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction, double[], double, double)
	 */
	@Override
	public void set(ObjectiveFunction _f, double[] guess, double stepSize,
			double _tolerance) {
		this.func = _f;
		System.arraycopy(guess, 0, this.x, 0, guess.length);
		//init delta to initial step size
		this.delta = VectorUtils.multiplyWithScalar(this.delta, stepSize);
		this.tolerance = _tolerance;
		//initial gradient computation
		this.f = _f.fdf(this.x, this.gradient);
		//set deltaX accordingly and adjust x
		for(int i=0;i<this.gradient.length;i++) {
			this.x[i] -= this.delta[i] * Math.signum(this.gradient[i]);
		}
		//compute new gradient
		_f.df(this.x, this.dx);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer#iterate()
	 */
	@Override
	public Result iterate() {
		for(int i=0;i<this.gradient.length;i++) {
			double prodSign = Math.signum(this.gradient[i]*this.dx[i]);
			double curGradSign = Math.signum(this.dx[i]);
			if(prodSign > 0) {
				this.delta[i] = Math.min(this.delta[i]*this.weightIncreaase, this.maxWeightUpdate);
			} else if(prodSign < 0) {
				this.delta[i] = Math.max(this.delta[i]*this.weightDecrease, this.minWeightUpdate);
//				dx[i] = 0;
//				curGradSign = 0;
			}
			this.x[i] = this.x[i] - curGradSign*this.delta[i];
		}
		System.arraycopy(this.dx, 0, this.gradient, 0, this.dx.length);
		double oldVal = this.f;
		this.f = this.func.fdf(this.x, this.dx);
//		assert(f<=oldVal);
		if(this.f < oldVal)
			return Result.SUCCESS;
		return Result.CONTINUE;
	}

}
