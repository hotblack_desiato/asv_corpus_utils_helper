/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.brownian;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.norm;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.subtract;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.RBFObjectiveFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

public class BrownianKernel extends CovarianceFunction {

	private double sigmaSquare;
	
	public BrownianKernel(double s2, double noise) {
		super(noise);
		this.sigmaSquare = s2;
	}
	
	@Override
	public double computeCovariance(double[] a, double[] b) {
		if(a==b)
			return this.sigmaSquare*Math.min(norm(a), norm(b));// + this.noiseLevel;
		return this.sigmaSquare*Math.min(norm(a), norm(b));
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#getHyperparameters()
	 */
	@Override
	public double[] getHyperparameters() {
		return new double[]{this.noiseLevel, this.sigmaSquare};
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#setHyperparameters(double[])
	 */
	@Override
	public void setHyperparameters(double[] newHyperparams) {
		this.noiseLevel = newHyperparams[0];
		this.sigmaSquare = newHyperparams[1];
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#getHyperparameterObjectiveValueFunction(de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess)
	 */
	@Override
	protected ObjectiveFunction getHyperparameterObjectiveFunction(
			AbstractGaussianProcess gp) {
		if(this.hyperparamUpdateFunction != null)
			this.hyperparamUpdateFunction = new RBFObjectiveFunction(3, gp); 
		return this.hyperparamUpdateFunction;
	}

	//derivatives	
	//dk/dsigma2
	public double computeCovarinceDerivSigma(double[] a, double[] b) {
		return Math.min(norm(a), norm(b));
	}
	
	//dK/dsigma2
	public double[][] computeCovarianceMatrixDerivSigma(double[][] trainingPoints) {
		double[][] tCov = new double[trainingPoints.length][trainingPoints.length];
		for(int i=0;i<trainingPoints.length;++i) {
			for(int j=i;j<trainingPoints.length;++j) {
				if(i==j) {
					tCov[i][j] = 1;
					continue;
				}
				tCov[i][j] = computeCovarinceDerivSigma(trainingPoints[i], trainingPoints[j]);
				tCov[j][i] = tCov[i][j]; 
			}
		}
		return tCov;
	}
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivXPrime(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivXPrime(double[] a, double[] b) {
//		double norm_a = norm(a);
//		double norm_b = norm(b);
//		if()
		throw new UnsupportedOperationException("not yet implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivX(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivX(double[] a, double[] b) {
		throw new UnsupportedOperationException("not yet implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#setHyperparamUpdateFunction(de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction)
	 */
	@Override
	public void setHyperparamUpdateFunction(
			ObjectiveFunction _hyperparamUpdateFunction) {
		this.hyperparamUpdateFunction = _hyperparamUpdateFunction;
	}

}
