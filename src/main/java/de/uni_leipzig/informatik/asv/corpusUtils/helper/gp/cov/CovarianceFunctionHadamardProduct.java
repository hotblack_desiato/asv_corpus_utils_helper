/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov;

import java.util.Vector;

import org.apache.commons.lang3.ArrayUtils;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class CovarianceFunctionHadamardProduct extends CovarianceFunction {

	private CovarianceFunction[] factors;
	private double s2;
	/**
	 * @param noise
	 */
	public CovarianceFunctionHadamardProduct(double noise, double var, CovarianceFunction[] factors) {
		super(noise);
		this.s2 = var;
		this.factors = factors;
	}
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovariance(double[], double[])
	 */
	@Override
	public double[][] computeCovarianceMatrix(double[][] a) {
		// TODO: make sure factor covariance is one
//		for(CovarianceFunction f : factors)
//			f;
		double[][] cov = null;
		for(int i =0;i<this.factors.length;i++) {
			if(i == 0) {
				cov = this.factors[i].computeCovarianceMatrix(a);
//				cov = VectorUtils.divideByScalar(cov, scalar)
			} else {
				double[][] covFactor = this.factors[i].computeCovarianceMatrix(a);
				cov = VectorUtils.multiply(cov, covFactor);
			}
		}
		cov = VectorUtils.multiplyWithScalar(cov, s2);
		cov = VectorUtils.add(VectorUtils.diag(VectorUtils.multiplyWithScalar(ArrayFactory.doubleArray(de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type.ONE, a.length), noiseLevel)), cov);
		this.trainingDataCovariance = cov;
		return cov;
	}

	
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#computeCrossCovarianceMatrix(double[][], double[][])
	 */
	@Override
	public double[][] computeCrossCovarianceMatrix(double[][] unseenPoints,
			double[][] trainingPoints) {
		double[][] cov = null;
		for(int i =0;i<this.factors.length;i++) {
			if(i == 0)
				cov = this.factors[i].computeCrossCovarianceMatrix(unseenPoints, trainingPoints);
			else {
				double[][] covFactor = this.factors[i].computeCrossCovarianceMatrix(unseenPoints, trainingPoints);
				cov = VectorUtils.multiply(cov, covFactor);
			}
		}
		return cov;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivXPrime(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivXPrime(double[] a, double[] b) {
		throw new UnsupportedOperationException("not implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivX(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivX(double[] a, double[] b) {
		throw new UnsupportedOperationException("not implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#getHyperparameters()
	 */
	@Override
	public double[] getHyperparameters() {
		Vector<Double> hyperparams = new Vector<>();
		for(CovarianceFunction f : this.factors) {
			double[] fHypers = f.getHyperparameters();
			for(double fHyper : fHypers)
				hyperparams.add(fHyper);
		}
		return ArrayUtils.toPrimitive(hyperparams.toArray(new Double[]{}));
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#setHyperparameters(double[])
	 */
	@Override
	public void setHyperparameters(double[] newHyperparams) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#getHyperparameterObjectiveFunction(de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess)
	 */
	@Override
	protected ObjectiveFunction getHyperparameterObjectiveFunction(
			AbstractGaussianProcess gp) {
		throw new UnsupportedOperationException("not implemented");
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovariance(double[], double[])
	 */
	@Override
	public double computeCovariance(double[] a, double[] b) {
		// TODO Auto-generated method stub
		return 0;
	}

}
