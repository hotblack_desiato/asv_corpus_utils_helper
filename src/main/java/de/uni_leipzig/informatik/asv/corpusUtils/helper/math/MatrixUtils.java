/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.math;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.jet.math.Functions;

public class MatrixUtils {

	public static DoubleMatrix1D dotProductOverColumns(DoubleMatrix1D a, DoubleMatrix2D b) {
		DoubleMatrix1D ret = new DenseDoubleMatrix1D(b.columns());
		for(int i=0;i<b.columns();i++)
			ret.set(i, a.zDotProduct(b.viewColumn(i)));
		return ret;
	}
	
	public static DoubleMatrix2D dotProduct(DoubleMatrix2D a, DoubleMatrix2D b) {
		assert(a.columns() == b.rows());
		DenseDoubleMatrix2D ret = new DenseDoubleMatrix2D(a.rows(), b.columns());
		for(int row=0;row<a.rows();++row) {
			for(int col=0;col<b.columns();++col) {
				ret.set(row, col, a.viewRow(row).zDotProduct(b.viewColumn(col)));
			}
		}
		return ret;
	}
	
	public static double mean(DoubleMatrix1D mat) {
		return mat.zSum()/(double)mat.size();
	}
	
	public static double mean(DoubleMatrix2D mat) {
		return mat.zSum()/mat.size();
	}

	public static DoubleMatrix2D multRowElements(DoubleMatrix2D mat, DoubleMatrix1D factors) {
		for(int row=0;row<mat.rows();++row) {
			mat.viewRow(row).assign(factors, Functions.mult);
		}
		return mat;
	}
	
	public static DoubleMatrix2D addRowWise(DoubleMatrix2D mat, DoubleMatrix1D summands) {
		for(int row=0;row<mat.rows();++row) {
			mat.viewRow(row).assign(summands, Functions.plus);
		}
		return mat;
	}
	
	public static DoubleMatrix2D addColumnWise(DoubleMatrix2D mat, DoubleMatrix1D summands) {
		for(int col=0;col<mat.columns();++col) {
			mat.viewColumn(col).assign(summands, Functions.plus);
		}
		return mat;
	}
	
	public static double logNormalize(DoubleMatrix1D mat) {
//		double logSum = Math.log(mat.copy().assign(Functions.exp).zSum());
//		mat.assign(Functions.minus(logSum));
//		return logSum;
		double logMax = 100.;
		double maxVal = VectorUtils.max(mat.toArray());
		double logShift = logMax - Math.log(mat.size()+1) - maxVal;
		double total = mat.copy().assign(Functions.plus(logShift)).assign(Functions.exp).zSum();
		double logNorm = Math.log(total) - logShift;
		mat.assign(Functions.minus(logNorm));
		return logNorm;
	}
	
	public static DoubleMatrix1D logNormalizeRows(DoubleMatrix2D mat) {
		DoubleMatrix1D ret = new DenseDoubleMatrix1D(mat.rows());
		for(int row=0;row<mat.rows();++row)
			ret.set(row, logNormalize(mat.viewRow(row)));
		return ret;
	}
	
	
	public static DoubleMatrix1D cumulativeSum(DoubleMatrix1D in) {
		DoubleMatrix1D out = new DenseDoubleMatrix1D(in.size());
		out.set(0, in.get(0));
		for(int i=2;i<=in.size();++i) {
			out.set(i-1, in.viewPart(0, i).zSum());
		}
		return out;
	}
	
	public static DoubleMatrix1D specialReverseCumulativeSum(DoubleMatrix1D in) {
		DoubleMatrix1D out = new DenseDoubleMatrix1D(in.size());
		out.set(in.size()-1, 0);
		for(int i=0;i<in.size()-1;i++)
			out.set(i, in.viewPart(i+1, in.size()-(i+1)).zSum());
		return out;
	}
	
	public static DoubleMatrix2D outerProduct(DoubleMatrix1D a, DoubleMatrix1D b) {
		DoubleMatrix2D ret = new DenseDoubleMatrix2D(a.size(), b.size());
		for(int i=0;i<a.size();++i)
			ret.viewRow(i).assign(b.copy().assign(Functions.mult(a.get(i))));
		return ret;
	}
	
	public static DoubleMatrix1D sumRows(DoubleMatrix2D mat) {
		DoubleMatrix1D ret = new DenseDoubleMatrix1D(mat.rows());
		for(int row=0;row<mat.rows();++row)
			ret.set(row, mat.viewRow(row).zSum());
		return ret;
	}
	
	public static DoubleMatrix1D sumColumns(DoubleMatrix2D mat) {
		DoubleMatrix1D ret = new DenseDoubleMatrix1D(mat.columns());
		for(int col=0;col<mat.columns();++col) {
			ret.set(col, mat.viewColumn(col).zSum());
		}
		return ret;
	}
	
	public static DoubleMatrix2D viewColumnSelection(DoubleMatrix2D mat, int[] columns) {
		int[] rowSelector = new int[mat.rows()];
		for(int i=0;i<rowSelector.length;++i)
			rowSelector[i] = i;
		return mat.viewSelection(rowSelector, columns).copy();
	}
	
	public static DoubleMatrix2D viewRowSelection(DoubleMatrix2D mat, int[] rows) {
		int[] colSelector = new int[mat.columns()];
		for(int i=0;i<colSelector.length;++i)
			colSelector[i] = i;
		return mat.viewSelection(rows, colSelector).copy();
	}

	public static DoubleMatrix1D logDirichletExpectation(DoubleMatrix1D params) {
		DoubleMatrix1D ret = params.copy().assign(DoubleFunctionFactory.digamma);
		return ret.assign(Functions.minus(MathUtils.digamma(params.zSum())));
	}
	
	public static DoubleMatrix2D logDirichletExpectation(DoubleMatrix2D params) {
		DoubleMatrix2D ret = params.copy().assign(DoubleFunctionFactory.digamma);
		for(int row=0;row<params.rows();row++) {
			ret.viewRow(row).assign(Functions.minus(MathUtils.digamma(params.viewRow(row).zSum())));
		}
		return ret;
	}
	
	public static DoubleMatrix1D logStickExpectation(DoubleMatrix1D _a, DoubleMatrix1D _b) {
		DoubleMatrix1D digSum = _a.copy().assign(_b, Functions.plus).assign(DoubleFunctionFactory.digamma);
		DoubleMatrix1D ElogW = _a.copy().assign(DoubleFunctionFactory.digamma).assign(digSum, Functions.minus);
		DoubleMatrix1D Elog1_W = _b.copy().assign(DoubleFunctionFactory.digamma).assign(digSum, Functions.minus);
		
		int n = _a.size() + 1;
		DoubleMatrix1D ElogStick = new DenseDoubleMatrix1D(n);
		ElogStick.viewPart(0, _a.size()).assign(ElogW);
		ElogStick.viewPart(1, _b.size()).assign(cumulativeSum(Elog1_W), Functions.plus);
		
		return ElogStick;
	}
	
	public static DoubleMatrix1D ensureCapacity(DoubleMatrix1D in, int minCapacity) {
		if(in.size() < minCapacity) {
			DoubleMatrix1D newMat = new DenseDoubleMatrix1D(minCapacity);
			newMat.viewPart(0, in.size()).assign(in.toArray());
			return newMat;
		}
		return in;			
	}

	public static DoubleMatrix2D ensureCapacity(DoubleMatrix2D in, int minRowCapacity, int minColCapacity) {
		if(in.rows() < minRowCapacity || in.columns() < minColCapacity) {
			DoubleMatrix2D newMat = new DenseDoubleMatrix2D(minRowCapacity, minColCapacity);
			newMat.viewPart(0, in.rows(), 0, in.columns()).assign(in.toArray());
			return newMat;
		}
		return in;			
	}
	
	public static int sampleFromCumulativeDistribution(DoubleMatrix1D dist, double rand) {
		int k=0;
		for (k = 0; k <= dist.size(); k++)
			if (rand < dist.get(k))
				break;
		return k;
	
	}

	public static void saveMatrixToFile(String filename, DoubleMatrix2D mat) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
		bw.write(mat.toString());
		bw.close();
	}

	public static void main(String[] args) throws Exception {
		DoubleMatrix2D test = new DenseDoubleMatrix2D(10, 10);
		DoubleMatrix2D test2 = new DenseDoubleMatrix2D(10, 10);
		test2.set(4, 4, 20);
		test.assign(test2, Functions.plus);
		System.out.println(test.toString());
	}
	
}
