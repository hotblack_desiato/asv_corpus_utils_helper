/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.sparsevb;

import java.util.Arrays;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

public class HyperparamLLFunction extends ObjectiveFunction {

	protected HyperparamLLFunction(int dim, LogLikelihoodOptimizeParameters p) {
		super(dim, p);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#f(double[])
	 */
	@Override
	public double f(double[] point) {
		LogLikelihoodOptimizeParameters p = getParameters();
		double noiseS2 = point[0];
		double s2 = point[1];
		double l = point[2];
		int n = p.getX().getRowDimension();
		int m = p.getXm().getRowDimension();
		
		CovarianceFunction cov = p.getModel().getCovFunction();
		cov.setHyperparameters(point);
		
		RealMatrix Knn = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrix(p.getX().getData()));
		RealMatrix Kmn = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrix(p.getXm().getData(), p.getX().getData()));
		RealMatrix Knm = Kmn.copy().transpose();
		RealMatrix Kmm = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrix(p.getXm().getData()));
		
		//add jitter
		RealMatrix jitter = MatrixUtils.createRealIdentityMatrix(m).scalarMultiply(p.getJitter());
		RealMatrix KmmInv = new CholeskyDecomposition(Kmm.add(jitter)).getSolver().getInverse();
		
		RealMatrix Qnn = Knm.multiply(KmmInv).multiply(Kmn);
		
		RealMatrix noise = MatrixUtils.createRealIdentityMatrix(n).scalarMultiply(noiseS2);
		RealMatrix noiseInv = MatrixUtils.createRealIdentityMatrix(n).scalarMultiply(1./noiseS2);
		
		RealMatrix tmpInv = new CholeskyDecomposition(Kmm.add(Kmn.multiply(noiseInv).multiply(Knm))).getSolver().getInverse();
		
		RealMatrix noiseQnnInv = noiseInv.subtract(noiseInv.multiply(Knm).multiply(tmpInv).multiply(Kmn).multiply(noiseInv));
		
		double det = new LUDecomposition(noise.add(Qnn)).getDeterminant();
		
		double f = -.5*n*Math.log(2*Math.PI) - .5*Math.log(det) - .5*p.getY().transpose().multiply(noiseQnnInv).multiply(p.getY()).getTrace()
				- .5* Knn.subtract(Qnn).getTrace()/noiseS2;
		
		return -f;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#df(double[], double[])
	 */
	@Override
	public void df(double[] point, double[] df) {
		LogLikelihoodOptimizeParameters p = getParameters();
		double noiseS2 = point[0];
		double s2 = point[1];
		double l = point[2];
		Arrays.fill(df, 0);
		
		int n = p.getX().getRowDimension();
		int m = p.getXm().getRowDimension();
		
		GaussianKernel cov = p.getModel().getCovFunction();
		cov.setHyperparameters(point);
		
		RealMatrix Knn = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrix(p.getX().getData()));
		RealMatrix Kmn = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrix(p.getXm().getData(), p.getX().getData()));
		RealMatrix Knm = Kmn.copy().transpose();
		RealMatrix Kmm = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrix(p.getXm().getData()));
		
		//add jitter
		RealMatrix jitter = MatrixUtils.createRealIdentityMatrix(m).scalarMultiply(p.getJitter());
		RealMatrix KmmInv = new CholeskyDecomposition(Kmm.add(jitter)).getSolver().getInverse();
		
		RealMatrix Qnn = Knm.multiply(KmmInv).multiply(Kmn);
		
		RealMatrix noiseInv = MatrixUtils.createRealIdentityMatrix(n).scalarMultiply(1./noiseS2);
		
		RealMatrix tmpInv = new CholeskyDecomposition(Kmm.add(Kmn.multiply(noiseInv).multiply(Knm))).getSolver().getInverse();
		
		RealMatrix noiseQnnInv = noiseInv.subtract(noiseInv.multiply(Knm).multiply(tmpInv).multiply(Kmn).multiply(noiseInv));
		
		RealMatrix v = noiseQnnInv.multiply(p.getY());
		df[0] = -.5*noiseQnnInv.getTrace() + .5*v.transpose().multiply(v).getTrace() + .5 * Knn.subtract(Qnn).getTrace()/(noiseS2*noiseS2);
		
		RealMatrix dKmn = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivSigma(p.getXm().getData(), p.getX().getData()));
		RealMatrix dKnm = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivSigma(p.getX().getData(), p.getXm().getData()));
		RealMatrix dKmm = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivSigma(p.getXm().getData()));
		RealMatrix dKnn = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivSigma(p.getX().getData()));
		RealMatrix dQnn = Knm.multiply(KmmInv.multiply(dKmn).subtract(KmmInv.multiply(dKmm).multiply(KmmInv).multiply(Kmn))).add(dKnm.multiply(KmmInv).multiply(Kmn));
		df[1] = -.5*noiseQnnInv.multiply(dQnn).getTrace() + .5*v.transpose().multiply(noiseQnnInv).multiply(v).getTrace() - .5*dKnn.add(dQnn).getTrace();
		
		dKmn = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivL(p.getXm().getData(), p.getX().getData()));
		dKnm = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivL(p.getX().getData(), p.getXm().getData()));
		dKmm = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivLengthScale(p.getXm().getData()));
		dKnn = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivLengthScale(p.getX().getData()));
		dQnn = Knm.multiply(KmmInv.multiply(dKmn).subtract(KmmInv.multiply(dKmm).multiply(KmmInv).multiply(Kmn))).add(dKnm.multiply(KmmInv).multiply(Kmn));
		df[2] = -.5*noiseQnnInv.multiply(dQnn).getTrace() + .5*v.transpose().multiply(noiseQnnInv).multiply(v).getTrace() - .5*dKnn.add(dQnn).getTrace()/noiseS2;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#fdf(double[], double[])
	 */
	@Override
	public double fdf(double[] point, double[] df) {
		LogLikelihoodOptimizeParameters p = getParameters();
		double noiseS2 = point[0];
		double s2 = point[1];
		double l = point[2];
		Arrays.fill(df, 0);
		
		int n = p.getX().getRowDimension();
		int m = p.getXm().getRowDimension();
		
		GaussianKernel cov = p.getModel().getCovFunction();
		cov.setHyperparameters(point);
		
		RealMatrix Knn = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrix(p.getX().getData()));
		RealMatrix Kmn = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrix(p.getXm().getData(), p.getX().getData()));
		RealMatrix Knm = Kmn.copy().transpose();
		RealMatrix Kmm = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrix(p.getXm().getData()));
		
		//add jitter
		RealMatrix jitter = MatrixUtils.createRealIdentityMatrix(m).scalarMultiply(p.getJitter());
		RealMatrix KmmInv = new CholeskyDecomposition(Kmm.add(jitter)).getSolver().getInverse();
		
		RealMatrix Qnn = Knm.multiply(KmmInv).multiply(Kmn);
		
		RealMatrix noise = MatrixUtils.createRealIdentityMatrix(n).scalarMultiply(noiseS2);
		RealMatrix noiseInv = MatrixUtils.createRealIdentityMatrix(n).scalarMultiply(1./noiseS2);
		
		RealMatrix tmpInv = new CholeskyDecomposition(Kmm.add(Kmn.multiply(noiseInv).multiply(Knm))).getSolver().getInverse();
		
		RealMatrix noiseQnnInv = noiseInv.subtract(noiseInv.multiply(Knm).multiply(tmpInv).multiply(Kmn).multiply(noiseInv));
		
		RealMatrix v = noiseQnnInv.multiply(p.getY());
		df[0] = -.5*noiseQnnInv.getTrace() + .5*v.transpose().multiply(v).getTrace() + .5 * Knn.subtract(Qnn).getTrace()/(noiseS2*noiseS2);
		
		RealMatrix dKmn = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivSigma(p.getXm().getData(), p.getX().getData()));
		RealMatrix dKnm = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivSigma(p.getX().getData(), p.getXm().getData()));
		RealMatrix dKmm = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivSigma(p.getXm().getData()));
		RealMatrix dKnn = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivSigma(p.getX().getData()));
		RealMatrix dQnn = Knm.multiply(KmmInv.multiply(dKmn).subtract(KmmInv.multiply(dKmm).multiply(KmmInv).multiply(Kmn))).add(dKnm.multiply(KmmInv).multiply(Kmn));
		df[1] = -.5*noiseQnnInv.multiply(dQnn).getTrace() + .5*v.transpose().multiply(noiseQnnInv).multiply(v).getTrace() - .5*dKnn.add(dQnn).getTrace();
		
		dKmn = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivL(p.getXm().getData(), p.getX().getData()));
		dKnm = MatrixUtils.createRealMatrix(cov.computeCrossCovarianceMatrixDerivL(p.getX().getData(), p.getXm().getData()));
		dKmm = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivLengthScale(p.getXm().getData()));
		dKnn = MatrixUtils.createRealMatrix(cov.computeCovarianceMatrixDerivLengthScale(p.getX().getData()));
		dQnn = Knm.multiply(KmmInv.multiply(dKmn).subtract(KmmInv.multiply(dKmm).multiply(KmmInv).multiply(Kmn))).add(dKnm.multiply(KmmInv).multiply(Kmn));
		df[2] = -.5*noiseQnnInv.multiply(dQnn).getTrace() + .5*v.transpose().multiply(noiseQnnInv).multiply(v).getTrace() - .5*dKnn.add(dQnn).getTrace()/noiseS2;
		
		
		
		double det = new LUDecomposition(noise.add(Qnn)).getDeterminant();
		double f = -.5*n*Math.log(2*Math.PI) - .5*Math.log(det) - .5*p.getY().transpose().multiply(noiseQnnInv).multiply(p.getY()).getTrace()
				- .5* Knn.subtract(Qnn).getTrace()/noiseS2;
		
		return -f;
	}

}
