/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.math;

import java.util.ArrayList;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class VectorUtils {
	
	//add
	
	public static double[] add(double[] a, double[] b) {
		double[] r = new double[a.length];
		for(int i=0;i<a.length;i++) r[i] = a[i] + b[i];
		return r;		                                      
	}
	
	public static void add_(double[] addThis, double[] toThis) {
		for(int i=0;i<addThis.length;++i)
			toThis[i] += addThis[i];
	}
	
	public static double[][] add(double[][] a, double[][] b) {
		double[][] r = new double[a.length][];
		for(int i=0;i<a.length;i++) {
			r[i] = new double[a[i].length];
			for(int j=0;j<a[0].length;j++)
				r[i][j] = a[i][j] + b[i][j];
		}
		return r;		                                      
	}

	public static double[] logAdd(double[] a, double[] b) {
		double[] r = new double[a.length];
		for(int i=0;i<a.length;i++) r[i] = MathUtils.logAdd(a[i], b[i]);
		return r;
	}
	
	public static double[][] logAdd(double[][] a, double[][] b) {
		double[][] r = new double[a.length][a[0].length];
		for(int i=0;i<a.length;i++)
			for(int j=0;j<a[0].length;j++)
				r[i][j] = MathUtils.logAdd(a[i][j], b[i][j]);
		return r;
	}
	
	public static double[] addScalar(double[] a, double s) {
		double[] result = new double[a.length];
		for(int i=0;i<result.length;i++) {
			result[i] = a[i]+s;
		}
		return result;
	}

	public static double[] addScalarLogSpace(double[] a, double s) {
		double[] result = new double[a.length];
		double logS = Math.log(s);
		for(int i=0;i<result.length;i++) {
			result[i] = MathUtils.logAdd(a[i],logS);
		}
		return result;
	}

	public static double[][] addScalar(double[][] a, double s) {
		double[][] result = new double[a.length][a[0].length];
		for(int i=0;i<result.length;i++) {
			for(int j=0;j<a[0].length;j++)
				result[i][j] = a[i][j]+s;
		}
		return result;
	}
	
	public static double[][] addScalarLogSpace(double[][] a, double s) {
		double[][] result = new double[a.length][a[0].length];
		double logS = Math.log(s);
		for(int i=0;i<result.length;i++) {
			for(int j=0;j<a[0].length;j++)
				result[i][j] = MathUtils.logAdd(a[i][j], logS);
		}
		return result;
	}
	
	public static int[] addVector(int[] a, int[] b) {
		int[] r = new int[a.length];
		for(int i=0;i<a.length;i++) r[i] = a[i] + b[i];
		return r;
	}

	//subtract
	
	public static double[] subtractScalar(double[] m, double s) {
		double[] r = new double[m.length];
		for(int i=0;i<m.length;i++) r[i] = m[i] - s;
		return r;
	}

	public static double[][] subtractScalar(double[][] m, double s) {
		double[][] r = new double[m.length][m[0].length];
		for(int i=0;i<m.length;i++) 
			for(int j=0;j<m[0].length;j++)
				r[i][j] = m[i][j] - s;
		return r;
	}

	public static double[] subtractFromScalar(double s, double[] a) {
		double[] r = new double[a.length];
		for(int i=0;i<a.length;i++)
			r[i] = s - a[i];			             
		return r;
	}
	
	public static double[][] subtractFromScalar(double s, double[][] a) {
		double[][] r = new double[a.length][a[0].length];
		for(int i=0;i<a.length;i++)
			for(int j=0;j<a[0].length;j++)
				r[i][j] = s - a[i][j];			             
		return r;
	}
	
	public static double[] subtract(double[] a, double[] b) {
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;i++)
			ret[i] = a[i] - b[i];
		return ret;
	}
	
	public static double[] logSubtract(double[] a, double[] b) {
		if(a.length != b.length)
			throw new IllegalArgumentException("vector lengths must match");
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;i++) ret[i] = MathUtils.logSubtract(a[i], b[i]);
		return ret;
	}
	
	public static void subtract_(double[] a, double[] b) {
		for(int i=0;i<a.length;i++)
			a[i] -= b[i];
	}

	
	public static double[][] subtract(double[][] a, double[][] b) {
		double ret[][] = new double[a.length][];
		for(int i=0;i<a.length;++i) {
			ret[i] = new double[a[i].length];
			for(int j=0;j<a[i].length;++j)
				ret[i][j] = a[i][j] - b[i][j];
		}
		return ret;
	}

	//multiply
	
	public static double[] matrixMultiply(double[] a, double[][] b) {
		RealMatrix bMat = MatrixUtils.createRealMatrix(b);
		return bMat.preMultiply(a);
	}
	
	public static double[][] matrixMultiply(double[][] a, double[][] b) {
		RealMatrix aMat = MatrixUtils.createRealMatrix(a);
		RealMatrix bMat = MatrixUtils.createRealMatrix(b);
		return aMat.multiply(bMat).getData();
	}
	
	public static double[] multiply(double[] vec, double[] beta) {
		double[] r = new double[vec.length];
		for(int i=0;i<vec.length;i++)
			r[i] = vec[i]*beta[i];
		return r;
	}
	
	public static double[] multiply(int[] a, double[] b) {
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;i++)
			ret[i] = a[i] * b[i];
		return ret;
	}
	
	public static double[][] multiply(double[][] a, double[][] b) {
		double[][] ret = new double[a.length][a[0].length];
		for(int i=0;i<a.length;i++)
			for(int j=0;j<a[0].length;j++)
				ret[i][j] = a[i][j] * b[i][j];
		return ret;
	}
	
	public static double[] multiplyWithScalar(double[] a, double s) {
		double[] r = new double[a.length];
		for(int i=0;i<a.length;i++)
			r[i] = s*a[i];
		return r;
	}


	public static void multiplyWithScalar_(double[] a, double s) {
		for(int i=0;i<a.length;i++)
			a[i] *= s;
	}

	public static double[][] multiplyWithScalar(double[][] a, double s) {
		double[][] r = new double[a.length][a[0].length];
		for(int i=0;i<a.length;i++)
			for(int j=0;j<a[0].length;j++)
				r[i][j] = s*a[i][j];
		return r;
	}
	
	//divide
	
	public static double[] divideByScalar(double[] dist, double scalar) {
		double[] r = new double[dist.length];
		for(int i=0;i<dist.length;i++)
			r[i] = dist[i] / scalar;
		return r;
	}
	
	public static double[] divideByScalar(int[] counts, double scalar) {
		double[] r = new double[counts.length];
		for(int i=0;i<counts.length;i++)
			r[i] = counts[i]/scalar;
		return r;
	}
	
	public static double[][] divideByScalar(double[][] dist, double scalar) {
		double[][] r = new double[dist.length][dist[0].length];
		for(int i=0;i<dist.length;i++)
			for(int j=0;j<dist[0].length;j++)
				r[i][j] = dist[i][j] / scalar;
		return r;
	}

	public static double[] divideScalar(double s, double[] d) {
		double[] r = new double[d.length];
		for(int i=0;i<d.length;i++)
			r[i] = s/d[i];
		return r;
	}
	
	public static double[][] divide(double[][] a, double[][] b) {
		double[][] result = new double[a.length][];
		for(int i=0;i<a.length;++i)
			result[i] = divideByVector(a[i], b[i]);
		return result;
	}
	
	
	public static double[] divideByVector(double[] a, double[] b) {
		double[] result = new double[a.length];
		for(int i=0;i<a.length;i++) {
			result[i] = a[i]/b[i];
		}
		return result;
	}
	
	public static double[] divideByVector(int[] a, double[] b) {
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;i++)
			ret[i] = a[i] / b[i];
		return ret;
	}
	
	//others
	
	public static double[][] diag(double[] d) {
		double[][] ret = new double[d.length][d.length];
		for(int i=0;i<d.length;++i)
			ret[i][i] = d[i];
		return ret;
	}
	
	public static double[] diag(double[][] d) {
		if(d.length != d[0].length)
			throw new IllegalArgumentException("parameter must be a square matrix");
		double[] ret = new double[d.length];
		for(int i=0;i<d.length;i++)
			ret[i] = d[i][i];
		return ret;
	}
	
	public static Integer[] findin(double[] a, double b) {
		ArrayList<Integer> idxList = new ArrayList<>();
		for(int i=0;i<a.length;++i) {
			if(a[i] == b)
				idxList.add(i);
		}
		return idxList.toArray(new Integer[]{});
	}
	
	public static Integer[] findin(int[] a, int b) {
		ArrayList<Integer> idxList = new ArrayList<>();
		for(int i=0;i<a.length;++i) {
			if(a[i] == b)
				idxList.add(i);
		}
		return idxList.toArray(new Integer[]{});
	}

	public static double[] neg(double[] a) {
		double[] ret = new double[a.length];
		for(int i=0;i<ret.length;++i)
			ret[i] = -a[i];
		return ret;
	}
	
	public static void neg_(double a[]) {
		for(int i=0;i<a.length;++i)
			a[i] = -a[i];
	}
	
	public static double max(double[] a) {
		double max = Double.NEGATIVE_INFINITY;
		for(int i=0;i<a.length;i++)
			max = max < a[i]?a[i]:max;
		return max;
	}
	public static int max(int[] a) {
		int max = Integer.MIN_VALUE;
		for(int i=0;i<a.length;i++)
			max = max < a[i]?a[i]:max;
		return max;
	}
	
	public static double max(double[][] a) {
		double max = Double.NEGATIVE_INFINITY;
		for(int i=0;i<a.length;i++) {
			double rowmax = max(a[i]);
			max = max < rowmax ? rowmax : max;
		}
		return max;
	}

	public static double min(double[] a) {
		double min = Double.POSITIVE_INFINITY;
		for(int i=0;i<a.length;i++)
			min = min > a[i] ? a[i] : min;
		return min;
	}

	public static double min(double[][] a) {
		double min = Double.POSITIVE_INFINITY;
		for(int i=0;i<a.length;i++) {
			double rowmin = min(a[i]);
			min = min > rowmin ? rowmin : min;
		}
		return min;
	}

	public static double[] abs(double[] a) {
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;i++)
			ret[i] = Math.abs(a[i]);
		return ret;
	}
	
	public static double mean(double[] a) {
		double ret = 0;
		for(int i=0;i<a.length;i++) 
			ret += a[i]; 
		return ret / a.length;
	}
	
	/**
	 * Computes the mean across rows. Given a n x m matrix,
	 * produces an m-vector containing each columns mean across rows.
	 * I.e. the data is assumed to be n samples in m dimensions.
	 * @param a - the matrix
	 * @return the mean across rows
	 */
	public static double[] mean(double[][] a) {
		int numSamples = a.length;
		double[] ret = sumRowAxis(a);
		divideByScalar(ret, numSamples);
		return ret;
	}
	
	public static double[][] covariance(double[][] data, double[] mean) {
		double[][] ret = new double[data.length][];
		for(int r=0;r<data.length;r++) {
			ret[r] = new double[data[r].length];
			for(int c=0;c<data[r].length;c++) {
				ret[r][c] = Math.pow(data[r][c]-mean[c],2);
			}
		}
		return ret;
	}
	
	public static double mean(int[] a) {
		double ret = 0;
		for(int i=0;i<a.length;i++) 
			ret += a[i]; 
		return ret / a.length;
		
	}
	
	public static double variance(double[] a) {
		double mean = mean(a);
		return sum(square(subtractScalar(a, mean)))/a.length;
	}
	
	public static double[][] transpose(double[][] a) {
		double[][] ret = new double[a[0].length][a.length];
		for(int i=0;i<a.length;i++)
			for(int j=0;j<a[0].length;j++)
				ret[j][i] = a[i][j];
		return ret;		
	}
	
	public static double[] invert(double[] a) {
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;++i)
			ret[i] = 1./a[i];
		return ret;
	}
	
	public static void invert_(double a[]) {
		for(int i=0;i<a.length;++i)
			a[i] = 1./a[i];
	}
	
	public static double sum(double[] a) {
		double s = 0;
		for(double d : a)
			s +=d;
		return s;
	}

	public static int sum(int[] a) {
		int s = 0;
		for(int d : a)
			s +=d;
		return s;
	}
	
	/**
	 * Sums across the first (row) axis, i.e. given a matrix we produce a row vector.
	 * Each row component is the sum over the column of the corresponding index.
	 * @param a - the original matrix K x V
	 * @return a row vector with summed columns as components 1 x V
	 */
	public static double[] sumRowAxis(double[][] a) {
		double[] ret = new double[a[0].length];
		for(int i=0;i<a.length;i++) {
			for(int j=0;j<a[i].length;++j)
				ret[j] += a[i][j];
		}
		return ret;
	}
	
	/**
	 * Sums across the second (column) axis, i.e. given a matrix we produce a column vector .
	 * Each column component is the sum over the rows at the corresponding index.
	 * @param a - the original matrix K x V
	 * @return a column vector with summed rows as components K x 1
	 */
	public static double[] sumColumnAxis(double[][] a) {
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;i++) {
			for(double d : a[i])
				ret[i] += d;
		}
		return ret;
	}

	public static double sum(double[][] a) {
		double s = 0;
		for(double[] b : a)
			for(double d : b)
				s +=d;
		return s;
	}
	
	public static double[] normalize(double[] _in) {
		double sum = sum(_in);
		return divideByScalar(_in, sum);
	}
	
	public static double[] normalize(int[] counts) {
		double sum = sum(counts);
		return divideByScalar(counts, sum);
	}
	
	public static double norm(double[] _in) {
		double result = 0;
		for(double i : _in)
			result += i*i;
		return Math.sqrt(result);
	}
	
	public static double cosineDistance(double[] a, double[] b) {
		return dotProduct(a, b)/(norm(a)*norm(b));
	}
	
	
	public static double logSum(double[] a) {
		double sum = a[0];
		for(int i=1;i<a.length;i++)
			sum = MathUtils.logAdd(sum, a[i]);
		return sum;
	}

	public static double logSum(double[][] a) {
		double sum = a[0][0];
		for(int i=0;i<a.length;i++)
			for(int j=0;j<a[0].length;j++) {
				if(i==0&&j==0) continue;
				sum = MathUtils.logAdd(sum, a[i][j]);
			}
		return sum;
	}
	
	public static double[] mask(double[] a, double[] b) {
		double[] r = new double[a.length];
		for(int i=0;i<a.length;i++) {
			if(b[i] == 0)
				a[i] = 0;
			
		}
		return r;
	}
	
	public static void setSelection(double[] target, double[] values, int[] idx) {
		for(int i=0;i<idx.length;++i)
			target[idx[i]] = values[i];
	}

	public static void addSelection(double[] target, double[] values, int[] idx) {
		for(int i=0;i<idx.length;++i)
			target[idx[i]] += values[i];
	}
	
	public static void subtractSelection(double[] target, double[] values, int[] idx) {
		for(int i=0;i<idx.length;++i)
			target[idx[i]] -= values[i];
	}

	public static double[] viewSelection(double[] source, int[] idx) {
		double[] ret = new double[idx.length];
		for(int i=0;i<idx.length;++i)
			ret[i] = source[idx[i]];
		return ret;
	}
	
	public static int[] viewSelection(int[] source, int[] idx) {
		int[] ret = new int[idx.length];
		for(int i=0;i<idx.length;++i)
			ret[i] = source[idx[i]];
		return ret;
	}
	
	public static double[] viewColumn(double[][] source, int col) {
		double[] ret = new double[source.length];
		for(int i=0;i<source.length;++i)
			ret[i] = source[i][col];
		return ret;
	}
	
	public static double[][] viewColumnSelection(double[][] src, int[] cols) {
		double[][] ret = new double[src.length][cols.length];
		for(int i=0;i<src.length;++i) {
			for(int j=0;j<cols.length;++j)
				ret[i][j] = src[i][cols[j]];
		}
		return ret;
	}
	
	public static double[] addElement(double[] src) {
		double[] newArray = new double[src.length+1];
		System.arraycopy(src, 0, newArray, 0, src.length);
		return newArray;
	}
	
	public static int[] addElement(int[] src) {
		int[] newArray = new int[src.length+1];
		System.arraycopy(src, 0, newArray, 0, src.length);
		return newArray;
	}

	public static int[][] addRow(int[][] src) {
		int[][] newArray = new int[src.length+1][];
		for(int i=0;i<src.length;i++) newArray[i] = src[i];
		newArray[newArray.length-1] = new int[src[0].length];
		return newArray;
	}
	
	public static double[] removeElement(double[] src, int elementId) {
		double[] newArray = new double[src.length - 1];
		int j = 0;
		for(int i = 0;i<src.length;i++) {
			if(i==elementId)
				continue;
			newArray[j++] = src[i];
		}
		return newArray;
	}

	public static int[] removeElement(int[] src, int elementId) {
		int[] newArray = new int[src.length - 1];
		int j = 0;
		for(int i = 0;i<src.length;i++) {
			if(i==elementId)
				continue;
			newArray[j++] = src[i];
		}
		return newArray;
	}
	
	public static int[][] removeRow(int[][] a, int row) {
		int[][] r = new int[a.length-1][];
		int count = 0;
		for(int i=0;i<a.length;i++) {
			if(i==row) continue;
			r[count++] = a[i];
		}
		return r;
	}

	
	public static double[][] deleteRow(double[][] a, int row) {
		double[][] r = new double[a.length-1][];
		int count = 0;
		for(int i=0;i<a.length;i++) {
			if(i==row) continue;
			r[count++] = a[i];
		}
		return r;
	}

	public static double[] square(double[] d) {
		double[] r = new double[d.length];
		for(int i=0;i<d.length;++i)
			r[i] = d[i]*d[i];
		return r;
	}
	
	public static double[] log(double[] d) {
		double[] r = new double[d.length];
		for(int i=0;i<r.length;i++)
			r[i] = MathUtils.safeLog(d[i]);
		return r;
	}
	
	public static double[][] log(double[][] d) {
		double[][] r = new double[d.length][d[0].length];
		for(int i=0;i<r.length;i++)
			for(int j=0;j<d[0].length;j++)
				r[i][j] = MathUtils.safeLog(d[i][j]);
		return r;
	}
	
	public static double[] log(int[] d) {
		double[] r = new double[d.length];
		for(int i=0;i<r.length;i++)
			r[i] = MathUtils.safeLog(d[i]);
		return r;
	}
	
	public static double[] exp(double[] d) {
		double[] r = new double[d.length];
		for(int i=0;i<r.length;i++)
			r[i] = Math.exp(d[i]);
		return r;
	}
	
	public static double[][] exp(double[][] d) {
		double[][] ret = new double[d.length][d[0].length];
		for(int i=0;i<d.length;i++)
			ret[i] = exp(d[i]);
		return ret;
	}
	
	public static double[] digamma(double[] in) {
		double[] out = new double[in.length];
		for(int i=0;i<out.length;++i)
			out[i] = MathUtils.digamma(in[i]);
		return out;
	}
	
	public static double[][] digamma(double[][] in) {
		double[][] out = new double[in.length][];
		for(int i=0;i<in.length;++i)
			out[i] = digamma(in[i]);
		return out;
	}
	
	public static double dotProduct(double[] a, double[] b) {
		assert(a.length == b.length);
		double ret = 0;
		for(int i=0;i<a.length;i++)
			ret += a[i]*b[i];
		return ret;
	}
	
	public static double dotProductLogSpace(double[] a, double[] b) {
		assert(a.length == b.length);
		double ret = 0;
		for(int i=0;i<a.length;i++)
			ret = MathUtils.logAdd(ret, a[i]+b[i]);
		return ret;
	}
	
	public static double[] dotProduct(double[] a, double[][] b) {
		double[] ret = new double[b[0].length];
		
		for(int i=0;i<a.length;i++) {
			for(int j=0;j<b[0].length;j++)
				ret[j] += a[i]*b[i][j];
		}
		return ret;
	}
	
	public static double[] dotProduct(double[][] a, double[] b) {
		double[] ret = new double[b.length];
		
		for(int i=0;i<b.length;i++) {
			for(int j=0;j<a.length;j++)
				ret[j] += a[j][i]*b[i];
		}
		return ret;
	}

	public static double[] dotProductLogSpace(double[] a, double[][] b) {
		double[] ret = new double[b[0].length];
		
		for(int j=0;j<b[0].length;j++)
			for(int i=0;i<a.length;i++) {
				ret[j] = MathUtils.logAdd(ret[j], a[i]+b[i][j]);
			}
		return ret;
	}

	public static void setColumn(double[][] target, int colId, double[] src) {
		for(int row=0;row<target.length;++row) {
			target[row][colId] = src[row];
		}
	}

}
