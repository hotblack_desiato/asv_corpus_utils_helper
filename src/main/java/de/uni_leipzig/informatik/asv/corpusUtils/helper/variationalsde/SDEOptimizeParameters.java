/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde;

import org.apache.commons.math3.linear.RealMatrix;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class SDEOptimizeParameters {

	private int dimensions;
	private SDEVBInferencer inf;
	private int time;
	private RealMatrix sdeDerivB;
	/**
	 * @return the dimensions
	 */
	public int getDimensions() {
		return this.dimensions;
	}
	/**
	 * @param dimensions the dimensions to set
	 */
	public void setDimensions(int _dimensions) {
		this.dimensions = _dimensions;
	}
	/**
	 * @return the inf
	 */
	public SDEVBInferencer getInf() {
		return this.inf;
	}
	/**
	 * @param inf the inf to set
	 */
	public void setInf(SDEVBInferencer _inf) {
		this.inf = _inf;
	}
	/**
	 * @return the time
	 */
	public int getTime() {
		return this.time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(int _time) {
		this.time = _time;
	}
	/**
	 * @param deriv
	 */
	public void setSdeDerivB(RealMatrix deriv) {
		this.sdeDerivB = deriv;
	}
	/**
	 * @return
	 */
	public RealMatrix getSdeDerivB() {
		return this.sdeDerivB;
	}
	
	

	
}
