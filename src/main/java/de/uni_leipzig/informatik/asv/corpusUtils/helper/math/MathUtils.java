/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.math;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.util.Random;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.math3.special.Gamma;

import cern.jet.random.engine.RandomEngine;

public class MathUtils {
	
	private static boolean jmathLibPresent;

	static {
		URL u = MathUtils.class.getResource(".");
		String libPath = ".";
		String libName = "";
		
		String osString = System.getProperty("os.name");
		switch(osString) {
		case "Mac OS X":
			libName = "libjmath_mac.so";
			jmathLibPresent = true;
			break;
		case "Linux":
			libName = "libjmath_linux.so";
			jmathLibPresent = true;
			break;
		default:
			System.out.println("currently no shared library for " + osString);
			jmathLibPresent = false;
			break;
		}
		if(jmathLibPresent) {
			if(u != null) {
				try {
					File f = new File(u.toURI());
					libPath = f.getAbsolutePath();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			} else {
				try {
					//compiled jar case
					CodeSource src = MathUtils.class.getProtectionDomain().getCodeSource();
					if (src != null) {
					  URL jar = src.getLocation();
					  ZipInputStream zip = new ZipInputStream(jar.openStream());
					  ZipEntry ze = null;
					  while((ze = zip.getNextEntry()) != null) {
						  String name = ze.getName();
						  //extract lib name
						  String tempDir = System.getProperty("java.io.tmpdir");
						  if(name.contains(libName)) {
							  int lastSlash = name.lastIndexOf("/")+1;
							  name = name.substring(lastSlash);
	
							  File tmpFile = File.createTempFile(name, "", new File(tempDir));
							  tmpFile.deleteOnExit();
							  
							  libName = tmpFile.getName();
							  
							  FileOutputStream fos = new FileOutputStream(tmpFile);
							  byte[] buf = new byte[(int)ze.getSize()];
							  int bytesRead = 0;
							  while((bytesRead = zip.read(buf, 0, buf.length)) > -1) {
								  byte[] sub = buf;
								  if(bytesRead < buf.length) {
									  sub = new byte[bytesRead];
									  System.arraycopy(buf, 0, sub, 0, bytesRead);
								  }
								  fos.write(sub);
							  }
							  fos.close();
						  }
					  }
					} else {
					  /* Fail... */
						System.err.println("could not read so files");
						jmathLibPresent = false;
					}
				} catch (IOException e) {
					System.out.println("something went wrong...");
					e.printStackTrace();
				}
				libPath = System.getProperty("java.io.tmpdir");
			}
			try {
				System.load(libPath + File.separator + libName);
			} catch (UnsatisfiedLinkError | SecurityException e) {
				System.err.println("Could not load jLibMath, unsatisfied link or not allowed to do so");
				jmathLibPresent = false;
			}
		}
	}
	
	private static final double log2Pi = Math.log(2*Math.PI);

	private static Random rng = new Random(System.currentTimeMillis());
	
//	private static RandomSamplers randSampler = new RandomSamplers(rng);
	
	private static RandomEngine re = RandomEngine.makeDefault();
	
	private static double C_LIMIT = 6;
	private static double S_LIMIT = 1e-5;
    private static double GAMMA = 0.577215664901532860606512090082;
    
	public static int drawFromDistribution(double[] distr) {
		double[] r = new double[distr.length];
		double sum = 0;
		for(int i=0;i<distr.length;i++) {
			sum += distr[i];
			r[i] = sum;
		}
		double a = rng.nextDouble() * sum;
		for(int i=0;i<r.length;i++){
			if(r[i]>a) return i;
		}
		return distr.length - 1;
	}
	
	public static int nextInt(int max) {
		return rng.nextInt(max);
	}
	
	public static double nextDouble() {
		return rng.nextDouble();
	}
	
	public static int drawFromLogDistribution(double[] d) {
		double[] cumSum = new double[d.length];
		cumSum[0] = Math.exp(d[0]);
		for(int i=1;i<d.length;i++) {
			cumSum[i] = cumSum[i-1] + Math.exp(d[i]);
		}
		double rand = rng.nextDouble() * cumSum[cumSum.length - 1];
		for(int i=0;i<cumSum.length;i++) {
			if(rand<cumSum[i])
				return i;
		}
		return d.length-1;
	}
	
	public static int bernoulliDraw(double p) {
		double rand = rng.nextDouble();
		if(rand > p)
			return 0;
		return 1;
	}
	
	public static double logAdd(double val1, double val2) {
		double p = Math.max(val1, val2);
		double q = Math.min(val1, val2);
		return p + safeLog(1d + Math.exp(q-p));
	}
	
	public static double logSubtract(double val1, double val2) {
		if (val1 < val2) 
			return -1000.0;
		return val1 + safeLog(1 - Math.exp(val2-val1));

	}
	
	
	public static int drawFromNormalizedDistribution(double[] dist) {
		double a = 0;
		double b = rng.nextDouble();
		for(int i=0;i<dist.length;i++){
			a += dist[i];
			if(a>b) return i;
		}
		return dist.length - 1;
	}
	
	public static double[] drawFromDirichlet(double[] concentrationParams) {
		double[] y = new double[concentrationParams.length];
		cern.jet.random.Gamma g = new cern.jet.random.Gamma(1, 1, re);
		for(int i=0;i<y.length;i++) {
			y[i] = g.nextDouble(concentrationParams[i], 1.);
		}
		return normalizeDistribution(y);
	}
	
	public static double[] logDrawFromDirichlet(double[] conParams) {
		double[] y = new double[conParams.length];
		for(int i=0;i<conParams.length;i++) {
			y[i] = loggamma(conParams[i]);
		}
		return normalizeLogDistribution(y);
	}
	
	public static int drawNumberOfTablesFromCRP(double concentrationParam, int numberOfCustomers) {
		if(numberOfCustomers == 0) return 0; //when there are no customers, no table will be used
		int numberOfTables = 1; //first customer always sits at first table
		int[] numberOfCustomersPerTable = new int[]{1};
		double[] p;
		for(int i=1;i<numberOfCustomers;i++) { //starting with second customer
			double constant = (concentrationParam + i - 1);
			p = new double[numberOfTables + 1];
			for(int t=0;t<numberOfTables;t++) {
				p[t] = (double) numberOfCustomersPerTable[t] / constant;
			}
			p[numberOfTables] = concentrationParam / constant;
			int chosenTable = drawFromDistribution(p);
			if(chosenTable == numberOfTables) { //new table chosen, increase numberOfTables and resize numberOfCustomersPerTable
				numberOfTables++;
				int[] numberOfCustomersPerTableNew = new int[numberOfTables];
				System.arraycopy(numberOfCustomersPerTable, 0, numberOfCustomersPerTableNew, 0, numberOfCustomersPerTable.length);
				numberOfCustomersPerTableNew[numberOfTables-1] = 1;
				numberOfCustomersPerTable = numberOfCustomersPerTableNew;
			} else 
				numberOfCustomersPerTable[chosenTable]++;
		}
		return numberOfTables;
	}
	
	public static double[] normalizeDistribution(double[] distr) {
		return VectorUtils.divideByScalar(distr, computeNormalizer(distr));
	}
	
	public static double[] normalizeLogDistribution(double[] logDistr) {
		double[] r = new double[logDistr.length];
		double logsum = logDistr[0];
		for(int i=1;i<logDistr.length;i++) {
			logsum = logAdd(logsum, logDistr[i]);
		}
		for(int i=0;i<logDistr.length;i++) {
			r[i] = logDistr[i] - logsum;
		}
		return r;
	}
	
	private static double computeNormalizer(double[] distr) {
		double sum = 0;
		for(double a : distr) sum += a;
		return sum;
	}
	
	public static double gamma(double x) {
//		double val = loggamma(x);
//		return Math.exp(val);
		return randgamma(x);
	}
	
	public static double digammaExternal(double x) {
		return Gamma.digamma(x);
	}
	
	public static double trigammaExternal(double x) {
		return Gamma.trigamma(x);
	}
	
//	public static double digamma(double x) {
//	    double localx = x+6;
//	    double p = 1/(localx*localx);
//	    p = (((0.004166666666667*p-0.003968253986254)*p+0.008333333333333)*p-0.083333333333333)*p;
//	    p = p+safeLog(localx)-0.5/localx-1/(localx-1)-1/(localx-2)-1/(localx-3)-1/(localx-4)-1/(localx-5)-1/(localx-6);
////		double localX = x, result = 0;
////
////		while(true) {
////		    if (localX > 0 && localX <= S_LIMIT) {
////	            // use method 5 from Bernardo AS103
////	            // accurate to O(x)
////	            result += -GAMMA - 1 / localX;
////	            break;
////	        }
////	
////	        if (localX >= C_LIMIT) {
////	            // use method 4 (accurate to O(1/x^8)
////	            double inv = 1 / (localX * localX);
////	            //            1       1        1         1		   1		 5		     691		 7
////	            // log(x) -  --- - ------ + ------- - ------- + ------- - -------- + ---------- - ------- 
////	            //           2 x   12 x^2   120 x^4   252 x^6	240 x^8	  660 x^10	 32760 x^12	  84 x^14
////	            result += Math.log(localX) - 0.5 / localX - inv * ((1.0 / 12) + inv * (1.0 / 120 - inv * ( 1. / 252))); // + inv * (1./240 - inv * (5./660 + inv * (691./32760 - inv * 7./84))))));
////	            break;
////	        }
////	        
////	        result -= 1./localX;
////	        localX += 1;
////		}
////        return digamma(x + 1) - 1 / x;
////		return result;
//	    return p;
//	}

//	public static double trigamma(double x) {
//	    if (x > 0 && x <= S_LIMIT) {
//	    	return 1 / (x * x);
//	    }
//	    if (x >= C_LIMIT) {
//	    	double inv = 1 / (x * x);
//	    	//  1    1      1       1       1
//    		//  - + ---- + ---- - ----- + -----
//	    	//  x      2      3       5       7
//	    	//      2 x    6 x    30 x    42 x
//	    	return 1 / x + inv / 2 + inv / x * (1.0 / 6 - inv * (1.0 / 30 + inv / 42));
//	    }
//	    return trigamma(x + 1) + 1 / (x * x);
////		return Gamma.trigamma(x);
//	}

//	   public static double digammaExt2 (double x) {
//		      final double C7[][] = {
//		       {1.3524999667726346383e4, 4.5285601699547289655e4, 4.5135168469736662555e4,
//		        1.8529011818582610168e4, 3.3291525149406935532e3, 2.4068032474357201831e2,
//		        5.1577892000139084710, 6.2283506918984745826e-3},
//		       {6.9389111753763444376e-7, 1.9768574263046736421e4, 4.1255160835353832333e4,
//		          2.9390287119932681918e4, 9.0819666074855170271e3,
//		          1.2447477785670856039e3, 6.7429129516378593773e1, 1.0}
//		      };
//		      final double C4[][] = {
//		       {-2.728175751315296783e-15, -6.481571237661965099e-1, -4.486165439180193579,
//		        -7.016772277667586642, -2.129404451310105168},
//		       {7.777885485229616042, 5.461177381032150702e1,
//		        8.929207004818613702e1, 3.227034937911433614e1, 1.0}
//		      };
//
//		      double prodPj = 0.0;
//		      double prodQj = 0.0;
//		      double digX = 0.0;
//
//		      if (x >= 3.0) {
//		         double x2 = 1.0 / (x * x);
//		         for (int j = 4; j >= 0; j--) {
//		            prodPj = prodPj * x2 + C4[0][j];
//		            prodQj = prodQj * x2 + C4[1][j];
//		         }
//		         digX = Math.log (x) - (0.5 / x) + (prodPj / prodQj);
//
//		      } else if (x >= 0.5) {
//		         final double X0 = 1.46163214496836234126;
//		         for (int j = 7; j >= 0; j--) {
//		            prodPj = x * prodPj + C7[0][j];
//		            prodQj = x * prodQj + C7[1][j];
//		         }
//		         digX = (x - X0) * (prodPj / prodQj);
//
//		      } else {
//		         double f = (1.0 - x) - Math.floor (1.0 - x);
//		         digX = digammaExt2(1.0 - x) + Math.PI / Math.tan (Math.PI * f);
//		      }
//
//		      return digX;
//		   }
	
	private static native double gslLgamma(double x);
	
	private static native double gslDigamma(double x);
	
	private static native double gslTrigamma(double x);

	public static double lgamma(double x) {
		if(jmathLibPresent)
			return gslLgamma(x);
		return Gamma.logGamma(x);
	}
	
	public static double digamma(double x) {
		if(jmathLibPresent)
			return gslDigamma(x);
		return Gamma.digamma(x);
	}
	
	public static double trigamma(double x) {
		if(jmathLibPresent)
			return gslTrigamma(x);
		return Gamma.trigamma(x);
	}

	public static double loggamma(double x) {
		return lgamma(x);
	}
	
	public static double[] loggamma(double[] a) {
		double[] ret = new double[a.length];
		for(int i=0;i<a.length;i++)
			ret[i] = loggamma(a[i]);
		return ret;
	}
	
	public static double[][] loggamma(double[][] a){
		double[][] ret = new double[a.length][a[0].length];
		for(int i=0;i<a.length;i++)
			for(int j=0;j<a[0].length;j++)
				ret[i][j] = loggamma(a[i][j]);
		return ret;
	}
	
	
	public static double beta(double a, double b) {
	    double Ga, Gb;

	    if ((a <= 1.0) && (b <= 1.0))
	    {
	        double U, V, X, Y;
	        /* Use Jonk's algorithm */

	        while (true)
	        {
	            U = rng.nextDouble();
	            V = rng.nextDouble();
	            X = Math.pow(U, 1.0/a);
	            Y = Math.pow(V, 1.0/b);

	            if ((X + Y) <= 1.0)
	            {
	                return X / (X + Y);
	            }
	        }
	    }
	    else
	    {
	        Ga = gamma( a);
	        Gb = gamma(b);
	        return Ga/(Ga + Gb);
	    }
	}
	
	public static double logFactorial(int n, double a) {
		if(n==0) return 0;
		return loggamma(n+a) - loggamma(a);
	}

	public static double loggammaWithLogParam(double logX) {
		return loggamma(Math.exp(logX));
	}

	
	public static double[] stirlingNumber1stKind(int n) {
//		return randSampler.stirling(n);
//		int max = Math.max(n, m) + 1;
//		if(stirlingNumbers.length<max)
//			prepareStirlingNumbers(max);
//		return stirlingNumbers[n][m];
		
		return new double[]{0};
	}
	
	public static double[] dirichletExpectation(double[] params) {
		double[] exp = new double[params.length];
		double alphaSum = 0;
		for(int i=0;i<params.length;i++) {
			exp[i] = digamma(params[i]);
			alphaSum += params[i];
		}
		return VectorUtils.subtractScalar(exp, digamma(alphaSum));
	}
	
	public static double[] dirichletExpectationLogSpace(double[] params) {
		double[] exp = new double[params.length];
		double alphaSum = 0;
		for(int i=0;i<params.length;i++) {
			double expParam = Math.exp(params[i]);
			exp[i] = digamma(expParam);
			alphaSum += expParam;
		}
		return VectorUtils.subtractScalar(exp, digamma(alphaSum));
	}
	
	public static double[][] dirichletExpectation(double[][] params) {
		int X = params.length;
		int Y = params[0].length;
		
		double[][] exp = new double[X][Y];
		for(int x=0;x<X;x++) {
			double alphaSum = 0;
			for(int y=0;y<Y;y++) {
				exp[x][y] = digamma(params[x][y]);
				alphaSum += params[x][y];
			}
			exp[x] = VectorUtils.subtractScalar(exp[x], digamma(alphaSum));
		}
		return exp;
	}
	
	public static double[][] dirichletExpectationLogSpace(double[][] params) {
		int X = params.length;
		int Y = params[0].length;
		
		double[][] exp = new double[X][Y];
		for(int x=0;x<X;x++) {
			double alphaSum = 0;
			for(int y=0;y<Y;y++) {
				double expParam = Math.exp(params[x][y]);
				exp[x][y] = digamma(expParam);
				alphaSum += expParam;
			}
			exp[x] = VectorUtils.subtractScalar(exp[x], digamma(alphaSum));
		}
		return exp;
	}

	/**
	 * Produce gamma distributed random value given the parameter.
	 * This code has been adapted from Y.W.Teh's npbayes matlab/c code package
	 * found here: <a href="http://www.gatsby.ucl.ac.uk/~ywteh/research/npbayes/npbayes-r21.readme">
	 * http://www.gatsby.ucl.ac.uk/~ywteh/research/npbayes/npbayes-r21.readme</a>
	 * @param rr
	 * @return
	 */
	private static double randgamma(double rr) {
		  double bb, cc, dd;
		  double uu, vv, ww, xx, yy, zz;

		  if ( rr <= 0.0 ) {
		    /* Not well defined, set to zero and skip. */
		    return 0.0;
		  } else if ( rr == 1.0 ) {
		    /* Exponential */
		    return - safeLog(rng.nextDouble());
		  } else if ( rr < 1.0 ) {
		    /* Use Johnks generator */
		    cc = 1.0 / rr;
		    dd = 1.0 / (1.0-rr);
		    while (true) {
		      xx = Math.pow(rng.nextDouble(), cc);
		      yy = xx + Math.pow(rng.nextDouble(), dd);
		      if ( yy <= 1.0 ) {
		        return -safeLog(rng.nextDouble()) * (xx / yy);
		      }
		    }
		  } else { /* rr > 1.0 */
		    /* Use bests algorithm */
		    bb = rr - 1.0;
		    cc = 3.0 * rr - 0.75;
		    while (true) {
		      uu = rng.nextDouble();
		      vv = rng.nextDouble();
		      ww = uu * (1.0 - uu);
		      yy = Math.sqrt(cc / ww) * (uu - 0.5);
		      xx = bb + yy;
		      if (xx >= 0) {
		        zz = 64.0 * ww * ww * ww * vv * vv;
		        if ( ( zz <= (1.0 - 2.0 * yy * yy / xx) ) ||
		             ( safeLog(zz) <= 2.0 * (bb * safeLog(xx / bb) - yy) ) ) {
		          return xx;
		        }
		      }
		    }
		  }
		}
	

	public static void main(String args[]) {
		System.out.println("########## digamma ############");
		long start = System.currentTimeMillis();
		for(int i=0;i<10000000;++i)
			digammaExternal(.5);
		long dur = System.currentTimeMillis() - start;
		System.out.println("colt method: " + dur + "ms");
		start = System.currentTimeMillis();
		for(int i=0;i<10000000;++i)
			digamma(.5);
		dur = System.currentTimeMillis() - start;
		System.out.println("C method: " + dur + "ms");

		System.out.println("########## log-gamma ############");
		start = System.currentTimeMillis();
		for(int i=0;i<10000000;++i)
			Gamma.logGamma(.5);
		dur = System.currentTimeMillis() - start;
		System.out.println("colt method: " + dur + "ms");
		start = System.currentTimeMillis();
		for(int i=0;i<10000000;++i)
			lgamma(.5);
		dur = System.currentTimeMillis() - start;
		System.out.println("C method: " + dur + "ms");
	} 
	
	public static double safeLog(double val) {
		//safe log
		if(val < 0)
			return -1000d;
		return Math.log(val + 1e-100);
	}
	
}
