/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.sparsevb;

import java.util.Arrays;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

public class LogLikelihoodFunction extends ObjectiveFunction {


	protected LogLikelihoodFunction(int dim, LogLikelihoodOptimizeParameters p) {
		super(dim, p);
	}

	@Override
	public double f(double[] point) {
		LogLikelihoodOptimizeParameters p = this.getParameters();
		p.setXm(MatrixUtils.createColumnRealMatrix(point));
		p.doComputations();
		
		double n = p.getKnn().getColumnDimension();


		double ll = -.5 * n * Math.log(2*Math.PI) - .5*Math.log(p.getDetSigmaQ());
		ll -= .5 * p.getY().transpose().multiply(p.getSigmaQInv()).multiply(p.getY()).getTrace();
		ll -= .5 * p.getTraceKQ() / p.getNoiseLevel();

		return -ll;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#df(double[], double[])
	 */
	@Override
	public void df(double[] point, double[] df) {
		LogLikelihoodOptimizeParameters p = this.getParameters();
		p.setXm(MatrixUtils.createColumnRealMatrix(point));
		p.doComputations();
		Arrays.fill(df, 0);
		RealMatrix v = p.getSigmaQInv().multiply(p.getY());
		for(int i=0;i<df.length;++i) {
			RealMatrix dQnn =  computeQnnDerivative(point, i, p);
			df[i] = -.5 * p.getSigmaQInv().multiply(dQnn).getTrace() 
					+ .5 * v.transpose().multiply(dQnn).multiply(v).getTrace()
					+ .5 * dQnn.getTrace()/p.getNoiseLevel();

		}
		VectorUtils.neg_(df);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#fdf(double[], double[])
	 */
	@Override
	public double fdf(double[] point, double[] df) {
		LogLikelihoodOptimizeParameters p = this.getParameters();
		p.setXm(MatrixUtils.createColumnRealMatrix(point));
		p.doComputations();
		int n = p.getY().getRowDimension();
		
		RealMatrix v = p.getSigmaQInv().multiply(p.getY());

		double ll = -.5 * n * Math.log(2*Math.PI) - .5*Math.log(p.getDetSigmaQ());
		ll -= .5 * p.getY().transpose().multiply(p.getSigmaQInv()).multiply(p.getY()).getTrace();
		ll -= .5 * p.getTraceKQ() / p.getNoiseLevel();

		for(int i=0;i<df.length;++i) {
			RealMatrix dQnn =  computeQnnDerivative(point, i, p);
			df[i] = -.5 * p.getSigmaQInv().multiply(dQnn).getTrace() 
					+ .5 * v.transpose().multiply(dQnn).multiply(v).getTrace()
					+ .5 * dQnn.getTrace()/p.getNoiseLevel();

		}
		VectorUtils.neg_(df);
		return -ll;
	}

	
	private RealMatrix computeQnnDerivative(double[] point, int inducingIndex, LogLikelihoodOptimizeParameters p) {
		RealMatrix dQnn = MatrixUtils.createRealMatrix(p.getX().getRowDimension(), p.getX().getRowDimension()); 
		
		RealMatrix dKmm = MatrixUtils.createRealMatrix(getDimensions(), getDimensions());
		for(int i=0;i<point.length;++i) {
			dKmm.setEntry(i, inducingIndex, p.getModel().getCovFunction().computeCovarianceDerivX(new double[]{point[inducingIndex]}, new double[]{point[i]})[0] );
			dKmm.setEntry(inducingIndex, i, p.getModel().getCovFunction().computeCovarianceDerivXPrime(new double[]{point[i]}, new double[]{point[inducingIndex]})[0]);
		}
		
		dQnn = p.getKnm().multiply(
					p.getKmmInv().multiply(p.getdKmn())
					.subtract(
						p.getKmmInv().multiply(dKmm).multiply(p.getKmmInv()).multiply(p.getKnm().transpose())
					)
				).add(
						p.getdKnm().multiply(p.getKmmInv()).multiply(p.getKnm().transpose())
				);
		return dQnn;
	}

}
