/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov;

import java.util.Arrays;
import java.util.Date;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;

/**
 * Covariance function baseclass. Provides some basic convenience methods to
 * work with {@link Date} objects and defines the methodology to update
 * hyperparameters. Up to now, only scalar hyperparameters are supported,
 * vectorial parameters can be simulated as a list of scalar parameters.
 * 
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick
 *         Jaehnichen</a>
 * 
 */
public abstract class CovarianceFunction implements ICovarianceFunction {
	
	protected double[][] trainingDataCovariance;
	private double[][] inverseTrainingDataCovariance;
	private boolean covarianceChanged;
	protected ObjectiveFunction hyperparamUpdateFunction = null;
	protected double noiseLevel;
	
	
	/**
	 * @param noise
	 */
	protected CovarianceFunction(double noise) {
		this.noiseLevel = noise;
	}

	/*
	 * (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#getInverseCovarianceMatrix()
	 */
	@Override
	public double[][] getInverseCovarianceMatrix() {
		if(this.inverseTrainingDataCovariance == null || this.covarianceChanged) {
			RealMatrix cov = MatrixUtils.createRealMatrix(this.trainingDataCovariance);
			double[] jitterSource = ArrayFactory.doubleArray(de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type.ONE, this.trainingDataCovariance.length);
			jitterSource = VectorUtils.multiplyWithScalar(jitterSource, 1e-5);
			RealMatrix jitter = MatrixUtils.createRealDiagonalMatrix(jitterSource);
			CholeskyDecomposition decomp = new CholeskyDecomposition(cov.add(jitter));
			this.inverseTrainingDataCovariance = decomp.getSolver().getInverse().getData();
			this.covarianceChanged = false;
		}
		return this.inverseTrainingDataCovariance;
	}

	/**
	 * @return the trainingDataCovariance
	 */
	public double[][] getTrainingDataCovariance() {
		return this.trainingDataCovariance;
	}

	
	public void updateTrainingDataCovariance(double[][] covariates) {
		this.trainingDataCovariance = computeCovarianceMatrix(covariates);
		this.covarianceChanged = true;
	}

	/**
	 * Computes the covariance between two date objects after being processed by
	 * the convertDate method.
	 * 
	 * @param a Date a
	 * @param b Date b
	 * @return their covariance
	 */
	@Override
	public double computeCovariance(Date a, Date b) {
		return computeCovariance(convertDate(a), convertDate(b));
	}

	/**
	 * Computes the covariance matrix between Date objects. Before computation,
	 * the timestamps of the Date objects are normalized to lie in the intervall
	 * [0,1].
	 * 
	 * @param trainingPoints - a set of Date objects
	 * @return their covariance matrix
	 */
	@Override
	public double[][] computeCovarianceMatrix(Date[] trainingPoints) {
		return computeCovarianceMatrix(convertDates(trainingPoints));
	}

	/**
	 * Computes the cross covariance between two sets of Date objects. Before
	 * computation, the timestamps of the Date objects are normalized to lie in
	 * the intervall [0,1].
	 * 
	 * @param unseenPoints - the first set of Date objects
	 * @param trainingPoints - the second set of Date objects
	 * @return their cross covariance matrix
	 */
	@Override
	public double[][] computeCrossCovariance(Date[] unseenPoints,
			Date[] trainingPoints) {
		return computeCrossCovarianceMatrix(convertDates(unseenPoints),
				convertDates(trainingPoints));
	}

	/**
	 * Set the new covariance function hyperparameters.
	 * @param newHyperparams - the new hyperparameters
	 */
	public abstract void setHyperparameters(double[] newHyperparams);

	/**
	 * Provide a function that computes the marginal likelihood with the given
	 * hyperparameter setting and the appropriate gradients
	 * @param gp - the Gaussian process for which the computations are done
	 * @return a subclass of {@link ObjectiveFunction}
	 */
	protected abstract ObjectiveFunction getHyperparameterObjectiveFunction(
			AbstractGaussianProcess gp);

	/*
	 * (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#updateHyperparameters(de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess)
	 */
	@Override
	public void updateHyperparameters(AbstractGaussianProcess gp) {
		// fr recode from gsl
		FletcherReevesConjugateGradientOptimizer opt = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);

		ObjectiveFunction f = getHyperparameterObjectiveFunction(gp);

		double[] currentHyperparams = getHyperparameters();
		
		Optimizer.optimize(currentHyperparams, .35, .1, 1e-10, 30, f, opt);
		
		setHyperparameters(opt.getX());
		System.out.println(Arrays.toString(currentHyperparams) + " --> " + Arrays.toString(this.getHyperparameters()));
	}



	/**
	 * @param hyperparamUpdateFunction the hyperparamUpdateFunction to set
	 */
	@Override
	public void setHyperparamUpdateFunction(ObjectiveFunction hyperparamUpdateFunction) {
		this.hyperparamUpdateFunction = hyperparamUpdateFunction;
	}

	@Override
	public double[][] computeCovarianceMatrix(double[][] trainingPoints) {
		double[][] tCov = new double[trainingPoints.length][trainingPoints.length];
		for(int i=0;i<trainingPoints.length;++i) {
			for(int j=i;j<trainingPoints.length;++j) {
				tCov[i][j] = computeCovariance(trainingPoints[i], trainingPoints[j]);
				if(i==j) {
					tCov[i][j] += this.noiseLevel;
					continue;
				}
				tCov[j][i] = tCov[i][j]; 
			}
		}
		this.trainingDataCovariance = tCov;
		return tCov;
	}

	@Override
	public double[][] computeCrossCovarianceMatrix(double[][] unseenPoints, double[][] trainingPoints) {
		double[][] cov = new double[unseenPoints.length][trainingPoints.length];
		for(int i=0;i<unseenPoints.length;++i) {
			for(int j=0;j<trainingPoints.length;++j) {
				cov[i][j] = computeCovariance(unseenPoints[i], trainingPoints[j]);
//				if(unseenPoints[i] == trainingPoints[j])
//					cov[i][j] += noiseLevel;
			}
		}
		return cov;
	}


	@Override
	public double[][][] computeCovarianceMatrixDerivX(double[][] points) {
		int dims = points[0].length;
		double[][][] cov = new double[points.length][points.length][dims];
		for(int i=0;i<points.length;++i) {
			for(int j=0;j<i;++j) {
				if(i==j) {
					cov[i][j] = new double[dims];
					continue;
				}
				cov[i][j] = computeCovarianceDerivX(points[i], points[j]);
				cov[j][i] = cov[i][j];
			}
		}
		return cov;
	}

	@Override
	public double[][][] computeCrossCovarianceMatrixDerivX(double[][] x, double[][] x_m) {
		int dims = x[0].length;
		double[][][] cov = new double[x.length][x_m.length][dims];
		for(int i=0;i<x.length;++i) {
			for(int j=0;j<x_m.length;++j) {
				cov[i][j] = computeCovarianceDerivX(x[i], x_m[j]);
			}
		}
		return cov;
	}

	@Override
	public double[][][] computeCovarianceMatrixDerivXPrime(double[][] points) {
		int dims = points[0].length;
		double[][][] cov = new double[points.length][points.length][dims];
		for(int i=0;i<points.length;++i) {
			for(int j=0;j<i;++j) {
				if(i==j) {
					cov[i][j] = new double[dims];
					continue;
				}
				cov[i][j] = computeCovarianceDerivXPrime(points[i], points[j]);
				cov[j][i] = cov[i][j];
			}
		}
		return cov;
	}

	@Override
	public double[][][] computeCrossCovarianceMatrixDerivXPrime(double[][] x,
			double[][] x_m) {
				int dims = x_m[0].length;
				double[][][] cov = new double[x.length][x_m.length][dims];
				for(int i=0;i<x.length;++i) {
					for(int j=0;j<x_m.length;++j) {
						cov[i][j] = computeCovarianceDerivXPrime(x[i], x_m[j]);
					}
				}
				return cov;
			}

	/**
	 * scales the given set of dates to the positive real interval [0,1]
	 * 
	 * @param dates
	 * @return
	 */
	public static double[][] convertDates(Date[] dates) {
		// make sure we are working with a sorted array of dates
		Arrays.sort(dates);

		double offset = dates[0].getTime() / (1000*3600*24);
		double diff = dates[dates.length - 1].getTime()/(1000*3600*24) - offset;

		double[][] ret = new double[dates.length][];
		for (int i = 0; i < dates.length; ++i) {
//			ret[i] = new double[] { (dates[i].getTime() - offset)
//					/ diff };
			ret[i] = convertDate(dates[i]);
			ret[i][0] += (- offset + 1);
			ret[i][0] /= diff;
		}

		return ret;
	}

	private static double[] convertDate(Date d) {
		return new double[] { d.getTime() /(1000*3600*24) };// /((double)1000)};
	}

}
