/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.HyperparameterOptimizationObjectiveFunction;

public class RBFObjectiveFunction extends HyperparameterOptimizationObjectiveFunction {


	public RBFObjectiveFunction(int dim, AbstractGaussianProcess in) {
		super(dim, in);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.HyperparameterOptimizationObjectiveFunction#computeGradient(double[])
	 */
	@Override
	protected void computeGradient(double[] df) {
		GaussianKernel k = getGP().getCovFunction();
		RealMatrix derivS = MatrixUtils.createRealMatrix(k.computeCovarianceMatrixDerivSigma(getGP().getDataCovariates()));
		RealMatrix derivL = MatrixUtils.createRealMatrix(k.computeCovarianceMatrixDerivLengthScale(getGP().getDataCovariates()));

		RealMatrix Kinv = MatrixUtils.createRealMatrix(k.getInverseCovarianceMatrix());
		RealMatrix alpha = Kinv.multiply(MatrixUtils.createColumnRealMatrix(getGP().getTargetData()));
		RealMatrix temp = alpha.multiply(alpha.transpose()).subtract(Kinv);
		
		df[0] = -.5* (temp).getTrace();
		df[1] = -.5* (temp.multiply(derivS)).getTrace();
		df[2] = -.5* (temp.multiply(derivL)).getTrace();
		
	}
	
}
