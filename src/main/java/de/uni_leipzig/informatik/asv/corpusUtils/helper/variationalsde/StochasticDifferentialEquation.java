/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public abstract class StochasticDifferentialEquation {
	
	private RealMatrix noise;
	private RealMatrix noiseInv;

	private double[] hyperparameters;
	
	public StochasticDifferentialEquation(double[][] processNoise, double[] hyperparameters) {
		this.noise = MatrixUtils.createRealMatrix(processNoise);
		invertNoiseMatrix();
		this.hyperparameters = hyperparameters;
	}

	public abstract RealMatrix driftFunction(RealMatrix X, int k);
	
	public abstract RealMatrix driftFunctionExpectedDerivativeX(RealMatrix expectationX, RealMatrix varianceX, int k);
	
	public abstract RealMatrix driftFunctionExpectation(RealMatrix expectationX, RealMatrix varianceX, int k);
	
	public abstract RealMatrix driftFunctionTimesXExpectation(RealMatrix expectationX, RealMatrix varianceX, int k);
	
	public abstract RealMatrix squaredDriftFunctionExpectation(RealMatrix expectationX, RealMatrix varianceX, int k);

	public abstract double[] driftFunctionDerivativeHyperparameters();

	/**
	 * @return the hyperparameters
	 */
	public double[] getHyperparameters() {
		return this.hyperparameters;
	}

	/**
	 * @param hyperparameters the hyperparameters to set
	 */
	public void setHyperparameters(double[] _hyperparameters) {
		this.hyperparameters = _hyperparameters;
	}

	/**
	 * @return the noise
	 */
	public RealMatrix getNoise() {
		return this.noise;
	}

	/**
	 * @param noise the noise to set
	 */
	public void setNoise(RealMatrix _noise) {
		if(this.noise.equals(_noise))
			return;
		this.noise = _noise;
		invertNoiseMatrix();
	}

	/**
	 * @return the noiseInv
	 */
	public RealMatrix getNoiseInv() {
		return this.noiseInv;
	}
	
	
	private void invertNoiseMatrix() {
		CholeskyDecomposition cd = new CholeskyDecomposition(this.noise);
		this.noiseInv = cd.getSolver().getInverse();
		cd = null;
	}

	/**
	 * @param variationalFunctionA 
	 * @param variationalFunctionB 
	 * @param posteriorVariance 
	 * @param posteriorMean 
	 * @param k
	 * @return
	 */
	public abstract RealMatrix energyDerivativeVariance(RealMatrix posteriorMean, RealMatrix posteriorVariance, RealMatrix variationalFunctionB, RealMatrix variationalFunctionA, int k);

	/**
	 * @param realMatrix
	 * @param realMatrix2
	 * @param realMatrix3
	 * @param realMatrix4
	 * @param k
	 * @return
	 */
	public abstract RealMatrix energyDerivativeMean(RealMatrix posteriorMean, RealMatrix posteriorVariance, RealMatrix variationalFunctionB, RealMatrix variationalFunctionA, int k);

	protected static RealMatrix normalSecondMoment(RealMatrix expectationX, RealMatrix varianceX) {
		return expectationX.multiply(expectationX).add(varianceX);
	}

	public static RealMatrix normalSecondMomentDerivMean(RealMatrix expectationX) {
		return expectationX.scalarMultiply(2);
	}

	public static RealMatrix normalSecondMomentDerivVar(RealMatrix varianceX) {
		return MatrixUtils.createRealIdentityMatrix(varianceX.getRowDimension());
	}

	public static RealMatrix normalThirdMomentDerivMean(RealMatrix expectationX, RealMatrix varianceX) {
		RealMatrix m2 = expectationX.multiply(expectationX);
		return m2.scalarMultiply(3).add(varianceX).scalarMultiply(3);
	}

	public static RealMatrix normalThirdMomentDerivVar(RealMatrix expectationX) {
		return expectationX.scalarMultiply(3);
	}

	public static RealMatrix normalFourthMoment(RealMatrix expectationX, RealMatrix varianceX) {
		RealMatrix m2 = expectationX.multiply(expectationX);
		RealMatrix m4 = m2.multiply(m2);
		RealMatrix s2 = varianceX.multiply(varianceX);
		return m4.add(m2.multiply(varianceX).scalarMultiply(6)).add(s2.scalarMultiply(3));
	}

	public static RealMatrix normalFourthMomentDerivMean(RealMatrix expectationX, RealMatrix varianceX) {
		RealMatrix m2 = expectationX.multiply(expectationX);
		RealMatrix m3= m2.multiply(expectationX);
		return m3.scalarMultiply(4).add(expectationX.multiply(varianceX).scalarMultiply(12));
	}

	public static RealMatrix normalFourthMomentDerivVar(RealMatrix expectationX, RealMatrix varianceX) {
		RealMatrix m2 = expectationX.multiply(expectationX);
		return m2.scalarMultiply(6).add(varianceX.scalarMultiply(6));
	}

	public static RealMatrix normalSixthMoment(RealMatrix expectationX, RealMatrix varianceX) {
		RealMatrix m2 = expectationX.multiply(expectationX);
		RealMatrix m4 = m2.multiply(m2);
		RealMatrix m6 = m4.multiply(m2);
		RealMatrix s2 = varianceX.multiply(varianceX);
		RealMatrix s3 = varianceX.multiply(s2);
		return m6.add(m4.multiply(varianceX).scalarMultiply(15)).add(m2.multiply(s2).scalarMultiply(45)).add(s3.scalarMultiply(15));		
	}

	public static RealMatrix normalSixthMomentDerivMean(RealMatrix expectationX, RealMatrix varianceX) {
		RealMatrix m2 = expectationX.multiply(expectationX);
		RealMatrix m3 = m2.multiply(expectationX);
		RealMatrix m5 = m3.multiply(m2);
		RealMatrix s2 = varianceX.multiply(varianceX);
		
		return m5.scalarMultiply(6).add(m3.multiply(varianceX).scalarMultiply(60)).add(expectationX.multiply(s2).scalarMultiply(90));
	}

	public static RealMatrix normalSixthMomentDerivVar(RealMatrix expectationX, RealMatrix varianceX) {
		RealMatrix m2 = expectationX.multiply(expectationX);
		RealMatrix m4 = m2.multiply(m2);
		RealMatrix s2 = varianceX.multiply(varianceX);
		return m4.scalarMultiply(15).add(m2.multiply(s2).scalarMultiply(90)).add(s2.scalarMultiply(45));
	}
	

	
}
