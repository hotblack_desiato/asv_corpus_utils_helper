/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.ou;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.norm;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.subtract;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class OrnsteinUhlenbeckKernel extends CovarianceFunction {

	private double sigmaSquare;
	private double lengthScale;
	
	/**
	 * @param noise
	 */
	public OrnsteinUhlenbeckKernel(double noise, double sigmaSquare, double lengthScale) {
		super(noise);
		this.sigmaSquare = sigmaSquare;
		this.lengthScale = lengthScale;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovariance(double[], double[])
	 */
	@Override
	public double computeCovariance(double[] a, double[] b) {
		double n = norm(subtract(a, b));
		return this.sigmaSquare * Math.exp(-.5*n/(this.lengthScale));
	}
	
	//derivatives	
	//dk/dsigma2
	public double computeCovarinceDerivSigma(double[] a, double[] b) {
		double n = norm(subtract(a, b));
		return Math.exp(-.5*n/(this.lengthScale));
	}
	
	//dK/dsigma2
	public double[][] computeCovarianceMatrixDerivSigma(double[][] trainingPoints) {
		double[][] tCov = new double[trainingPoints.length][trainingPoints.length];
		for(int i=0;i<trainingPoints.length;++i) {
			for(int j=i;j<trainingPoints.length;++j) {
				if(i==j) {
					tCov[i][j] = 1;
					continue;
				}
				tCov[i][j] = computeCovarinceDerivSigma(trainingPoints[i], trainingPoints[j]);
				tCov[j][i] = tCov[i][j]; 
			}
		}
		return tCov;
	}
	
	
	//dk/dl
	public double computeCovarinceDerivLengthScale(double[] a, double[] b) {
		double n = norm(subtract(a, b));
		double derivL = n/(this.lengthScale*this.lengthScale);
		if(n == 0)
			return this.sigmaSquare*derivL;
		return this.sigmaSquare * Math.exp(-.5*n/(this.lengthScale)) * derivL;
	}
	
	//dK/dl
	public double[][] computeCovarianceMatrixDerivLengthScale(double[][] trainingPoints) {
		double[][] tCov = new double[trainingPoints.length][trainingPoints.length];
		for(int i=0;i<trainingPoints.length;++i) {
			for(int j=i;j<trainingPoints.length;++j) {
				tCov[i][j] = computeCovarinceDerivLengthScale(trainingPoints[i], trainingPoints[j]);
				if(i==j) {
					continue;
				}
				tCov[j][i] = tCov[i][j]; 
			}
		}
		return tCov;
	}


	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivXPrime(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivXPrime(double[] a, double[] b) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#computeCovarianceDerivX(double[], double[])
	 */
	@Override
	public double[] computeCovarianceDerivX(double[] a, double[] b) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction#getHyperparameters()
	 */
	@Override
	public double[] getHyperparameters() {
		return new double[]{this.noiseLevel, this.sigmaSquare, this.lengthScale};
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#setHyperparameters(double[])
	 */
	@Override
	public void setHyperparameters(double[] newHyperparams) {
		this.noiseLevel = newHyperparams[0];
		this.sigmaSquare = newHyperparams[1];
		this.lengthScale = newHyperparams[2];
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction#getHyperparameterObjectiveFunction(de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess)
	 */
	@Override
	protected ObjectiveFunction getHyperparameterObjectiveFunction(
			AbstractGaussianProcess gp) {
		if(this.hyperparamUpdateFunction == null)
			this.hyperparamUpdateFunction = new OrnsteinUhlenbeckObjectiveFunction(3, gp);
		return this.hyperparamUpdateFunction;
	}

}
