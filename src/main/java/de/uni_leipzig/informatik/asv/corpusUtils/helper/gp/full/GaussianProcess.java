/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.dotProduct;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.IMeanFunction;

public class GaussianProcess extends AbstractGaussianProcess{

	private double[] alpha;
	
	private CholeskyDecomposition L;
	
	public GaussianProcess(IMeanFunction m, ICovarianceFunction k) {
		super(m, k);
	}
	

	
	public void train() {
		RealMatrix y = MatrixUtils.createColumnRealMatrix(this.targetData);
		this.alpha = this.L.getSolver().solve(y).getColumn(0);
	}
	
	
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess#setDataCovariates(double[][])
	 */
	@Override
	public void setDataCovariates(double[][] _x) {
		super.setDataCovariates(_x);
 		RealMatrix covMatrix = MatrixUtils.createRealMatrix(this.dataCovariateCovariance);
 		//decomposition cf. Rasmussen & Williams: Gaussian Processes for Machine Learning, Alg. 2.1
		this.L = new CholeskyDecomposition(covMatrix);
	}



	public double computeLikelihood() {
		double logMarginalLik = 0;
		logMarginalLik -= .5*dotProduct(this.targetData, this.alpha);
		double logDet = 0;
		for(int i=0;i<this.L.getL().getColumnDimension();++i)
			logDet += Math.log(this.L.getL().getEntry(i, i));
		logMarginalLik -= logDet;
		logMarginalLik -= .5*this.targetData.length*Math.log(2*Math.PI);
		return logMarginalLik;
	}

	/*
	 * (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess#predict(double[][])
	 */
	public void predict(double[][] unseenPoints) {

		this.predictiveVariance = new double[this.testData.length];
		this.predictiveMean = new double[this.testData.length];
		
//		unseenCov = this.covariance.computeCrossCovarianceMatrix(unseenPoints, this.dataCovariates);
		
		for(int i=0;i<unseenPoints.length;++i) {
			//predictive mean
			//TODO transpose the cross covariance matrix?
			this.predictiveMean[i] = dotProduct(this.dataTestDataCrossCovariance[i], this.alpha);
			
			//predictive variance
			double[] v = this.L.getSolver().solve(MatrixUtils.createColumnRealMatrix(this.dataTestDataCrossCovariance[i])).getColumn(0);
			this.predictiveVariance[i] = this.testDataCovariance[i][i] - dotProduct(v, v);
		}
	}

}
