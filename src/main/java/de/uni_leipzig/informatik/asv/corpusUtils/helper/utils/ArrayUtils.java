/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

public class ArrayUtils {
	public static void swap(int[] arr, int arg1, int arg2){
		   int t = arr[arg1]; 
		   arr[arg1] = arr[arg2]; 
		   arr[arg2] = t; 
	}
	
	public static void swap(int[][] arr, int arg1, int arg2) {
		   int[] t = arr[arg1]; 
		   arr[arg1] = arr[arg2]; 
		   arr[arg2] = t; 
	}
	
	public static double[] ensureCapacity(double[] arr, int min){
		int length = arr.length;
		if (min < length)
			return arr;
		double[] arr2 = new double[min*2];
		for (int i = 0; i < length; i++) 
			arr2[i] = arr[i];
		return arr2;
	}

	public static int[] ensureCapacity(int[] arr, int min) {
		int length = arr.length;
		if (min < length)
			return arr;
		int[] arr2 = new int[min*2];
		for (int i = 0; i < length; i++) 
			arr2[i] = arr[i];
		return arr2;
	}

	public static int[][] add(int[][] arr, int[] newElement, int index) {
		int length = arr.length;
		if (length <= index){
			int[][] arr2 = new int[index*2][];
			for (int i = 0; i < length; i++) 
				arr2[i] = arr[i];
			arr = arr2;
		}
		arr[index] = newElement;
		return arr;
	}

	public static double sum(double[] vec) {
		double r = 0;
		for(int i=0;i<vec.length;i++) {
			r += vec[i];
		}
		return r;
	}
	
	public static double[] getColumn(double[][] src, int column) {
		double[] ret = new double[src.length];
		for(int i=0;i<ret.length;++i)
			ret[i] = src[i][column];
		return ret;
	}
	
	public static void setColumn(double[][] _dest, double[] _src, int column) {
		assert(_dest.length == _src.length);
		for(int i=0;i<_dest.length;++i)
			_dest[i][column] = _src[i];
	}
	
	public static void setAll(double[][] _a, double val) {
		for(int i=0;i<_a.length;++i) {
			Arrays.fill(_a[i], val);
		}
	}
	
	public static void setAll(double[][][] _a, double val) {
		for(int i=0;i<_a.length;i++)
			setAll(_a[i], val);
	}
	
	public static double[] copyOf(double[] _in) {
		double[] out = new double[_in.length];
		System.arraycopy(_in, 0, out, 0, _in.length);
		return out;
	}
	
	public static double[][] copyOf(double[][] _in) {
		double[][] out = new double[_in.length][];
		for(int i=0;i<_in.length;++i) {
			out[i] = copyOf(_in[i]);
		}
		return out;
	}
	
	//IO
	
	public static void saveArrayToWriter(BufferedWriter writer, double[][] array) throws IOException {
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<array.length;i++) {
			for(int j=0;j<array[0].length;j++) {
				sb.append(array[i][j]).append("\t");
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append("\n");
			writer.write(sb.toString());
			sb.setLength(0);
		}
	}
	
	public static void saveArrayToFile(String filename, double[][] array) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
		saveArrayToWriter(bw, array);
		bw.close();
	}
	
	public static void saveArrayToWriter(BufferedWriter writer, int[][] array) throws IOException {
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<array.length;i++) {
			for(int j=0;j<array[0].length;j++) {
				sb.append(array[i][j]).append("\t");
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append("\n");
			writer.write(sb.toString());
			sb.setLength(0);
		}
	}

	public static void saveArrayToFile(String filename, int[][] array) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
		saveArrayToWriter(bw, array);
		bw.close();
	}
	
	public static double[][] loadDoubleArrayFromFile(String filename) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		Vector<double[]> lineArrays = new Vector<>();
		String line = "";
		while((line = br.readLine()) != null) {
			if(line.trim().equals(""))
				continue;
			String[] parts = line.split("\\s+");
			double[] curLine = new double[parts.length];
			for(int i=0;i<parts.length;i++) 
				curLine[i] = Double.valueOf(parts[i]).doubleValue();
			lineArrays.add(curLine);
		}
		br.close();
		return lineArrays.toArray(new double[][]{});
	}
	
	public static void saveDoubleVectorToFile(String filename, double[] vector) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
		for(double val : vector)
			bw.write(val + "\n");
		bw.close();
	}
	
	public static double[] loadDoubleVectorFromFile(String filename) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		Vector<String> lineArray = new Vector<>();
		String line = "";
		while((line = br.readLine()) != null) {
			if(line.trim().equals(""))
				continue;
			lineArray.add(line);
		}
		br.close();
		double[] ret = new double[lineArray.size()];
		for(int i=0;i<ret.length;++i)
			ret[i] = Double.parseDouble(lineArray.get(i));
		return ret;
	}

	public static int[][] loadIntArrayFromFile(String filename) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));
		Vector<int[]> lineArrays = new Vector<>();
		String line = "";
		while((line = br.readLine()) != null) {
			if(line.trim().equals(""))
				continue;
			String[] parts = line.split("\\s+");
			int[] curLine = new int[parts.length];
			for(int i=0;i<parts.length;i++) 
				curLine[i] = Integer.valueOf(parts[i]).intValue();
			lineArrays.add(curLine);
		}
		br.close();
		return lineArrays.toArray(new int[][]{});
	}

}
