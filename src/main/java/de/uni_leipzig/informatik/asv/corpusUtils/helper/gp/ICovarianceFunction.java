/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp;

import java.util.Date;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;
/**
 * A general covariance function for a Gaussian process. The covariance function defines the behavior of the process.
 * 
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public interface ICovarianceFunction {
	
	public enum Type {
		GAUSSIAN_KERNEL, BROWNIAN_MOTION;
	}
	/**
	 * the type of the Kernel
	 */
	public Type type = Type.GAUSSIAN_KERNEL;

	/**
	 * @return the trainingDataCovariance
	 */
	public double[][] getTrainingDataCovariance();

	/**
	 * @param trainingDataCovariance the trainingDataCovariance to set
	 */
	public void updateTrainingDataCovariance(double[][] covariates);

	
	/**
	 * Computes the covariance between two input points. 
	 * This corresponds to a direct implementation of the covariance function evaluation between two points.
	 * @param a the first point
	 * @param b the second point
	 * @return the covariance between a and b
	 */
	public double computeCovariance(double[] a, double[] b);
	
	/**
	 * Computes the covariance matrix of any set of points
	 * @param points
	 * @return the covariance matrix
	 */
	public double[][] computeCovarianceMatrix(double[][] points);
	
	/**
	 * Computes the cross covariance between two different sets of points. 
	 * @param x_star - first set of points (size m)
	 * @param x - second set of points (size n)
	 * @return the cross covariance between the first and second set of points (a mxn matrix)
	 */
	public double[][] computeCrossCovarianceMatrix(double[][] x_star, double[][] x);
	
	/**
	 * Computes the covariance of two dates. This corresponds to the covariance in a time series.
	 * @param a the first date
	 * @param b the second date
	 * @return the covariance between a and b
	 */
	public double computeCovariance(Date a, Date b);
	
	/**
	 * Compute the covariance matrix of a set of input dates.
	 * @param trainingPoints the training dates
	 * @return the covariance matrix
	 */
	public double[][] computeCovarianceMatrix(Date[] trainingPoints);
	
	/**
	 * Compute the covariance between a test date and all of the training dates.
	 * @param unseenPoint the new date
	 * @param trainingPoints the training dates
	 * @return a vector of covariances between the unseenPoint and the trainingPoints
	 */
	public double[][] computeCrossCovariance(Date[] unseenPoints, Date[] trainingPoints);
	
	/**
	 * Returns the inverse covariance matrix of the training input points. As this stays constant,
	 * it can be cached. The corresponding object will be filled in the {@link AbstractGaussianProcess}'s train method.
	 * @return the inverse training data covariance matrix
	 */
	public double[][] getInverseCovarianceMatrix();

	public abstract double[][][] computeCrossCovarianceMatrixDerivXPrime(double[][] x,
			double[][] x_m);

	public abstract double[][][] computeCovarianceMatrixDerivXPrime(double[][] points);

	public abstract double[] computeCovarianceDerivXPrime(double[] a,
			double[] b);

	public abstract double[][][] computeCrossCovarianceMatrixDerivX(double[][] x,
			double[][] x_m);

	public abstract double[][][] computeCovarianceMatrixDerivX(double[][] points);

	public abstract double[] computeCovarianceDerivX(double[] a, double[] b);

	/**
	 * Update the hyperparameter with conjugate gradient algorithm.
	 * @param gp - the Gaussian process for which to update the hyperparameters
	 */
	public void updateHyperparameters(AbstractGaussianProcess gp);

	/**
	 * Get the current hyperparameter setting. Always start with system noise sigma^2_f.
	 * @return
	 */
	public abstract double[] getHyperparameters();

	/**
	 * @param hyperparamUpdateFunction
	 */
	public void setHyperparamUpdateFunction(ObjectiveFunction hyperparamUpdateFunction);
	
}
