/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.sde;

import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class DoubleWellSDE extends StochasticDifferentialEquation {

	/**
	 * @param processNoise
	 */
	public DoubleWellSDE(double[][] processNoise, double _theta) {
		super(processNoise, new double[]{_theta});
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunction(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunction(RealMatrix x, int k) {
		double theta = getHyperparameters()[0];
		return x.scalarMultiply(4.).multiply(x.multiply(x).scalarMultiply(-1.).scalarAdd(theta));
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionExpectedDerivativeX(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunctionExpectedDerivativeX(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		return normalSecondMoment(expectationX, varianceX).scalarMultiply(-12.).scalarAdd(4*theta);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionExpectation(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunctionExpectation(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		RealMatrix x2 = expectationX.multiply(expectationX);
		return expectationX.scalarMultiply(4.).multiply(x2.scalarMultiply(-1).subtract(varianceX.scalarMultiply(3)).scalarAdd(theta));
	}
	
	

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionTimesXExpectation(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix driftFunctionTimesXExpectation(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		return normalSecondMoment(expectationX, varianceX).scalarMultiply(4*theta).subtract(normalFourthMoment(expectationX, varianceX).scalarMultiply(4));
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#squaredDriftFunctionExpectation(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix squaredDriftFunctionExpectation(RealMatrix expectationX,
			RealMatrix varianceX, int k) {
		double theta = getHyperparameters()[0];
		return ( normalSecondMoment(expectationX, varianceX).scalarMultiply(2*theta*theta)
					.add(normalFourthMoment(expectationX, varianceX).scalarMultiply(-theta))
					.add(normalSixthMoment(expectationX, varianceX).scalarMultiply(2))
				).scalarMultiply(8);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#driftFunctionDerivativeHyperparameters()
	 */
	@Override
	public double[] driftFunctionDerivativeHyperparameters() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#energyDerivativeVariance(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix energyDerivativeVariance(RealMatrix posteriorMean,
			RealMatrix posteriorVariance, RealMatrix variationalFunctionB,
			RealMatrix variationalFunctionA, int k) {
		double theta = getHyperparameters()[0];
		RealMatrix ThetaA = variationalFunctionA.scalarAdd(4*theta);
		RealMatrix ThetaA2 = ThetaA.multiply(ThetaA);
		return normalSixthMomentDerivVar(posteriorMean, posteriorVariance).scalarMultiply(8)
					.subtract(normalFourthMomentDerivVar(posteriorMean, posteriorVariance).multiply(ThetaA.scalarMultiply(4)))
					.add(normalThirdMomentDerivVar(posteriorMean).multiply(variationalFunctionB).scalarMultiply(4))
					.add(normalSecondMomentDerivVar(posteriorMean).multiply(ThetaA2).scalarMultiply(.5));
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.StochasticDifferentialEquation#energyDerivativeMean(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)
	 */
	@Override
	public RealMatrix energyDerivativeMean(RealMatrix posteriorMean,
			RealMatrix posteriorVariance, RealMatrix variationalFunctionB,
			RealMatrix variationalFunctionA, int k) {
		double theta = getHyperparameters()[0];
		RealMatrix ThetaA = variationalFunctionA.scalarAdd(4*theta);
		RealMatrix ThetaA2 = ThetaA.multiply(ThetaA);
		return normalSixthMomentDerivMean(posteriorMean, posteriorVariance).scalarMultiply(8)
					.subtract(normalFourthMomentDerivMean(posteriorMean, posteriorVariance).multiply(ThetaA.scalarMultiply(4)))
					.add(normalThirdMomentDerivMean(posteriorMean, posteriorVariance).multiply(variationalFunctionB).scalarMultiply(4))
					.add(normalSecondMomentDerivMean(posteriorMean).multiply(ThetaA2).scalarMultiply(.5))
					.add(ThetaA).multiply(variationalFunctionB);
	}

}
