/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.math;

import java.util.Random;

import cern.jet.random.Gamma;
import cern.jet.random.Normal;

public class ArrayFactory {
	public enum Type {ZERO, ONE, NORMAL, UNIFORM, GAMMA}
	public static Random rng = new Random();
	
	public static double[] doubleArray(Type t, int dim) {
		double[] ret = new double[dim];
		for(int i=0;i<dim;i++)
			ret[i] = generateRandomNumber(t);
		return ret;
	}

	public static double[][] doubleArray(Type t, int dim, int dim2) {
		double[][] ret = new double[dim][];
		for(int i=0;i<dim;i++)
			ret[i] = doubleArray(t, dim2);
		return ret;
	}
	
	public static double[][][] doubleArray(Type t, int dim, int dim2, int dim3) {
		double[][][] ret = new double[dim][][];
		for(int i=0;i<dim;i++)
			ret[i] = doubleArray(t, dim2, dim3);
		return ret;
		
	}

	private static double generateRandomNumber(Type t) {
		double num = 0.;
		switch(t) {
		case ZERO: 
			num = 0.;
			break;
		case ONE:
			num = 1.;
			break;
		case NORMAL:
			num = Normal.staticNextDouble(0, 1);
			break;
		case UNIFORM:
			num = rng.nextDouble();
			break;
		case GAMMA:
			num = Gamma.staticNextDouble(100., 100.);
			break;
		default:
			num = 0;
			break;
		}
		return num;
	}
}
