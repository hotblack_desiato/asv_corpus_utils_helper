/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.sparsevb;

import java.util.Arrays;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ICovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.IMeanFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;

public class SparseGaussianProcessVB extends AbstractGaussianProcess{

	private RealMatrix Knn, Kmm, Knm, Kmn, Xm, XmInitial, A, X, mu;
	private RealMatrix y;
	private int numInducing;
	private double likelihood;
	
	public SparseGaussianProcessVB(IMeanFunction meanFct, ICovarianceFunction covFct, int numInducingVariables) {
		super(meanFct, covFct);
		this.numInducing = numInducingVariables;
	}
	
	
	public int getNumberOfInducingVariables() {
		return this.numInducing;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess#train(double[][], double[])
	 */
	@Override
	public void setTrainingData(double[][] covs, double[] targets) {
		super.setTrainingData(covs, targets);
		
		this.X = MatrixUtils.createRealMatrix(this.dataCovariates);
		this.covariance.updateTrainingDataCovariance(this.dataCovariates);

		this.mu = MatrixUtils.createRealMatrix(this.numInducing, 1);
		this.A = MatrixUtils.createRealMatrix(this.numInducing, this.numInducing);
		
		this.Xm = MatrixUtils.createRealMatrix(this.numInducing, this.dataCovariates[0].length);
		
		//different sample strategies
		//random sample
//		ArrayList<Integer> idx = new ArrayList<>();
//		for(int i=0;i<this.dataCovariates.length;++i)
//			idx.add(i);
//		Collections.shuffle(idx);
//		for(int i=0;i<this.numInducing;i++) {
//			this.Xm.setRow(i, this.dataCovariates[idx.get(i)]);
//		}
		
		//random values
//		double offset = VectorUtils.min(this.dataCovariates);
//		double range = VectorUtils.max(this.dataCovariates)-offset;
//		Random r = new Random();
//		for(int i=0;i<this.numInducing;i++) {
//			this.Xm.setRow(i, new double[]{r.nextDouble()*range+offset});
//		}

		
		double[] sortedInputs = VectorUtils.viewColumn(this.dataCovariates,0);
		Arrays.sort(sortedInputs);
		//in the front
//		for(int i=0;i<this.numInducing;++i) {
//			this.Xm.setRow(i, new double[]{sortedInputs[i]});
//		}
		
		//in the middle
		for(int i=0;i<this.numInducing;++i) {
			this.Xm.setRow(i, new double[]{sortedInputs[(int) Math.round(i + this.targetData.length/2.)]});
		}
		
		this.XmInitial = this.Xm.copy();
		
		//compute initial covariance matrix
		this.Knn = MatrixUtils.createRealMatrix(this.covariance.getTrainingDataCovariance());
		
		//the target data as matrix
		this.y = MatrixUtils.createColumnRealMatrix(this.targetData);
		
//		trainGP();
	}
	
	
	@Override
	public void train() {
		double oldLikelikelihood = -1e100;
		this.likelihood = -1e100;
		double converge = 1;
		while(converge > 1e-3) {
			this.likelihood = vbIteration();
			converge = (oldLikelikelihood - this.likelihood)/oldLikelikelihood;
			oldLikelikelihood = this.likelihood;
		}

	}

	@Override
	public double computeLikelihood() {
		return this.likelihood;
	}


	private void computeRelevantCovariances() {
		this.Kmm = MatrixUtils.createRealMatrix(this.covariance.computeCovarianceMatrix(this.Xm.getData()));
		this.Knm = MatrixUtils.createRealMatrix(this.covariance.computeCrossCovarianceMatrix(this.X.getData(), this.Xm.getData()));
		this.Kmn = this.Knm.copy().transpose();
	}


	private double vbIteration() {
		double lik = updateInducingVariables();
		updateHyperparameters();
		return lik;
	}

	private void updateHyperparameters() {
		LogLikelihoodOptimizeParameters p = new LogLikelihoodOptimizeParameters(this);
		p.setKnn(this.Knn);
		p.setX(this.X);
		p.setXm(this.Xm);
		p.setY(this.y);
		p.setJitter(1e-3);
//		p.doComputations();
		
		HyperparamLLFunction f = new HyperparamLLFunction(3, p);
		this.covariance.setHyperparamUpdateFunction(f);
		this.covariance.updateHyperparameters(this);
	}


	private double updateInducingVariables() {
		//minimize the log likelihood function
		Optimizer opt = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);

		LogLikelihoodOptimizeParameters p = new LogLikelihoodOptimizeParameters(this);
		p.setKnn(this.Knn);
		p.setX(this.X);
		p.setXm(this.Xm);
		p.setY(this.y);
		p.setJitter(1e-3);
		p.doComputations();
		
		LogLikelihoodFunction f = new LogLikelihoodFunction(this.numInducing, p);
		
		Optimizer.optimize(VectorUtils.viewColumn(this.Xm.getData(), 0), .35, .01, 1e-10, 30, f, opt);
		
		double[] newXm = opt.getX();
		
		this.Xm.setColumn(0, newXm);
		return -opt.getF();
	}
	

	public void predict(double[][] unseenPoints) {
//		double[][] unseenCov = new double[unseenPoints.length][];
		
		computeRelevantCovariances();
		double n = this.y.getRowDimension();
		RealMatrix KmmInv = MatrixUtils.createRealMatrix(this.covariance.getInverseCovarianceMatrix());
		//TODO check how the noise enters this computations
		RealMatrix noiseInv = MatrixUtils.createRealIdentityMatrix(this.X.getRowDimension()).scalarMultiply(1./this.covariance.getHyperparameters()[0]);
		RealMatrix noise = MatrixUtils.createRealIdentityMatrix(this.X.getRowDimension()).scalarMultiply(this.covariance.getHyperparameters()[0]);
		RealMatrix tmp = this.Kmm.add(this.Kmn.multiply(noiseInv).multiply(this.Knm));
		RealMatrix tmpInv = new LUDecomposition(tmp, 1e-50).getSolver().getInverse();
		RealMatrix noiseQnnInv = noiseInv.subtract(noiseInv.multiply(this.Knm).multiply(tmpInv).multiply(this.Kmn).multiply(noiseInv));
		RealMatrix Qnn = this.Knm.multiply(KmmInv).multiply(this.Kmn);

		double det = new LUDecomposition(Qnn.add(noise)).getDeterminant();
		
		double ll = -.5 * n * Math.log(2*Math.PI) - .5*Math.log(det);
		RealMatrix v = noiseQnnInv.multiply(this.y);
		ll -= .5 * v.transpose().multiply(noiseQnnInv).multiply(v).getTrace();
		ll -= .5 * this.Knn.subtract(Qnn).getTrace() / this.covariance.getHyperparameters()[0];

		
		this.predictiveMean = new double[unseenPoints.length];
		this.predictiveVariance = new double[unseenPoints.length];
		
		//compute distribution over pseudo targets
		RealMatrix Sigma = this.Kmm.add(this.Kmn.multiply(this.Knm).scalarMultiply(1./this.covariance.getHyperparameters()[0]));
		//this takes the time
		LUDecomposition lud = new LUDecomposition(Sigma, 1e-50);
		RealMatrix SigmaInv = lud.getSolver().getInverse();
		this.mu = this.Kmm.multiply(SigmaInv).multiply(this.Kmn).multiply(this.y).scalarMultiply(1./this.covariance.getHyperparameters()[0]);
		this.A = this.Kmm.multiply(SigmaInv).multiply(this.Kmm);


		RealMatrix Kxm = MatrixUtils.createRealMatrix(this.covariance.computeCrossCovarianceMatrix(unseenPoints, this.Xm.getData()));
		RealMatrix Kxx = MatrixUtils.createRealMatrix(this.covariance.computeCovarianceMatrix(unseenPoints));
//		predictiveMean = Kxm.multiply(KmmInv).multiply(mu).scalarAdd(mu0);
//		predictiveVariance = getDiagonalMatrix(Kxx)
//				.subtract(getDiagonalMatrix(Kxm.multiply(KmmInv).multiply(Kxm.transpose())))
//				.add(getDiagonalMatrix(Kxm.multiply(KmmInv).multiply(A).multiply(KmmInv).multiply(Kxm.transpose())))
//				.scalarAdd(mu0);
		for(int i=0;i<unseenPoints.length;++i) {
			//predictive mean
			RealMatrix tmp1 = Kxm.getRowMatrix(i).multiply(KmmInv).multiply(this.mu);
			this.predictiveMean[i] = tmp1.getTrace();
			
			//predictive variance
			double kxx = Kxx.getEntry(i, i);
			double variance = kxx - 
					Kxm.getRowMatrix(i).multiply(KmmInv).multiply(Kxm.getRowMatrix(i).transpose())
					.add(
							Kxm.getRowMatrix(i).multiply(KmmInv).multiply(this.A).multiply(KmmInv).multiply(Kxm.getRowMatrix(i).transpose())
					).getTrace();
			System.out.println("variance: " + variance);
			if(variance < 0)
				variance *= variance;
			this.predictiveVariance[i] = variance;
		}
		
//		return ll;
	}

	public double[] getInitialInducingValues() {
		return this.XmInitial.getColumn(0);
	}
	
	public double[] getFinalInducingValues() {
		return this.Xm.getColumn(0);
	}
	
	
	private RealMatrix getDiagonalMatrix(RealMatrix input) {
		double[] vals = new double[input.getRowDimension()];
		for(int i=0;i<input.getRowDimension();++i)
			vals[i] = input.getEntry(i, i);
		return MatrixUtils.createRealDiagonalMatrix(vals);
	}


	/**
	 * @return the knn
	 */
	public RealMatrix getKnn() {
		return Knn;
	}


	/**
	 * @return the xm
	 */
	public RealMatrix getXm() {
		return Xm;
	}


	/**
	 * @return the x
	 */
	public RealMatrix getX() {
		return X;
	}


	/**
	 * @return the y
	 */
	public RealMatrix getY() {
		return y;
	}
	


}
