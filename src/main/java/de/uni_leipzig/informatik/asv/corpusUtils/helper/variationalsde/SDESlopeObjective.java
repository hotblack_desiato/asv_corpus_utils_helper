/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde;

import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class SDESlopeObjective extends ObjectiveFunction {

	/**
	 * @param dimension
	 * @param parameters
	 */
	protected SDESlopeObjective(int dimension, SDEOptimizeParameters parameters) {
		super(dimension, parameters);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#f(double[])
	 */
	@Override
	public double f(double[] point) {
		SDEOptimizeParameters p = getParameters();
		int k = p.getTime();
		p.getInf().setVariationalFunctionA(point, k);
		return p.getInf().computeLagrangian(k);
	}

	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#df(double[], double[])
	 */
	@Override
	public void df(double[] point, double[] df) {
		SDEOptimizeParameters p = getParameters();
		int k = p.getTime();
		p.getInf().setVariationalFunctionA(point, k);
		RealMatrix deriv = p.getInf().computeLagrangianDerivativeA(k, p.getSdeDerivB());
		System.arraycopy(SDEVBInferencer.create1DArray(deriv), 0, df, 0, df.length);
	}

	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#fdf(double[], double[])
	 */
	@Override
	public double fdf(double[] point, double[] df) {
		SDEOptimizeParameters p = getParameters();
		int k = p.getTime();
		p.getInf().setVariationalFunctionA(point, k);
		RealMatrix deriv = p.getInf().computeLagrangianDerivativeA(k, p.getSdeDerivB());
		System.arraycopy(SDEVBInferencer.create1DArray(deriv), 0, df, 0, df.length);
		return p.getInf().computeLagrangian(k);
	}

}
