/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg;

import java.util.Arrays;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;

public class FletcherReevesConjugateGradientOptimizer extends Optimizer{

	public boolean debug = false;

	private OptimState state;

	public FletcherReevesConjugateGradientOptimizer() {
		this.state = new OptimState();
	}

	public void allocate(int n) {
		super.allocate(n);
		this.state.allocate(n);
	}

	public void set(ObjectiveFunction _func, double[] _x, double stepSize,
			double tolerance) {
		if (this.x.length != _func.getDimensions())
			throw new IllegalArgumentException("incompatible solver size");
		if (_x.length != _func.getDimensions())
			throw new IllegalArgumentException(
					"vector length not compatible with function");
		this.func = _func;
		System.arraycopy(_x, 0, this.x, 0, _x.length);
		Arrays.fill(this.dx, 0);

		this.state.setIter(0);
		this.state.setStep(stepSize);
		this.state.setMaxStep(stepSize);
		this.state.setTol(tolerance);

		this.f = this.func.fdf(_x, this.gradient);
		this.state.setP(this.gradient.clone());
		this.state.setG0(this.gradient.clone());

		double gnorm = VectorUtils.norm(this.gradient);
		this.state.setpNorm(gnorm);
		this.state.setG0Norm(gnorm);
	}

	public Result iterate() {
		double[] x1 = this.state.getX1();
		double[] dx1 = this.state.getDx1();
		double[] x2 = this.state.getX2();
		double[] p = this.state.getP();
		double[] g0 = this.state.getG0();

		double pNorm = this.state.getpNorm();
		double g0Norm = this.state.getG0Norm();

		double fa = this.f, fb, fc;
		double dir;
		double stepa = 0.0, stepb, stepc = this.state.getStep(), tol = this.state
				.getTol();

		double g1norm;
		double pg;

		if (pNorm == 0.0 || g0Norm == 0.0) {
			Arrays.fill(this.dx, 0);
			return Result.ENOPROG;
		}

		/* Determine which direction is downhill, +p or -p */

		pg = VectorUtils.dotProduct(p, this.gradient);

		//TODO: figure out which is correct
		dir = (pg >= 0.0) ? -1.0 : +1.0;
		dir = (pg >= 0.0) ? +1.0 : -1.0;

		/*
		 * Compute new trial point at x_c= x - step * p, where p is the current
		 * direction
		 */

		takeStep(this.x, p, stepc, dir / pNorm, x1, this.dx);

		/* Evaluate function and gradient at new point xc */

		fc = this.func.f(x1);

		if (fc < fa) {
			/* Success, reduced the function value */
			this.state.setStep(stepc * 2.0);
			this.f = fc;
			System.arraycopy(x1, 0, this.x, 0, x1.length);

			this.func.df(x1, this.gradient);
			return Result.SUCCESS;
		}

		if (this.debug) {
			System.out.format("got stepc = %g fc = %g\n", stepc, fc);
		}

		/*
		 * Do a line minimisation in the region (xa,fa) (xc,fc) to find an
		 * intermediate (xb,fb) satisifying fa > fb < fc. Choose an initial xb
		 * based on parabolic interpolation
		 */

		double[] res = intermediatePoint(this.x, p, dir / pNorm, pg, stepa, stepc,
				fa, fc, x1, dx1, this.gradient);
		stepb = res[0];
		fb = res[1];

		if (stepb == 0.0) {
			return Result.ENOPROG;
		}

		g1norm = minimize(this.x, p, dir / pNorm, stepa, stepb, stepc, fa, fb, fc,
				tol, x1, dx1, x2, this.dx, this.gradient, this.state.getStep(), this.f);

		System.arraycopy(x2, 0, this.x, 0, x2.length);

		/* Choose a new conjugate direction for the next step */

		this.state.setIter(this.state.getIter() + 1 % this.x.length);

		if (this.state.getIter() == 0) {
			System.arraycopy(this.gradient, 0, p, 0, this.gradient.length);
			this.state.setpNorm(g1norm);
		} else {
			/* p' = g1 - beta * p */

			double beta = -Math.pow(g1norm / g0Norm, 2.0);
			VectorUtils.multiplyWithScalar_(p, -beta);
			VectorUtils.add_(this.gradient, p);
			this.state.setpNorm(VectorUtils.norm(p));
		}

		this.state.setG0Norm(g1norm);
		System.arraycopy(this.gradient, 0, g0, 0, this.gradient.length);

		if (this.debug) {
			System.out.println("updated conjugate directions\n");
			 System.out.println("p: " + Arrays.toString(p));
			 System.out.println("g: " + Arrays.toString(this.gradient));
		}
		return Result.SUCCESS;
	}

	private double minimize(final double[] x, final double[] p, double lambda,
			double stepa, double stepb, double stepc, double fa, double fb,
			double fc, double tol, double[] x1, double[] dx1, double[] x2,
			double[] dx2, double[] gradient, double step, double f) {
		/*
		 * Starting at (x0, f0) move along the direction p to find a minimum
		 * f(x0 - lambda * p), returning the new point x1 = x0-lambda*p,
		 * f1=f(x1) and g1 = grad(f) at x1.
		 */

		double u = stepb;
		double v = stepa;
		double w = stepc;
		double fu = fb;
		double fv = fa;
		double fw = fc;

		double old2 = Math.abs(w - v);
		double old1 = Math.abs(v - u);

		double stepm, fm, pg, gnorm1;

		double gnorm;
		int iter = 0;

		System.arraycopy(x1, 0, x2, 0, x1.length);
		System.arraycopy(dx1, 0, dx2, 0, dx1.length);

		this.f = fb;
		this.state.setStep(stepb);
		gnorm = VectorUtils.norm(gradient);

		//mid_trial:
		while (true) {
			iter++;
			if (iter > 10) {
				return gnorm; /* MAX ITERATIONS */
			}

			{
				double dw = w - u;
				double dv = v - u;
				double du = 0.0;

				double e1 = ((fv - fu) * dw * dw + (fu - fw) * dv * dv);
				double e2 = 2.0 * ((fv - fu) * dw + (fu - fw) * dv);

				if (e2 != 0.0) {
					du = e1 / e2;
				}
				if (du > 0.0 && du < (stepc - stepb)
						&& Math.abs(du) < 0.5 * old2) {
					stepm = u + du;
				} else if (du < 0.0 && du > (stepa - stepb)
						&& Math.abs(du) < 0.5 * old2) {
					stepm = u + du;
				} else if ((stepc - stepb) > (stepb - stepa)) {
					stepm = 0.38 * (stepc - stepb) + stepb;
				} else {
					stepm = stepb - 0.38 * (stepb - stepa);
				}
			}

			takeStep(x, p, stepm, lambda, x1, dx1);

			fm = this.func.f(x1);
			if (this.debug)
				System.out.format("trying stepm = %g  fm = %.18e\n", stepm, fm);

			if (fm > fb) {
				if (fm < fv) {
					w = v;
					v = stepm;
					fw = fv;
					fv = fm;
				} else if (fm < fw) {
					w = stepm;
					fw = fm;
				}

				if (stepm < stepb) {
					stepa = stepm;
					fa = fm;
				} else {
					stepc = stepm;
					fc = fm;
				}
				//goto mid_trial;
			} else if (fm <= fb) {
				old2 = old1;
				old1 = Math.abs(u - stepm);
				w = v;
				v = u;
				u = stepm;
				fw = fv;
				fv = fu;
				fu = fm;

				System.arraycopy(x1, 0, x2, 0, x1.length);
				System.arraycopy(dx1, 0, dx2, 0, dx1.length);

				this.func.df(x1, gradient);
				pg = VectorUtils.dotProduct(p, gradient);
				// gsl_blas_ddot (p, gradient, &pg);
				gnorm1 = VectorUtils.norm(gradient);
				// gnorm1 = gsl_blas_dnrm2 (gradient);

				if (this.debug) {
					System.out.println("p: " + Arrays.toString(p));
					System.out.println("g: " + Arrays.toString(gradient));
					System.out.format("gnorm: %.18e\n", gnorm1);
					System.out.format("pg: %.18e\n", pg);
					System.out.format("orth: %g\n",
							Math.abs(pg * lambda / gnorm1));
				}
				this.f = fm;
				this.state.setStep(stepm);
				gnorm = gnorm1;

				if (Math.abs(pg * lambda / gnorm1) < tol) {
					if (this.debug)
						System.out.println("ok!");
					return gnorm; /* SUCCESS */
				}

				if (stepm < stepb) {
					stepc = stepb;
					fc = fb;
					stepb = stepm;
					fb = fm;
				} else {
					stepa = stepb;
					fa = fb;
					stepb = stepm;
					fb = fm;
				}
			}

		}
	}

	private double[] intermediatePoint(final double[] x, final double[] p, double lambda,
			double pg, double stepa, double stepc, double fa, double fc,
			double[] x1, double[] dx, double[] gradient) {
		
		double stepb = 1, fb = 1;

//		while (fb >= fa && stepb > 0.0) {
		while(true && stepb > 1e-50) {
			double u = Math.abs(pg * lambda * stepc);
			stepb = 0.5 * stepc * u / ((fc - fa) + u);
			takeStep(x, p, stepb, lambda, x1, dx);
			fb = this.func.f(x1);
			if (this.debug)
				System.out.format("trying stepb = %g  fb = %.18e\n", stepb, fb);
			if (fb >= fa && stepb > 0.0) {
				/* downhill step failed, reduce step-size and try again */
				fc = fb;
				stepc = stepb;
			} else
				break;
		}
		if (this.debug)
			System.out.print("ok!\n");

		this.func.df(x1, gradient);
		return new double[] { stepb, fb };
	}

	private void takeStep(final double[] x, final double[] p, double step, double lambda,
			double[] x1, double[] dx) {
		Arrays.fill(dx, 0);

		VectorUtils.add_(VectorUtils.multiplyWithScalar(p, -step * lambda), dx);

		System.arraycopy(x, 0, x1, 0, x.length);

		VectorUtils.add_(dx, x1);

		// gsl_vector_memcpy (x1, x);
		// gsl_blas_daxpy (1.0, dx, x1);

	}


}
