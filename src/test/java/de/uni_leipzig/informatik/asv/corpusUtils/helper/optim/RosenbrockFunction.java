/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.optim;

import java.util.Arrays;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class RosenbrockFunction extends ObjectiveFunction {

	public RosenbrockFunction(int dim) {
		super(dim, null);
	}
	
	@Override
	public double f(double[] point) {
		return Math.pow(1-point[0], 2) + 100*Math.pow(point[1] - Math.pow(point[0], 2), 2);
	}

	@Override
	public void df(double[] point, double[] df) {
		Arrays.fill(df, 0.);
		df[0] = -2. * (1. - point[0]) - 400. * (point[1] - Math.pow(point[0], 2)) * point[0];
		df[1] = 200. * (point[1] - Math.pow(point[0], 2));
	}

	@Override
	public double fdf(double[] point, double[] df) {
		Arrays.fill(df, 0.);
		df[0] = -2. * (1. - point[0]) - 400. * (point[1] - Math.pow(point[0], 2)) * point[0];
		df[1] = 200. * (point[1] - Math.pow(point[0], 2));

		return Math.pow(1-point[0], 2) + 100*Math.pow(point[1] - Math.pow(point[0], 2), 2);
	}
}
