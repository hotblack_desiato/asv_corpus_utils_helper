/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ZeroMeanFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full.GaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full.GaussianProcessTest;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
@RunWith(JUnit4.class)
public class RBFObjectiveFunctionTest {
	private double h = 1e-7;
	private GaussianProcess gp;
	private GaussianKernel k;
	private static double[][] trainingInputs;
	private static double[] trainingTargets;
	private double noise = .1;
	private double s2 = .4;
	private double l = .8;
	
	@BeforeClass
	public static void setUpClass() {
		trainingInputs = GaussianProcessTest.generateTrainingData();
		trainingTargets = GaussianProcessTest.generateTargetData(trainingInputs);
	}
	
	@Before
	public void setUp() {
		this.k = new GaussianKernel(this.s2, this.l, this.noise);
		this.gp = new GaussianProcess(new ZeroMeanFunction(), this.k);
		this.gp.setTrainingData(RBFObjectiveFunctionTest.trainingInputs, RBFObjectiveFunctionTest.trainingTargets);

	}
	
	@Test
	public void testF() {
		// get the hyperparemeters
		double[] point = this.gp.getCovFunction().getHyperparameters();
		//create the objective function with a new gp
		RBFObjectiveFunction f = new RBFObjectiveFunction(point.length, this.gp);
		
		//compute the function value at the current point
		double valX = f.f(point);
		double negLL = -this.gp.computeLikelihood();
		assertEquals(null, negLL, valX, 1e-9);
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.RBFObjectiveFunction#df(double[], double[])}.
	 */
	@Test
	public void testDf() throws Exception {
		//init some data
		double[] point = this.gp.getCovFunction().getHyperparameters();
		double[] grad = new double[3]; 
		//create the objective function with a new gp
		RBFObjectiveFunction f = new RBFObjectiveFunction(point.length, this.gp);
		
		//compute the gradients at the current point
		f.df(point, grad);
		
		//compute the function value at the current point
		double valX = f.f(point);
		
		
		// noise variation
		point[0] += this.h;
		double valXNoisePlusH = f.f(point);
		point[0] = this.noise;
		
		// variance variation
		point[1] += this.h;
		double valXVarPlusH = f.f(point);
		point[1] = this.s2;
		
		//length scale variation
		point[2] += this.h;
		double valXLPlusH = f.f(point);
		point[2] = this.l;
		
		//differentials
		double diffNoise = (valXNoisePlusH - valX)/this.h;
		double diffVar = (valXVarPlusH - valX)/this.h;
		double diffL = (valXLPlusH - valX)/this.h;
		
		assertEquals(null, grad[0], diffNoise, Math.abs(grad[0]/1000));
		assertEquals(null, grad[1], diffVar, valX/1000);
		assertEquals(null, grad[2], diffL, valX/1000);
	}
	


}
