/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.rprop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.RosenbrockFunction;


/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
@RunWith(JUnit4.class)
public class RPropOptimizerTest {
	@Test
	public void testOptimze() {
		
		for(double tolerance=.1; tolerance>=1e-6; tolerance/=10) {
			System.out.println("tolerance at " + tolerance);
			double[] res = doOptimize(tolerance);
			Assert.assertArrayEquals(new double[]{1.0, 1.0}, res, 1e-3);
		}
	}
	
	public double[] doOptimize(double tolerance) {
		RPropOptimizer opt = Optimizer.getOptimizer(RPropOptimizer.class);
		RosenbrockFunction f = new RosenbrockFunction(2);
		Optimizer.optimize(new double[2], 2, tolerance, 1e-6, 100000, f, opt);
		return opt.getX();
	}

}
