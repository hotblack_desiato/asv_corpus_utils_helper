/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.sde.DoubleWellSDETest;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick
 *         Jaehnichen</a>
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({ 
	SDEAbsolutePartObjectiveTest.class,
	SDESlopeObjectiveTest.class, 
	SDEVBInferencerTest.class,
	DoubleWellSDETest.class
})
public class AsvCorpusUtilsHelperSDETestSuite {

}
