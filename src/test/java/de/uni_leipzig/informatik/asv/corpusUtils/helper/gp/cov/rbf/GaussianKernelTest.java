/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.apache.commons.math3.linear.MatrixUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
@RunWith(JUnit4.class)
public class GaussianKernelTest {
	private double h = 1e-6;
	private int dim = 2;
	private Random rng = new Random(System.currentTimeMillis());
	
	@Test
	public void testComputeCovarinceDerivSigma() {
		double[] a = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		double[] b = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		
		double noise = this.rng.nextDouble() + 1e-7;
		double s2 = this.rng.nextDouble() + 1e-7;
		double l = this.rng.nextDouble() * 2;
		
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		
		double gradS2 = k.computeCovarinceDerivSigma(a, b);
		
		double[] hypers = k.getHyperparameters();
		
		double cov = k.computeCovariance(a, b);
		hypers[1] += this.h;
		k.setHyperparameters(hypers);
		double covS2PlusH = k.computeCovariance(a, b);
		hypers[1] -= this.h;
		
		double diffS2 = (covS2PlusH - cov)/this.h;
		System.out.println("testing variance gradient, should be " + gradS2 + ", was " + diffS2);
		assertEquals(null, gradS2, diffS2, 1e-3);
	}
	
	@Test
	public void testComputeCovarinceDerivLengthScale() {
		double[] a = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		double[] b = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		
		double noise = this.rng.nextDouble() + 1e-7;
		double s2 = this.rng.nextDouble() + 1e-7;
		double l = this.rng.nextDouble() * 2;
		
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		
		double gradL = k.computeCovarinceDerivLengthScale(a, b);
		
		double[] hypers = k.getHyperparameters();
		
		double cov = k.computeCovariance(a, b);
		hypers[2] += this.h;
		k.setHyperparameters(hypers);
		double covLPlusH = k.computeCovariance(a, b);
		hypers[2] -= this.h;
		
		double diffL = (covLPlusH - cov)/this.h;
		System.out.println("testing length scale gradient, should be " + gradL + ", was " + diffL);
		assertEquals(null, gradL, diffL, 1e-3);
	}

	@Test
	public void testComputeCovarianceDerivX() {
		double[] a = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		double[] b = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		
		double noise = this.rng.nextDouble() + 1e-7;
		double s2 = this.rng.nextDouble() + 1e-7;
		double l = this.rng.nextDouble() * 2;
		
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		
		double[] gradX = k.computeCovarianceDerivX(a, b);
		double[] diffs = new double[5];
		double cov = k.computeCovariance(a, b);
		
		double covIPlusH;
		for(int i=0;i<this.dim;i++) {
			a[i] += this.h;
			covIPlusH = k.computeCovariance(a, b);
			a[i] -= this.h;
			diffs[i] = (covIPlusH - cov)/this.h;
		}
		
		for(int i=0;i<this.dim;i++) {
			System.out.println("testing x[" + i +"] gradient, should be " + gradX[i] + ", was " + diffs[i]);
			assertEquals(null, gradX[i], diffs[i], 1e-3);
		}
	}

	@Test
	public void testComputeCovarianceDerivXPrime() {
		double[] a = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		double[] b = ArrayFactory.doubleArray(Type.UNIFORM, this.dim);
		
		double noise = this.rng.nextDouble() + 1e-7;
		double s2 = this.rng.nextDouble() + 1e-7;
		double l = this.rng.nextDouble() * 2;
		
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		
		double[] gradXPrime = k.computeCovarianceDerivXPrime(a, b);
		
		double cov = k.computeCovariance(a, b);
		double[] diffs = new double[5];
		double covIPlusH;
		for(int i=0;i<this.dim;i++) {
			b[i] += this.h;
			covIPlusH = k.computeCovariance(a, b);
			diffs[i] = (covIPlusH - cov)/this.h;
			b[i] -= this.h;
		}
		
		for(int i=0;i<this.dim;i++) {
			System.out.println("testing xPrime[" + i +"] gradient, should be " + gradXPrime[i] + ", was " + diffs[i]);
			assertEquals(null, gradXPrime[i], diffs[i], 1e-3);
		}
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel#computeCovarianceMatrixDerivSigma(double[][])}.
	 */
	@Test
	public void testComputeCovarianceMatrixDerivSigma() throws Exception {
		int numSamples = 20;
		double[][] trainingPoints = new double[numSamples][];
		for(int i=0;i<numSamples;i++) {
			trainingPoints[i] = ArrayFactory.doubleArray(Type.NORMAL, this.dim);
		}
		double noise = this.rng.nextDouble() + 1e-7;
		double s2 = this.rng.nextDouble() + 1e-7;
		double l = this.rng.nextDouble() * 2;
		
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		
		double[][] gradMatrix = k.computeCovarianceMatrixDerivSigma(trainingPoints);
		
		double[][] cov = k.computeCovarianceMatrix(trainingPoints);
		
		double[] hypers = k.getHyperparameters();
		
		hypers[1] += this.h;
		k.setHyperparameters(hypers);
		double[][] covSigmaPlusH = k.computeCovarianceMatrix(trainingPoints);
		
		double[][] diffs = VectorUtils.subtract(covSigmaPlusH, cov);
		diffs = VectorUtils.divideByScalar(diffs, this.h);
		
		for(int i=0;i<diffs.length;i++) {
			for(int j=0;j<diffs[i].length;j++) {
				assertEquals(null, gradMatrix[i][j], diffs[i][j], 1e-3);
			}
		}
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel#computeCovarianceMatrixDerivLengthScale(double[][])}.
	 */
	@Test
	public void testComputeCovarianceMatrixDerivLengthScale() throws Exception {
		int numSamples = 20;
		double[][] trainingPoints = new double[numSamples][];
		for(int i=0;i<numSamples;i++) {
			trainingPoints[i] = ArrayFactory.doubleArray(Type.NORMAL, this.dim);
		}
		double noise = this.rng.nextDouble() + 1e-7;
		double s2 = this.rng.nextDouble() + 1e-7;
		double l = this.rng.nextDouble() * 2;
		
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		
		double[][] gradMatrix = k.computeCovarianceMatrixDerivLengthScale(trainingPoints);
		
		double[][] cov = k.computeCovarianceMatrix(trainingPoints);
		
		double[] hypers = k.getHyperparameters();
		
		hypers[2] += this.h;
		k.setHyperparameters(hypers);
		double[][] covLPlusH = k.computeCovarianceMatrix(trainingPoints);
		
		double[][] diffs = VectorUtils.subtract(covLPlusH, cov);
		diffs = VectorUtils.divideByScalar(diffs, this.h);
		
		for(int i=0;i<diffs.length;i++) {
			for(int j=0;j<diffs[i].length;j++) {
				assertEquals(null, gradMatrix[i][j], diffs[i][j], 1e-3);
			}
		}
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel#computeCovarianceMatrix(double[][])}.
	 */
	@Test
	public void testComputeCovarianceMatrix() throws Exception {
		int numSamples = 20;
		double[][] trainingPoints = new double[numSamples][];
		for(int i=0;i<numSamples;i++) {
			trainingPoints[i] = ArrayFactory.doubleArray(Type.NORMAL, this.dim);
		}
		double noise = this.rng.nextDouble() + 1e-7;
		double s2 = this.rng.nextDouble() + 1e-7;
		double l = this.rng.nextDouble() * 2;
		
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		
		double[][] gradMatrix = MatrixUtils.createRealIdentityMatrix(numSamples).getData();
		
		double[][] cov = k.computeCovarianceMatrix(trainingPoints);
		
		double[] hypers = k.getHyperparameters();
		
		hypers[0] += this.h;
		k.setHyperparameters(hypers);
		double[][] covNoisePlusH = k.computeCovarianceMatrix(trainingPoints);
		
		double[][] diffs = VectorUtils.subtract(covNoisePlusH, cov);
		diffs = VectorUtils.divideByScalar(diffs, this.h);
		
		for(int i=0;i<diffs.length;i++) {
			for(int j=0;j<diffs[i].length;j++) {
				assertEquals(null, gradMatrix[i][j], diffs[i][j], 1e-3);
			}
		}
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel#setHyperparameters(double[])}.
	 */
	@Test
	public void testSetHyperparameters() throws Exception {
		GaussianKernel k = new GaussianKernel(.2, .3, .4);
		double newNoise = .1;
		double newVar = .9;
		double newL = 1.0;
		
		double[] newHypers = new double[]{newNoise, newVar, newL};
		k.setHyperparameters(newHypers);
		newHypers = k.getHyperparameters();
		
		assertEquals(newNoise, newHypers[0], 0);
		assertEquals(newVar, newHypers[1], 0);
		assertEquals(newL, newHypers[2], 0);		
	}
}
