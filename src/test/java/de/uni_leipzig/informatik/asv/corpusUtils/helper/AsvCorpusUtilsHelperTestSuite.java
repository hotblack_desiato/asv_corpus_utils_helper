/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper;

import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AsvCorpusUtilsHelperGPTestSuite;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.AsvCorpusUtilsHelperMathTestSuite;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.AsvCorpusUtilsHelperOptimTestSuite;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.AsvCorpusUtilsHelperSDETestSuite;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick
 *         Jaehnichen</a>
 * 
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
		// gp package
		AsvCorpusUtilsHelperGPTestSuite.class,
		//math package
		AsvCorpusUtilsHelperMathTestSuite.class,
		//optim package
		AsvCorpusUtilsHelperOptimTestSuite.class,
		//sde package
		AsvCorpusUtilsHelperSDETestSuite.class
})
public class AsvCorpusUtilsHelperTestSuite extends TestSuite {
}
