/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.math;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.apache.commons.math3.special.Gamma;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;

@RunWith(JUnit4.class)
public class VectorUtilsTest {
	
	private static MathUtils mUtils;
	
	private double[] oneDTest1 = new double[]{1,2,3};
	private double[] oneDTest2 = new double[]{4,5,6};
	
	private double[] oneDOutAddScal = new double[]{2.5,3.5,4.5};
	private double[] oneDOutSubScal = new double[]{-.5,0.5,1.5};
	private double[] oneDOutMultScal = new double[]{1.5,3,4.5};
	private double[] oneDOutDivScal = new double[]{2./3,1.33333333333,2};
	
	private double[] oneDOutAdd = new double[]{5,7,9};
	private double[] oneDOutSub = new double[]{-3,-3,-3};
	private double[] oneDOutMult = new double[]{4,10,18};
	private double[] oneDOutDiv = new double[]{.25,.4,.5};
	
	
	
	private double[][] twoDTest1 = new double[][]{{1,2,3},{4,5,6},{7,8,9}};
	private double[][] twoDTest2 = new double[][]{{9,8,7},{6,5,4},{3,2,1}};
	
	private double[][] twoDOutAddScal = new double[][]{{2.5,3.5,4.5},{5.5,6.5,7.5},{8.5,9.5,10.5}};
	private double[][] twoDOutSubScal = new double[][]{{-.5,0.5,1.5},{2.5,3.5,4.5},{5.5,6.5,7.5}};
	private double[][] twoDOutMultScal = new double[][]{{1.5,3,4.5},{6,7.5,9},{10.5,12,13.5}};
	private double[][] twoDOutDivScal = new double[][]{{0.666666666,1.33333333333,2},{2.66666666666,3.333333333333,4},{4.66666666666666,5.333333333333333,6}};

	private double[][] twoDOutAdd = new double[][]{{10,10,10},{10,10,10},{10,10,10}};
	private double[][] twoDOutSub = new double[][]{{-8,-6,-4},{-2,0,2},{4,6,8}};
	private double[][] twoDOutMult = new double[][]{{9,16,21},{24,25,24},{21,16,9}};
	private double[][] twoDOutDiv = new double[][]{{1./9,.25,3./7},{4./6,1,1.5},{7./3,4,9}};
	
	@BeforeClass
	public static void setUpClass() {
		mUtils = new MathUtils();
	}

	@Test
	public void testAbs() {
		double[] input = new double[]{-1,-2,3,4,-5,6,7,-8,-9};
		double[] ouput = new double[]{1,2,3,4,5,6,7,8,9};
		
		double[] res = VectorUtils.abs(input);
		assertArrayEquals(ouput, res, 0);
	}
	
	@Test
	public void testAdd1D1D() {
		double[] input1 = new double[]{1,2,3};
		double[] input2 = new double[]{4,5,6};
		double[] output = new double[]{5,7,9};
		
		double[] res = VectorUtils.add(input1, input2);
		assertArrayEquals(output, res, 0);
	}
	
	@Test
	public void testAdd2D2D() {
		double[][] res = VectorUtils.add(this.twoDTest1, this.twoDTest2);
		for(int i=0;i<this.twoDTest1.length;++i) {
			assertArrayEquals(this.twoDOutAdd[i], res[i], 0);
		}
	}
	
	@Test
	public void testAdd1D1DModifying() {
		double[] test = new double[3];
		VectorUtils.add_(this.oneDTest1, test);
		assertArrayEquals(this.oneDTest1, test, 0);
	}
	
	@Test
	public void testAddElement1D() {
		double[] test = VectorUtils.addElement(this.oneDTest1);
		assertEquals(4, test.length);
	}
	
	@Test
	public void testAddElement1I() {
		int[] start = new int[]{1,2,3};
		int[] end = VectorUtils.addElement(start);
		assertEquals(4, end.length);
	}
	
	//spare log space function by now
	
	@Test
	public void testAddRow() {
		int[][] in = new int[][]{{1,2},{3,4}};
		int[][] res = VectorUtils.addRow(in);
		assertEquals(in.length+1, res.length);
	}
	
	@Test
	public void testAddScalar1D() {
		double[] res = VectorUtils.addScalar(this.oneDTest1, 1.5);
		double[] expected = new double[]{2.5,3.5,4.5};
		assertArrayEquals(expected, res, 0);
	}
	
	@Test
	public void testAddScalar2D() {
		double[][] res = VectorUtils.addScalar(this.twoDTest1, 1.5);
		
		for(int row=0;row<this.twoDTest1.length;++row) {
			assertArrayEquals(res[row], VectorUtils.addScalar(this.twoDTest1[row], 1.5), 0);
			assertArrayEquals(this.twoDOutAddScal[row], res[row], 0);
		}
	}
	
	@Test
	public void testAddSelection() {
		double[] input = new double[]{1,2,3};
		VectorUtils.addSelection(input, new double[]{1,-1}, new int[]{0,2});
		assertArrayEquals(input, new double[]{2,2,2}, 0);
	}
	
	@Test
	public void testAddVectorInt() {
		int[] in = new int[]{1,2,3};
		int[] res = VectorUtils.addVector(in, new int[]{3,2,1});
		assertArrayEquals(new int[]{4,4,4}, res);
	}
	
	@Test
	public void testDeleteRow() {
		double[][] input = new double[5][5];
		input[0][0] = 2;
		input[1][0] = -5;
		input[2][0] = 3;
		double[][] res = VectorUtils.deleteRow(input, 1);
		assertEquals(input.length - 1, res.length);
		assertEquals(2, res[0][0], 0);
		assertEquals(3, res[1][0], 0);
	}
	
	@Test
	public void testDigamma() {
		double[] res = VectorUtils.digamma(this.oneDTest1);
		for(int i=0;i<this.oneDTest1.length;++i) {
			assertEquals(MathUtils.digamma(this.oneDTest1[i]), res[i], 1e-3);
			assertEquals(Gamma.digamma(this.oneDTest1[i]), res[i], 1e-3);
		}
	}
	
	@Test
	public void testDivideByScalar1D() {
		double[] res = VectorUtils.divideByScalar(this.oneDTest1, 1.5);
		assertArrayEquals(this.oneDOutDivScal, res, 1e-5);
	}
	
	@Test
	public void testDivideByScalar2D() {
		double[][] res = VectorUtils.divideByScalar(this.twoDTest1, 1.5);
		for(int i=0;i<this.twoDTest1.length;++i)
			assertArrayEquals(this.twoDOutDivScal[i], res[i], 1e-5);
	}
	
	@Test
	public void testDivideByVectorD() {
		double[] res = VectorUtils.divideByVector(this.oneDTest1, this.oneDTest2);
		assertArrayEquals(this.oneDOutDiv, res, 1e-5);
	}
	
	@Test
	public void testDivideByVectorI() {
		int[] input1 = new int[]{8,10,14};
		double[] input2 = new double[]{.5,.1,2};
		
		double[] out = new double[]{16, 100, 7};
		double[] res = VectorUtils.divideByVector(input1, input2);
		assertArrayEquals(out, res, 0);
	}
	
	@Test
	public void testDivideScalar1D() {
		double[] res = VectorUtils.divideScalar(5., this.oneDTest1);
		double[] out = new double[]{5, 2.5, 5./3.};
		
		assertArrayEquals(out, res, 1e-5);
	}
	
	@Test
	public void testDotProduct1D() {
		double res = VectorUtils.dotProduct(this.oneDTest1, this.oneDTest2);
		assertEquals(32, res, 0);
	}
	
	@Test
	public void testDotProduct1D2D() {
		double[] res = VectorUtils.dotProduct(this.oneDTest1, this.twoDTest1);
		double[] out = new double[]{30,36,42};
		assertArrayEquals(out, res, 0);
	}
	
	@Test
	public void testDotProduct2D1D() {
		double[] res = VectorUtils.dotProduct(this.twoDTest1, this.oneDTest1);
		double[] out = new double[]{14,32,50};
		assertArrayEquals(out, res, 0);
	}
	
	@Test
	public void testExp1D() {
		double[] res = VectorUtils.exp(this.oneDTest1);
		for(int i=0;i<this.oneDTest1.length;++i)
			assertEquals(Math.exp(this.oneDTest1[i]), res[i], 0);
	}
	
	@Test
	public void testExp2D() {
		double[][] res = VectorUtils.exp(this.twoDTest1);
		for(int i=0;i<this.twoDTest1.length;++i) {
			for (int j = 0; j < this.twoDTest1[i].length; ++j) {
				assertEquals(Math.exp(this.twoDTest1[i][j]), res[i][j], 0);
			}
		}
	}
	
	@Test
	public void testFindinD() {
		double[] test = new double[]{1,2,3,1,2,3,1,2,3,1};
		Integer[] expected = new Integer[]{0,3,6,9};
		Integer[] idx = VectorUtils.findin(test, 1.0);
		assertArrayEquals(expected, idx);
	}
	
	@Test
	public void testFindinI() {
		int[] test = new int[]{1,2,3,1,2,3,1,2,3,1};
		Integer[] expected = new Integer[]{0,3,6,9};
		Integer[] idx = VectorUtils.findin(test, 1);
		assertArrayEquals(expected, idx);
	}
	
	@Test
	public void testInvert() {
		double[] res = VectorUtils.invert(this.oneDTest1);
		for(int i=0;i<this.oneDTest1.length;++i)
			assertEquals(1./this.oneDTest1[i], res[i], 0);
	}
	
	@Test
	public void testLog1D() {
		double[] res = VectorUtils.log(this.oneDTest1);
		for(int i=0;i<this.oneDTest1.length;++i)
			assertEquals(Math.log(this.oneDTest1[i]), res[i], 0);
	}
	
	@Test
	public void testLog2D() {
		double[][] res = VectorUtils.log(this.twoDTest1);
		for(int i=0;i<this.twoDTest1.length;++i) {
			for (int j = 0; j < this.twoDTest1[i].length; ++j) {
				assertEquals(Math.log(this.twoDTest1[i][j]), res[i][j], 0);
			}
		}
		
	}
	
	@Test
	public void testLog1DI() {
		int[] test = new int[]{2,4,6,8};
		double[] res = VectorUtils.log(test);
		for(int i=0;i<this.oneDTest1.length;++i)
			assertEquals(Math.log(test[i]), res[i], 0);
	}
		
	@Test
	public void testMask() {
		
	}
	
	@Test
	public void testMultiply1D1D() {
		double[] res = VectorUtils.multiply(this.oneDTest1, this.oneDTest2);
		assertArrayEquals(this.oneDOutMult, res, 0);
	}
	
	@Test
	public void testMultiply2D2D() {
		double[][] res = VectorUtils.multiply(this.twoDTest1, this.twoDTest2);
		for(int i=0;i<this.twoDTest1.length;++i)
			assertArrayEquals(this.twoDOutMult[i], res[i], 0);
	}
	
	@Test
	public void testMultiply1I1D() {
		
	}
	
	@Test
	public void testMultiplyWithScalar1D() {
		double[] res = VectorUtils.multiplyWithScalar(this.oneDTest1, 1.5);
		assertArrayEquals(this.oneDOutMultScal, res, 0);
	}
	
	@Test
	public void testMultiplyWithScalar2D() {
		double[][] res = VectorUtils.multiplyWithScalar(this.twoDTest1, 1.5);
		for(int i=0;i<this.twoDTest1.length;++i)
			assertArrayEquals(this.twoDOutMultScal[i], res[i], 0);
	}
	
	@Test
	public void testMultiplyWithScalar1DModifying() {
		double[] res = this.oneDTest1.clone();
		VectorUtils.multiplyWithScalar_(res, 1.5);
		assertArrayEquals(this.oneDOutMultScal, res, 0);
	}
	
	@Test
	public void testNeg() {
		double[] res = VectorUtils.neg(this.oneDTest1);
		for(int i=0;i<this.oneDTest1.length;++i)
			assertEquals(-this.oneDTest1[i], res[i], 0);
	}
	
	@Test
	public void testNegModifying() {
		double[] res = this.oneDTest1.clone();
		VectorUtils.neg_(res);
		for(int i=0;i<this.oneDTest1.length;++i)
			assertEquals(-this.oneDTest1[i], res[i], 0);
		
	}
	
	@Test
	public void testNorm() {
		
	}
	
	@Test
	public void testNormalize() {
		
	}
	
	@Test
	public void testSetColumnD() {
		
	}
	
	@Test
	public void testSetSelectionD() {
		
	}
	
	@Test
	public void testSubtract1D() {
		
	}
	
	@Test
	public void testSubtract2D() {
		
	}
	
	@Test
	public void testSubtract1DModifying() {
		
	}
	
	@Test
	public void testSubtractFromScalar1D() {
		
	}
	
	@Test
	public void testSubtractFromScalar2D() {
		
	}
	
	@Test
	public void testSubtractScalar1D() {
		
	}
	
	@Test
	public void testSubtractScalar2D() {
		
	}
	
	@Test
	public void testSubtractSelection() {
		
	}
	
	@Test
	public void testSum1D() {
		double sum = VectorUtils.sum(this.oneDTest1);
		assertEquals(6., sum, 0);
	}
	
	@Test
	public void testSum2D() {
		double sum = VectorUtils.sum(this.twoDTest1);
		assertEquals(45, sum, 0);
	}
	
	@Test
	public void testSum1I() {
		
	}
	
	@Test
	public void testSumAxis1() {
		
	}
	
	@Test
	public void testSumAxis2() {
		
	}
	
	@Test
	public void testTranspose2D() {
		
	}

	@Test
	public void testViewColumn() {
		
	}
	
	@Test
	public void testViewColumnSelection() {
		
	}
	
	@Test
	public void testViewSelection() {
		
	}
	
/*
	@Test
	public void testMax() {
		
	}
	
	@Test
	public void testMeanD() {
		
	}
	
	@Test
	public void testMeanI() {
		
	}
	
	@Test
	public void testMin() {
		
	}
	
	@Test
	public void testRemoveElementD() {
		
	}
	
	@Test
	public void testRemoveElementI() {
		
	}
	
	@Test
	public void testRemoveRowI() {
		
	}
	
	@Test
	public void testSquare1D() {
		
	}
	
	
	@Test
	public void testVariance() {
		
	}
	
	*/
}
