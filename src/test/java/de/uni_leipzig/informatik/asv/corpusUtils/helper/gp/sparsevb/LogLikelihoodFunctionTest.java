/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.sparsevb;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ZeroMeanFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full.GaussianProcessTest;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
@RunWith(JUnit4.class)
public class LogLikelihoodFunctionTest {
	@Test
	public void testFunctionGradient() {
		SparseGaussianProcessVB gp = new SparseGaussianProcessVB(new ZeroMeanFunction(), new GaussianKernel(.001, .25, .5), 5);
		double[][] trainingData = GaussianProcessTest.generateTrainingData();
		gp.setTrainingData(trainingData, GaussianProcessTest.generateTargetData(trainingData));
		
		LogLikelihoodOptimizeParameters p = new LogLikelihoodOptimizeParameters(gp);
		p.setKnn(gp.getKnn());
		p.setX(gp.getX());
		p.setXm(gp.getXm());
		p.setY(gp.getY());
		p.setJitter(1e-3);
		p.doComputations();
		
		
		LogLikelihoodFunction f = new LogLikelihoodFunction(5, p);

		double[] point = VectorUtils.viewColumn(gp.getXm().getData(), 0);
		double h = 1e-3;
		
		
		double valX = f.f(point);
		double[] diffs = new double[5];
		for(int i=0;i<5;++i) {
			// variable variations
			point[i] += h;
			double valXPlusH = f.f(point);
			point[i] -= h;
			//differentials
			diffs[i] = (valXPlusH - valX)/h;
		}
		
		double[] grad = new double[5]; 
		f.df(point, grad);
		
		for(int i=0;i<5;++i)
			assertEquals(null, grad[i], diffs[i], 1e-3);
	}
	

}
