/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde;

import static junit.framework.Assert.assertEquals;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.sde.DoubleWellSDE;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class SDEAbsolutePartObjectiveTest {

	private static final int K = 10;
	private SDEVBInferencer inf;
	private SDEOptimizeParameters optParams;
	private SDEAbsolutePartObjective func;
	private double h = 1e-9;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		RealMatrix noiseMatrix = MatrixUtils.createRealIdentityMatrix(1);
		RealMatrix trainingData = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.K, 1));
		for(int i=0;i<this.K;i++)
			trainingData.setEntry(i, 0, i);
		
		RealMatrix targetData = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.NORMAL, this.K, 1));
		
		StochasticDifferentialEquation sde = new DoubleWellSDE(noiseMatrix.getData(), 1);
		this.inf = new SDEVBInferencer(sde);
		this.inf.setTrainingData(trainingData.getColumn(0), trainingData.getData(), targetData.getData(), 1, this.K, .04);
		
		optParams = new SDEOptimizeParameters();
		optParams.setDimensions(1);
		optParams.setInf(inf);
		
		func = new SDEAbsolutePartObjective(1, optParams);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.SDEAbsolutePartObjective#df(double[], double[])}.
	 */
	@Test
	public void testDf() throws Exception {
		for(int k=0;k<K;k++) {
			optParams.setTime(k);
			double[] pointAsList = SDEVBInferencer.create1DArray(inf.getVariationalFunctionB()[k]);
			double val = func.f(pointAsList);
			
			double[] df = new double[pointAsList.length]; 
			func.df(pointAsList, df);
			
			for(int i=0;i<pointAsList.length;i++) {
				double oldPoint = pointAsList[i];
				pointAsList[i] += h ;
				double valPlusH = func.f(pointAsList);
				pointAsList[i] = oldPoint;
				double diff = (valPlusH - val)/h;
				assertEquals(null, diff, df[i], val/1000);
			}
		}
	}

}
