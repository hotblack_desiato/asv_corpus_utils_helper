/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.sparsevb;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import cern.jet.random.Normal;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.ZeroMeanFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;

@RunWith(JUnit4.class)
public class SparseGaussianProcessTest {
	
	
	private static double s2;
	private static double l;
	private static double noise;
	private static double[] trainingTargets;
	private static double[][] trainingInputs;
	
	@BeforeClass
	public static void startUpClass() {
		s2 = .5;
		l = .9;
		noise = .1;
		trainingInputs = generateTrainingData();
		trainingTargets = generateTargetData(trainingInputs);
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full.GaussianProcess#predict(double[][])}.
	 */
	@Test
	public void testPredict() throws Exception {
		//generate some test data inputs
		double[][] testData = generateTestData();
		
		//construct a new kernel and GP
		GaussianKernel k = new GaussianKernel(s2, l, noise);
		SparseGaussianProcessVB gp = new SparseGaussianProcessVB(new ZeroMeanFunction(), k, 15);
		//set the training data and train initially
		gp.setTrainingData(trainingInputs, trainingTargets);
		gp.train();
		//get current likelihood
		double lik, oldVal = gp.computeLikelihood();
		double converged = 1;
		while(converged > 1e-7) {
			//update the hyperparameters with conjugate gradient steps
			gp.getCovFunction().updateHyperparameters(gp);
			//and train the gp again
			gp.train();
			//compute current likelihood and compute convergence, likelihood should increase
			lik = gp.computeLikelihood();
			converged = (oldVal - lik) / oldVal;
			oldVal = lik;
			System.out.println("current likelihood: " + lik + ", converged = " + converged);
		}
		gp.predict(testData);
		
		
		System.out.println("got predictive mean and variance");
		double[] predictiveMean = gp.getPredictiveMean();
		double[] predictiveVariance = gp.getPredictiveVariance();
				
		BufferedWriter bw = new BufferedWriter(new FileWriter("/Users/patrick/Code/workspace/lda_simple/data/sgp/data"));
		for(int i=0;i<trainingInputs.length;++i) 
			bw.write(trainingInputs[i][0] + "\t" + trainingTargets[i] + "\n");
		bw.close();
		
		bw = new BufferedWriter(new FileWriter("/Users/patrick/Code/workspace/lda_simple/data/sgp/test"));
		
		for(int i=0;i<testData.length;++i) { 
			double doubleSd = Math.sqrt(predictiveVariance[i]);
			bw.write(trainingInputs[i][0] + "\t" + trainingTargets[i] + "\t" + testData[i][0] + "\t" + predictiveMean[i] + "\t" + predictiveVariance[i] + "\t" + (predictiveMean[i] - doubleSd) + "\t" + (predictiveMean[i] + doubleSd) + "\n"); 
		}
		bw.close();
		
		for(int i=0;i<predictiveMean.length;i++) {
			assertEquals(i+"-th point, cur variance = " + predictiveVariance[i] + ": ", Math.sin(testData[i][0]), predictiveMean[i], 2*Math.sqrt(predictiveVariance[i]));
		}

	}

	public static double[][] generateTestData() {
		int n = 200;
		double[][] test = new double[n][];
		double xStart = -10;
		double xEnd = 10;
		double step = (xEnd-xStart)/(double)n;
		for(int i=0;i<test.length;++i) {
			double noise = Normal.staticNextDouble(0, .01);
			test[i] = new double[]{xStart + i*step + noise};
		}
		return test;
	}

	public static double[] generateTargetData(double[][] trainingData) {
		double[] target = new double[trainingData.length];
		for(int i=0;i<trainingData.length;i++) {
			double n = Normal.staticNextDouble(0, .5);
			target[i] = Math.sin(trainingData[i][0]) + n; 
		}
		return target;
	}

	public static double[][] generateTrainingData() {
		int n = 300;
		double[][] ret = new double[n][];
		double xStart = -5;
		double xEnd = 5;
		
		double step = (xEnd-xStart)/(double)n;
		
		for(int i=0;i<n;i++) {
			ret[i] = new double[]{xStart + i*step};
		}
		return ret;
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full.GaussianProcess#computeLikelihood()}.
	 */
	@Test
	public void testComputeLikelihood() throws Exception {
		//construct local temp kernel
		GaussianKernel localKernel = new GaussianKernel(SparseGaussianProcessTest.s2, SparseGaussianProcessTest.l, SparseGaussianProcessTest.noise);
		//compute covariance matrix
		RealMatrix covMatrix = MatrixUtils.createRealMatrix(localKernel.computeCovarianceMatrix(SparseGaussianProcessTest.trainingInputs));
		//zero mean the targets and construct a matrix object
		double m = VectorUtils.mean(SparseGaussianProcessTest.trainingTargets);
		RealMatrix y = MatrixUtils.createColumnRealMatrix(SparseGaussianProcessTest.trainingTargets);
		y = y.scalarAdd(-m);
		//cholesky decomposition
		CholeskyDecomposition cd = new CholeskyDecomposition(covMatrix);
		//compute inverse cov matrix and determinant
		RealMatrix covMatrixInv = cd.getSolver().getInverse();
		double det = cd.getDeterminant();
		
		//compute the likelihood and its terms
		double logLikelihood = 0;
		//term 1, given by y^T*K^{-1}*y
		RealMatrix term1 = y.transpose().multiply(covMatrixInv).multiply(y);
		assertEquals(null, 1, term1.getRowDimension(), 0);
		assertEquals(null, 1, term1.getColumnDimension(), 0);
		//use trace operator, if all was ok, term1 is 1x1 matrix
		logLikelihood += -.5*term1.getTrace();
		//compute log determinant
		if(det == 0.)
			logLikelihood += -.5*MathUtils.safeLog(det);
		else
			logLikelihood += -.5*Math.log(det);
		// add the normalizing constant
		logLikelihood += -.5*SparseGaussianProcessTest.trainingTargets.length *Math.log(2*Math.PI);
		
		//the following is optional
		
		double logLikelihoodAlt = 0;
		//new decomposition
		CholeskyDecomposition L = new CholeskyDecomposition(covMatrix);
		//solve for alpha as in Rasmussen/Williams: Gaussian Processes for Machine Learning
		RealMatrix alpha = L.getSolver().solve(y);
		//compute alternative for term1
		RealMatrix term1Alt = y.transpose().multiply(alpha);
		assertEquals(null, 1, term1Alt.getRowDimension(), 0);
		assertEquals(null, 1, term1Alt.getColumnDimension(), 0);
		//term1 should be the same in both types of computations
		assertEquals(null, term1.getTrace(), term1Alt.getTrace(), 1e-9);
		logLikelihoodAlt += -.5*term1Alt.getTrace();
		//alternative way to compute the determinant of a matrix
		double detAlt = 0;
		double logDetAlt = 0;
		double[] lDiag = new double[L.getL().getRowDimension()];
		for(int i=0;i<lDiag.length;++i) {
			lDiag[i] = L.getL().getEntry(i, i);
			logDetAlt += Math.log(lDiag[i]);
			if(i == 0)
				detAlt = lDiag[i]*lDiag[i];
			else
				detAlt *= lDiag[i]*lDiag[i];
		}
		//both determinants should equal
		assertEquals(null, det, detAlt, 1e-9);
		assertEquals(null, Math.log(det), 2*logDetAlt, 1e-9);
		logLikelihoodAlt += - logDetAlt;
		//add normalizer
		logLikelihoodAlt += -.5*SparseGaussianProcessTest.trainingTargets.length*Math.log(2*Math.PI);
		
		assertEquals(null, logLikelihood, logLikelihoodAlt, 1e-9);
		
		// end optional, was for debugging purposes
		
		SparseGaussianProcessVB gp = new SparseGaussianProcessVB(new ZeroMeanFunction(), localKernel, 15);
		gp.setTrainingData(trainingInputs, trainingTargets);
		gp.train();
		double gpLikelihood = gp.computeLikelihood();
		assertEquals(null, logLikelihood, gpLikelihood, 1e-9);		
	}

}
