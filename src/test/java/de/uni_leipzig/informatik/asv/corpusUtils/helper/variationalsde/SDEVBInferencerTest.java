/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde;

import static junit.framework.Assert.assertEquals;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.sde.DoubleWellSDE;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class SDEVBInferencerTest {

	SDEVBInferencer inf;
	private double h = 1e-9;
	private int K = 10;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		RealMatrix noiseMatrix = MatrixUtils.createRealIdentityMatrix(1);
		RealMatrix trainingData = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.ZERO, this.K, 1));
		for(int i=0;i<this.K;i++)
			trainingData.setEntry(i, 0, i);
		
		RealMatrix targetData = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.NORMAL, this.K, 1));
		
		StochasticDifferentialEquation sde = new DoubleWellSDE(noiseMatrix.getData(), 1);
		this.inf = new SDEVBInferencer(sde);
		this.inf.setTrainingData(trainingData.getColumn(0), trainingData.getData(), targetData.getData(), 1, this.K, .04);
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.SDEVBInferencer#computeSDEEnergyDerivativeA(org.apache.commons.math3.linear.RealMatrix, int)}.
	 */
	@Test
	public void testComputeSDEEnergyDerivativeA() throws Exception {
		for(int k=1;k<=this.K;k++) {
			double val = this.inf.computeSDEEnergy(k);
			RealMatrix derivB = this.inf.computeSDEEnergyDerivativeB(k);
			RealMatrix deriv = this.inf.computeSDEEnergyDerivativeA(k, derivB);
			
			double[][] derivArray = deriv.getData();
			
			
			for(int i=0;i<derivArray.length;i++) {
				for(int j=0;j<derivArray[i].length;j++) {
					double oldA = this.inf.getVariationalFunctionA()[k].getEntry(i, j);
					this.inf.getVariationalFunctionA()[k].setEntry(i, j, oldA+this.h );
					double valIJ = this.inf.computeSDEEnergy(k);
					double diff = (valIJ - val)/this.h;
					this.inf.getVariationalFunctionA()[k].setEntry(i, j, oldA);
					assertEquals("time " + k + ", element["+i+"]["+j+"]: ", diff, derivArray[i][j], val/1000);
				}
			}
		}
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.SDEVBInferencer#computeSDEEnergyDerivativeB(int)}.
	 */
	@Test
	public void testComputeSDEEnergyDerivativeB() throws Exception {
		for(int k=1;k<=this.K;k++) {
			double val = this.inf.computeSDEEnergy(k);
			RealMatrix derivB = this.inf.computeSDEEnergyDerivativeB(k);

			double[] derivArray = derivB.getColumn(0);
			
			
			for(int i=0;i<derivArray.length;i++) {
				double oldB = this.inf.getVariationalFunctionB()[k].getEntry(i, 0);
				this.inf.getVariationalFunctionB()[k].setEntry(i, 0, oldB+this.h );
				double valI = this.inf.computeSDEEnergy(k);
				double diff = (valI - val)/this.h;
				this.inf.getVariationalFunctionA()[k].setEntry(i, 0, oldB);
				assertEquals("element["+i+"]: ", diff, derivArray[i], val/1000);
			}
		}
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.SDEVBInferencer#computeObservationEnergyDerivativeMean(int)}.
	 */
	@Test
	public void testComputeObservationEnergyDerivativeMean() throws Exception {
		for(int k=0;k<this.K;k++) {
			double val = this.inf.computeObservationEnergy(k);
			RealMatrix derivMean = this.inf.computeObservationEnergyDerivativeMean(k);

			double[] derivArray = derivMean.getColumn(0);
			
			
			for(int i=0;i<derivArray.length;++i) {
				double oldM = this.inf.getPredictiveMean()[k].getEntry(i, 0);
				this.inf.getPredictiveMean()[k].setEntry(i, 0, oldM+this.h );
				double valI = this.inf.computeObservationEnergy(k);
				double diff = (valI - val)/this.h;
				this.inf.getPredictiveMean()[k].setEntry(i, 0, oldM);
				System.out.println("diff_i : " + diff + ", deriv_i: " + derivArray[i]);
				assertEquals("mean: time " + k + ", element["+i+"]: ", diff, derivArray[i], val/1000);
			}
		}

	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.SDEVBInferencer#computeObservationEnergyDerivativeVariance(int)}.
	 */
	@Test
	public void testComputeObservationEnergyDerivativeVariance() throws Exception {
		for(int k=0;k<this.K;k++) {
			double val = this.inf.computeObservationEnergy(k);
			RealMatrix derivVar = this.inf.computeObservationEnergyDerivativeVariance(k);

			double[][] derivArray = derivVar.getData();
			
			
			for(int i=0;i<derivArray.length;i++) {
				for(int j=0;j<derivArray[i].length;j++) {
					double oldVar = this.inf.getPredictiveVariance()[k].getEntry(i, j);
					this.inf.getPredictiveVariance()[k].setEntry(i, j, oldVar+this.h );
					double valIJ = this.inf.computeObservationEnergy(k);
					double diff = (valIJ - val)/this.h;
					this.inf.getPredictiveVariance()[k].setEntry(i, j, oldVar);
					System.out.println("diff_ij : " + diff + ", deriv_ij: " + derivArray[i][j]);
					assertEquals("var: time " + k + ", element["+i+"]["+j+"]: ", diff, derivArray[i][j], val/1000);
				}
			}
		}
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.SDEVBInferencer#createRealMatrixFromList(double[], int, int)}.
	 */
	@Test
	public void testCreateRealMatrixFromList() throws Exception {
		double[] list = ArrayFactory.doubleArray(Type.UNIFORM, 1000);
		RealMatrix temp = SDEVBInferencer.createRealMatrixFromList(list, 20, 50);
		double[] secList = SDEVBInferencer.create1DArray(temp);
		assertEquals(null, list.length, secList.length, 0);
		for(int i=0;i<list.length;i++)
			assertEquals(null, list[i], secList[i], 0);
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.SDEVBInferencer#create1DArray(org.apache.commons.math3.linear.RealMatrix)}.
	 */
	@Test
	public void testCreate1DArray() throws Exception {
		RealMatrix m1 = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.UNIFORM, 50, 20));
		double[] tempList = SDEVBInferencer.create1DArray(m1);
		RealMatrix m2 = SDEVBInferencer.createRealMatrixFromList(tempList, 50, 20);
		assertEquals(m1, m2);
	}

}
