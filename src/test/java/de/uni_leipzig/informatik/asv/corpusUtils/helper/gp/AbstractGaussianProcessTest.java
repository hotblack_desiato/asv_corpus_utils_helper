/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.gp;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full.GaussianProcess;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.full.GaussianProcessTest;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class AbstractGaussianProcessTest {
	
	private AbstractGaussianProcess gp;
	private double[][] testData;
	private double[] targetData;
	private ICovarianceFunction kernel;
	private double mean;
	
	@Before
	public void testSetUp() {
		this.kernel = new GaussianKernel(.2, .5, .1);
		this.gp = new GaussianProcess(new ZeroMeanFunction(), this.kernel);
		this.testData = GaussianProcessTest.generateTestData();
		this.targetData = GaussianProcessTest.generateTargetData(this.testData);
		this.mean = VectorUtils.mean(this.targetData);
		this.gp.setTrainingData(this.testData, this.targetData);
	}
	
	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess#getCovFunction()}.
	 */
	@Test
	public void testGetCovFunction() throws Exception {
		assertEquals(this.kernel, this.gp.getCovFunction());
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess#setTrainingData(double[][], double[])}.
	 */
	@Test
	public void testSetTrainingData() throws Exception {
		double[] newTarget = VectorUtils.subtractScalar(this.targetData, this.mean);

		assertEquals(null, this.mean, this.gp.m0, 0);
		testArrays(this.gp.getTargetData(), newTarget);
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess#getDataCovariates()}.
	 */
	@Test
	public void testGetDataCovariates() throws Exception {
		testArrays(this.testData, this.gp.getDataCovariates());
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.AbstractGaussianProcess#getTargetData()}.
	 */
	@Test
	public void testGetTargetData() throws Exception {
		double[] origTarget = VectorUtils.addScalar(this.gp.getTargetData(), this.mean);
		testArrays(this.targetData, origTarget);
	}
	
	private void testArrays(double[][] expected, double[][] actual) {
		assertEquals(expected.length, actual.length);
		for(int i=0;i<expected.length;++i)
			testArrays(expected[i], actual[i]);
	}
	
	private void testArrays(double[] expected, double[] actual) {
		assertEquals(null, expected.length, actual.length);
		for(int i=0;i<expected.length;++i) {
			assertEquals(null, expected[i], actual[i], 1e-9);
		}
	}

}
