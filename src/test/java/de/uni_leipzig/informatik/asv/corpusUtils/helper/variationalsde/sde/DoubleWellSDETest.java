/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.sde;

import static junit.framework.Assert.assertEquals;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class DoubleWellSDETest {

	DoubleWellSDE sde;
	private RealMatrix vX;
	private RealMatrix eX;
	private double h = 1e-5;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		RealMatrix noiseMatrix = MatrixUtils.createRealIdentityMatrix(1);
		this.eX = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.NORMAL, 1, 1));
		this.vX = MatrixUtils.createRealMatrix(ArrayFactory.doubleArray(Type.UNIFORM, 1, 1));
		
		this.sde = new DoubleWellSDE(noiseMatrix.getData(), 1);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link de.uni_leipzig.informatik.asv.corpusUtils.helper.variationalsde.sde.DoubleWellSDE#driftFunctionExpectedDerivativeX(org.apache.commons.math3.linear.RealMatrix, org.apache.commons.math3.linear.RealMatrix, int)}.
	 */
	@Test
	public void testDriftFunctionExpectedDerivativeX() throws Exception {
		RealMatrix val = this.sde.driftFunctionExpectation(this.eX, this.vX, 0);
		
		RealMatrix deriv = this.sde.driftFunctionExpectedDerivativeX(this.eX, this.vX, 0);
		
		this.eX = this.eX.scalarAdd(this.h);
		RealMatrix valPlusH = this.sde.driftFunctionExpectation(this.eX, this.vX, 0);
		
		double diff = (valPlusH.getEntry(0, 0) - val.getEntry(0, 0))/this.h;
		
		assertEquals(null, deriv.getEntry(0, 0), diff, 1e-3);
	}

}
