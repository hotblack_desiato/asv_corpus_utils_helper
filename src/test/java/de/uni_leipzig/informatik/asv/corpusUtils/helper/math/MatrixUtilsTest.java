/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.corpusUtils.helper.math;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import cern.colt.Arrays;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MatrixUtils;

@RunWith(JUnit4.class)
public class MatrixUtilsTest {

	@Test
	public void testCumulativeSum() {
		double[] inArray = new double[]{1.,2.,3.,4.,5.};
		double[] expected = new double[]{1.,3.,6.,10.,15.};
		DoubleMatrix1D in = new DenseDoubleMatrix1D(inArray);
		DoubleMatrix1D out = MatrixUtils.cumulativeSum(in);
		assertEquals(Arrays.toString(expected), Arrays.toString(out.toArray()));
	}
	 
}
